<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['email','type_id','entity_id','email_nature_id'];

    protected $dates = ['deleted_at'];
}
