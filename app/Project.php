<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use SoftDeletes;
    protected $table = 'projects';
    protected $fillable=['project_type','name','tenant_type','units_no','description'];
    protected $dates = ['deleted_at'];
}
