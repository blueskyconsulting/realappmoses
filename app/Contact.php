<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['salutation_id','firstname','lastname','title',
        'organization_id','contact_type_id','background','permission_id'];

    protected $dates = ['deleted_at'];
}
