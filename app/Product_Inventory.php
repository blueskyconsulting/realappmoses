<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Inventory extends Model
{
    use SoftDeletes;
    protected $table='product_inventories';
    protected $fillable=['property_no','product_id','inventory_status_id'];
}
    
    
    