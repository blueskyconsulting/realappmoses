<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Task extends Model
{
    
    use SoftDeletes;
    //
    protected $table='tasks';
    protected $fillable=['id','name','priority_type_id','task_type',
        'reminder','start_date','status','today','mail','permission_id'];

    protected $dates = ['deleted_at'];

}
