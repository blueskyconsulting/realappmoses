<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Priority_type extends Model
{
    use SoftDeletes;
    protected $table = 'priority_types';
    protected $fillable=['type'];
    protected $dates = ['deleted_at'];
}
