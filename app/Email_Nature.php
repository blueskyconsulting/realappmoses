<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email_Nature extends Model
{
    use SoftDeletes;
    protected $table='email_natures';
    protected $fillable=['email_nature'];

    protected $dates = ['deleted_at'];
}
