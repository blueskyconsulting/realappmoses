<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PinnedTask extends Model
{
    use SoftDeletes;
    protected $fillable=['task_id','entity_id','type_id'];
    protected $dates=['deleted_at'];
}
