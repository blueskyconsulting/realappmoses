<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phone_Number extends Model
{
    use SoftDeletes;
    //
    protected $table='phone_numbers';
    protected $fillable=['phone_number','entity_id','type_id','phone_nature_id'];

    protected $dates = ['deleted_at'];
}
