<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
use App\Dates_to_remember;
use App\Event;
use App\Type;
use App\Contact_Type;
use App\User;
use Mail;
use DB;

class Notify extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show notifications,(tasks, dates, events) that occur today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $actual_date = date('Y-m-d', time());
        $user = User::all();
        $events = Event::all();
        if ($events->count()) {
            foreach ($events as $e) {
                $phpdate = strtotime($e->start);
                $mysqldate = date('Y-m-d', $phpdate);
                if ($mysqldate == $actual_date) {
                    $e->today = 1;
                    $e->save();

                    if ($e->mail == 0) {
                        foreach ($user as $u) {
                            foreach ($user as $u) {
                                Mail::send('emails.eventreminder', ['user' => $u->name, 'event' => $e->title], function ($m) use ($u) {
                                    $m->from('reminder@Realapp.com', 'Real app');

                                    $m->to($u->email, $u->name)->subject('Event Reminder');
                                });
                            }
                        }
                        $e->mail = 1;
                        $e->save();
                    }
                } else {
                    $e->today = 0;
                    $e->save();
                }
            }
        }
        $orgtype = Type::where('type', 'Organization')->first();
        $contact_type = Type::where('type', 'Contact')->first();

        $orgdates = \DB::table('organizations')
                ->join('dates_to_remember', 'dates_to_remember.entity_id', '=', 'organizations.id')
                ->select('organizations.name', 'organizations.id AS id', 'dates_to_remember.entity_id', 'dates_to_remember.description', 'dates_to_remember.date', 'dates_to_remember.today', 'dates_to_remember.mail', 'dates_to_remember.id AS date_id')
                ->where('dates_to_remember.type_id', $orgtype->id)
                ->get();
        //dd($orgdates);
        $orgowners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $orgtype->id)
                ->get();
        //dd($orgowners);
        $contactowners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $contact_type->id)
                ->get();
        //dd($contactowners);
        $contact_dates = \DB::table('contacts')
                ->join('dates_to_remember', 'dates_to_remember.entity_id', '=', 'contacts.id')
                ->select('contacts.firstname', 'contacts.lastname', 'dates_to_remember.entity_id', 'dates_to_remember.description', 'dates_to_remember.date', 'dates_to_remember.today', 'dates_to_remember.mail', 'dates_to_remember.id AS date_id', 'contacts.id')
                ->where('dates_to_remember.type_id', $contact_type->id)
                ->get();
        //dd($contact_dates);

        foreach ($orgdates as $d) {

            $phpdate = strtotime($d->date);
            $mysqldate = date('Y-m-d', $phpdate);

            if ($mysqldate == $actual_date) {

                $condate = Dates_to_remember::find($d->date_id);
                $condate->today = 1;
                $condate->save();
                foreach ($orgowners as $u) {
                    if ($u->entity_id == $d->id) {
                        if ($d->mail == 0) {
                            $this->info('org');
                            Mail::send('emails.datesreminder', ['user' => $u->name, 'date' => $d->description, 'org' => $d->name], function ($m) use ($u) {
                                $m->from('reminder@Realapp.com', 'Real app');

                                $m->to($u->email, $u->name)->subject('Date Reminder');
                            });

                            $condate = Dates_to_remember::find($d->date_id);
                            $condate->mail = 1;
                            $condate->save();
                        }
                    }
                }
            } else {
                $condate = Dates_to_remember::find($d->date_id);
                $condate->today = 0;
                $condate->save();
            }
        }


        foreach ($contact_dates as $d) {
            $phpdate = strtotime($d->date);
            $mysqldate = date('Y-m-d', $phpdate);

            if ($mysqldate == $actual_date) {
                $condate = Dates_to_remember::find($d->date_id);
                $condate->today = 1;
                $condate->save();
                foreach ($contactowners as $u) {
                    if ($u->entity_id == $d->id) {
                        if ($d->mail == 0) {
                            $this->info('contact');
                            Mail::send('emails.datesreminder', ['user' => $u->name, 'date' => $d->description, 'org' => $d->firstname], function ($m) use ($u) {
                                $m->from('reminder@Realapp.com', 'Real app');

                                $m->to($u->email, $u->name)->subject('Date Reminder');
                            });

                            $condate = Dates_to_remember::find($d->date_id);
                            $condate->mail = 1;
                            $condate->save();
                        }
                    }
                }
            } else {
                $condate = Dates_to_remember::find($d->date_id);
                $condate->today = 0;
                $condate->save();
            }
        }

        $tasks = Task::all();
        $task_type = Type::where('type', 'task')->first();
        $owners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $task_type->id)
                ->get();

        foreach ($tasks as $t) {
            if ($t->reminder) {
                $phpdate = strtotime($t->reminder);
                $mysqldate = date('Y-m-d H:i:s', $phpdate);
                $actual = date('Y-m-d H:i:s', time());
                if ($mysqldate == $actual) {

                    foreach ($owners as $u) {
                        if ($u->entity_id == $t->id) {
                            if ($t->mail == 0) {

                                Mail::send('emails.tasksreminder', ['user' => $u->name, 'task' => $t->name], function ($m) use ($u) {
                                    $m->from('reminder@Realapp.com', 'Real app');

                                    $m->to($u->email, $u->name)->subject('Task Reminder');
                                });

                                $t->mail = 1;
                                $t->save();
                            }
                        }
                    }
                } else {
                    $t->today = 0;
                    $t->save();
                }
            }

            $phpdate = strtotime($t->start_date);
                $mysqldate = date('Y-m-d', $phpdate);
             
            if ($mysqldate == $actual_date) {
                $this->info('nawak');
                $t->today = 1;
                $t->save();
            } else {
                $t->today = 0;
                $t->save();
            }
        }

        $this->info('Done' . $mysqldate . 'same as' . $actual_date);
    }

}
