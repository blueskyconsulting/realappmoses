<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Task_status extends Model
{
    protected $table='tasks_status';
    protected $fillable=['id','status'];

    protected $dates = ['deleted_at'];
}
