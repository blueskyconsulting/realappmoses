<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opportunities_Status extends Model
{
    // alllow for softdeleted
    use SoftDeletes;
    
    // table to use
    protected $table = 'opportunities_status';
    // mass assigned
    protected $fillable = ['opportunity_id','status_stage_id', 'status_name_id',
        'notes', 'reasons'];
    // for soft deletes
    protected $dates = ['deleted_at'];
}
