<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Task_type extends Model
{
    protected $table='task_types';
    protected $fillable=['id','type'];

    protected $dates = ['deleted_at'];
}
