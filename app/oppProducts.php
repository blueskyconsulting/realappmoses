<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class oppProducts extends Model
{
    use SoftDeletes;
    //
    protected  $table='opp_inventory';
    protected $fillable=['opportunity_id','inventory_id','payment_plan_id','quantity','value'];

    protected $dates = ['deleted_at'];
}
