<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Opportunities extends Model {

    // alllow for softdeleted
    use SoftDeletes;

    // table to use
    protected $table = 'opportunities';
    // mass assigned
    protected $fillable = ['name','entity_id','type_id','description', 'winning_probability',
        'user_responsible_id', 'amount', 'bid_type_id', 'pipeline_id','permission_id', 'close_date'];
    // for soft deletes
    protected $guarded = ['deleted_at'];
    protected $dates = ['deleted_at'];

}
