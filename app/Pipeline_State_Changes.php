<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pipeline_State_Changes extends Model
{
    // alllow for softdeleted
    use SoftDeletes;
    
    // table to use
    protected $table = 'pipeline_state_changes';
    // mass assigned
    protected $fillable = ['opportunity_id','current_stage_id', 'previous_stage_id',
        'notes'];
    // for soft deletes
    protected $dates = ['deleted_at'];
}
