<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     use SoftDeletes;
    protected $table='events';
    protected $fillable=['title','start','end','today','mail'];

    protected $dates = ['deleted_at'];
}
