<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;
    //
     protected $table = 'address';
     protected $fillable=['city','address','postal_code','type_id','entity_id','address_nature_id','country'];
     protected $dates = ['deleted_at'];
}
