<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assigned_Entity extends Model
{
    use SoftDeletes;
    protected $table = 'assigned_entities';
    protected $fillable=['user_id','type_id','entity_id'];
    protected $dates = ['deleted_at'];
}
