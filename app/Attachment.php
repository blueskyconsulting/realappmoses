<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Attachment extends Model
{
   
   protected $table='email_attachments';
   protected $fillable=['url','filename','email_id'];
}
