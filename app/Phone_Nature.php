<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phone_Nature extends Model
{
    use SoftDeletes;
    protected $table='phone_natures';
    protected $fillable=['phone_nature'];

    protected $dates = ['deleted_at'];
}
