<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    use SoftDeletes;
    protected $table = 'receipts';
    protected $fillable=['invoice_id','receipt_no','payment_reason_id','payment_mode_id'
                        ,'receipt_date','amount','next_payment_date','cheque_no','permission_id'];
    protected $dates = ['deleted_at'];
}
