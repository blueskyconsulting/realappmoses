<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opportunities_StatusNames extends Model
{
    // alllow for softdeleted
    use SoftDeletes;
    
    // table to use
    protected $table = 'opportunities_status_names';
    // mass assigned
    protected $fillable = ['name'];
    // for soft deletes
    protected $dates = ['deleted_at'];
}
