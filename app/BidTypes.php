<?php
/* =============================================================================
 * 
 * ============================================================================= 
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BidTypes extends Model
{
    // alllow for softdeleted
    use SoftDeletes;
    // table to use
    protected $table='bid_types';
    // mass assigned
    protected $fillable=['name'];
    // for soft deletes
    protected $dates=['deleted_at'];
    
}
