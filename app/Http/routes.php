<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/pdfs', function () {
    return view('pdf.invoice');
});
Route::group(['prefix' => 'api'], function()
{
	Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
	Route::post('authenticate', 'AuthenticateController@authenticate');
	Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
        Route::get('notifications', 'TaskController@notification');
        Route::resource('leads','LeadsController');
        Route::resource('contacts','ContactController');
        Route::resource('emails','EmailController');
        Route::resource('email_attachments','EmailAttachmentController');
        Route::post('emails/send','EmailController@sendMail');
        
        Route::post('attachments','EmailController@uploadAttachment');
        Route::resource('phone_numbers','Phone_NumberController');
        Route::resource('organizations','OrganizationController');
        Route::resource('salutations','SalutationController');
        Route::resource('types','TypeController');
        Route::resource('phone_natures','Phone_NatureController');
        Route::resource('email_natures','Email_NatureController');
        Route::resource('address_natures','Address_NatureController');
        Route::resource('address','AddressController');
        Route::resource('socialnetworks','SocialNetworkController');
        Route::resource('socialnetworks_accounts','Social_Network_AccountController');
        Route::resource('priorities','PriorityTypeController');
        Route::resource('tasks','TaskController');
        Route::resource('industries','IndustryController');
        Route::resource('pinnedtasks','PinnedTaskController');
        Route::resource('pinned_events','PinnedEventController');
        Route::resource('owners','OwnerController');
        Route::resource('dates','DatesController');
        Route::resource('opportunities','OpportunityController');
        Route::resource('stage_changes','Opportunities_State_ChangesController');
        Route::resource('projects','ProjectController');
        Route::resource('products','ProductController');
        Route::resource('product_inventories','Product_InventoryController');
        Route::resource('payment_plans','Payment_PlanController');
        Route::resource('notes','NoteController');
        Route::resource('sources','LeadSourceController');
        Route::resource('opp_products','oppProductsController');
        Route::resource('events','EventController');
        Route::resource('invoices','InvoiceController');
        Route::resource('receipts','ReceiptController');
        Route::resource('transactions','TransactionController');
        Route::resource('pdfs','PdfController');

        Route::get('dateredirect/{id}','DatesController@getDate');
        Route::resource('email_messages','EmailMessageController');
        

        Route::get('dateredirect','DatesController@getDate');
        Route::resource('private_entities','Private_EntityController');
        Route::resource('assigned_entities','Assigned_EntityController');
        Route::resource('permissions','PermissionController');
        Route::resource('documents','DocumentController');
        Route::resource('profile_pictures','Profile_PictureController');

});
