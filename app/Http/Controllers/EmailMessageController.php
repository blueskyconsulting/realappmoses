<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Contact_Type;
use App\EmailMessage;
use DB;
class EmailMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgtype = Type::where('type', 'organization')->first();
        $contact_type = Type::where('type', 'Contact')->first();
        $leadtype = Contact_Type::where('contact_type', 'Lead')->first();
        
        $orgemails=DB::table('organizations')
                ->join('email_messages','email_messages.entity_id','=','organizations.id')
                ->leftJoin('email_attachments','email_messages.id','=','email_attachments.email_id')
               -> select('organizations.name','email_messages.subject',
                       'email_messages.body_text','email_messages.from','email_messages.id','email_messages.created_at','email_messages.status','email_messages.to','email_attachments.url')
                ->where('email_messages.type_id',$orgtype->id)
                ->get();
        $conemails=DB::table('contacts')
                ->join('email_messages','email_messages.entity_id','=','contacts.id')
                ->leftJoin('email_attachments','email_messages.id','=','email_attachments.email_id')
               -> select(DB::raw('CONCAT(contacts.firstname, ", ", contacts.lastname) AS name'),'email_messages.subject','email_messages.body_text','email_messages.from','email_messages.id','email_messages.created_at','email_messages.status','email_messages.to','email_attachments.url')
                ->where('email_messages.type_id',$contact_type->id)
                ->get();
        $leademails=DB::table('contacts')
                ->join('email_messages','email_messages.entity_id','=','contacts.id')
                ->leftJoin('email_attachments','email_messages.id','=','email_attachments.email_id')
               -> select(DB::raw('CONCAT(contacts.firstname, ", ", contacts.lastname) AS name'),'email_messages.subject','email_messages.body_text','email_messages.from','email_messages.id','email_messages.created_at','email_messages.status','email_messages.to','email_attachments.url')
                ->where('email_messages.type_id',$contact_type->id)
                ->where('contacts.contact_type_id',$leadtype->id)
                ->get();
        return response()->json([
            'orgemails'=>$orgemails,
            'contact_emails'=>$conemails,
            'lead_emails'=>$leademails
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $email= EmailMessage::find($id);
        $email->update($request->all());
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
