<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone_Nature;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Phone_NatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
//    public function __construct()
//    {
//        $this->middleware('jwt.auth');
//    }
    
    public static function index()
    {
        //return all the phonees to the view success value
        $phone_natures=Phone_Nature::all();
        return $phone_natures;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Phone_Nature::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phone_nature=Phone_Nature::find($id);
        return response()->json(array('success'=>true, 'phone_nature'=>$phone_nature));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $phone_nature=Phone_Nature::find($id);
        $phone_nature->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Phone_Nature::destroy($id);
        
        //this will permanently delete the item for db
        //$phone_natures->forcedelete();
        return response()->json(array('success'=>true));
    }
}
