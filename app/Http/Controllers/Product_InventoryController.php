<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Product_Inventory;

class Product_InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=\App\Product::all();
        $inventory_status=\App\InventoryStatus::all();
        $type_id=Type::where('type','Inventory')->first();
        $product_inventories=\DB::table('product_inventories')
                      ->join('products','products.id','=','product_inventories.product_id')
                      ->join('projects','projects.id','=','products.project_id')
                      ->join('payment_plans','payment_plans.product_id','=','product_inventories.product_id')
                      ->join('inventory_status','product_inventories.inventory_status_id','=','inventory_status.id')
                      ->select('products.name','products.size','projects.name as project_name',
                              'payment_plans.deposit','projects.project_type','product_inventories.*'
                              ,'inventory_status.status')
                      ->whereNull('product_inventories.deleted_at')
                      ->where('payment_plans.installment_no','=',0)
                      ->groupBy('product_inventories.id')
                      ->get();
        return response()->json(array('product_inventories'=>$product_inventories,
                            'products'=>$products,
                            'inventory_status'=>$inventory_status,
                             'type_id'=>$type_id->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Product_Inventory::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_inventory=Product_Inventory::find($id);
        $type_id=Type::where('type','Inventory')->first();
        $inventory_notes=\DB::table('notes')
                        ->leftJoin('users','users.id','=','notes.user_id')
                        ->where('entity_id',$id)
                        ->where('type_id',$type_id->id)
                        ->whereNull('notes.deleted_at')
                        ->select('notes.note','notes.id as note_id','users.name')
                        ->get();
        return response()->json(array('product_inventory'=>$product_inventory,
                                      'inventory_notes'=>$inventory_notes));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $product_inventory=Product_Inventory::find($id);
        $product_inventory->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product_Inventory::destroy($id);
        return response()->json(array('success'=>true));
    }
}
