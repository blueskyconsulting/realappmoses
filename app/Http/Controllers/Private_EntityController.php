<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Private_Entity;

class Private_EntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
//        return all the private_entityes to the view success value
        $private_entities=Private_Entity::all();
        return $private_entities;
//        return response()->json(array('private_entities'=>$private_entities));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Private_Entity::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $private_entity=Private_Entity::find($id);
        return response()->json(array('success'=>true, 'private_entity'=>$private_entity));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $private_entity=Private_Entity::find($id);
        $private_entity->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Private_Entity::destroy($id);
        
        //this will permanently delete the item for db
        //$private_entity_natures->forcedelete();
        return response()->json(array('success'=>true));
    }
}
