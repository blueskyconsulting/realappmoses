<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lead;
use App\Organization;
use App\Type;

use App\Dates_to_remember;
use App\Salutation;
use App\Contact_Type;
use App\Lead_source;
use App\Note;
use App\Address;
use App\Phone_Number;
use App\Email;
use App\Lead_status;
use App\Contact;
use App\Permission;
use App\Document;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\EmailMessage;
class LeadsController extends Controller
{
     function __construct() {
         $this->middleware('jwt.auth');
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type=Type::where('type','Contact')->first();
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;
        //Get the private permission id
        $private_id=Permission::where('permission','Private')->first()->id;
        //Get the public permission id
        $public_id=Permission::where('permission','Public')->first()->id;
        //Get the assigned id
        $assigned_id=Permission::where('permission','Assigned')->first()->id;
        
       $contact_type=  Contact_Type::where('contact_type','Lead')->first();
       $contact_update=  Contact_Type::where('contact_type','Customer')->first();
       $status=  LeadStatusController::index();
       
       $private_leads=\DB::table('leads')
               ->join('leads_status', 'leads.status_id','=','leads_status.id')
               ->join('leads_sources','leads.lead_source_id','=','leads_sources.id')
               ->join('contacts','contacts.id','=','leads.contact_id')
               ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
               ->leftJoin('private_entities','contacts.id','=','private_entities.entity_id')
               ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id','=',Type::where('type', 'Contact')->first()->id);
                })
               ->select('contacts.*','contacts.id AS id' ,'leads.status_id','leads.description','leads.lead_source_id',
                       'leads_sources.source','leads_status.status','leads.id AS lead_id', 'profile_pictures.path')
               ->where('leads.deleted_at',NULL)
               ->whereNull('contacts.deleted_at')
               ->where('contacts.contact_type_id',$contact_type->id)
               ->where('contacts.permission_id',$private_id)
               ->where('private_entities.type_id',$type->id)
               ->where('private_entities.user_id',$user_id);
       $assigned_leads=\DB::table('leads')
               ->join('leads_status', 'leads.status_id','=','leads_status.id')
               ->join('leads_sources','leads.lead_source_id','=','leads_sources.id')
               ->join('contacts','contacts.id','=','leads.contact_id')
               ->leftJoin('assigned_entities','contacts.id','=','assigned_entities.entity_id')
               ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
               ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id','=',Type::where('type', 'Contact')->first()->id);
                })
               ->select('contacts.*','contacts.id AS id' ,'leads.status_id','leads.description','leads.lead_source_id',
                       'leads_sources.source','leads_status.status','leads.id AS lead_id', 'profile_pictures.path')
               ->where('leads.deleted_at',NULL)
               ->whereNull('contacts.deleted_at')
               ->where('contacts.contact_type_id',$contact_type->id)
               ->where('contacts.permission_id',$assigned_id)
               ->where('assigned_entities.type_id',$type->id)
               ->where('assigned_entities.user_id',$user_id);
               
               
       $leads=\DB::table('leads')
               ->join('leads_status', 'leads.status_id','=','leads_status.id')
               ->join('leads_sources','leads.lead_source_id','=','leads_sources.id')
               ->join('contacts','contacts.id','=','leads.contact_id')
               ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
               ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id','=',Type::where('type', 'Contact')->first()->id);
                })
               ->select('contacts.*','contacts.id AS id' ,'leads.status_id','leads.description','leads.lead_source_id',
                       'leads_sources.source','leads_status.status','leads.id AS lead_id', 'profile_pictures.path')
               ->where('leads.deleted_at',NULL)
               ->whereNull('contacts.deleted_at')
               ->where('contacts.contact_type_id',$contact_type->id)
               ->where('contacts.permission_id',$public_id)
               ->union($private_leads)
               ->union($assigned_leads)
               ->get();
       
       $organizations=  Organization::all();
       $salutations=SalutationController::index();
       $sources=  LeadSourceController::index();

       $conversations=\DB::table('email_messages')
                ->leftJoin('email_attachments','email_messages.id','=','email_attachments.email_id')
                ->select('email_messages.*','email_messages.id AS id','email_attachments.*','email_attachments.id AS attach_id')
                
                 ->where('email_messages.type_id','=',$type->id)
               ->orderBy('email_messages.created_at','DESC')
                ->get();

       $phone_natures=Phone_NatureController::index();
       $email_natures=Email_NatureController::index();
       $users= AuthenticateController::index();
       
       $notes=\DB::table('notes')
            ->leftJoin('users','users.id','=','notes.user_id')
            ->where('type_id',$type->id)
            ->whereNull('notes.deleted_at')
            ->select('notes.*','users.name')
               
            ->get();
       $documents=Document::where('type_id',$type->id)            
               ->get();
       $phone_numbers=Phone_number::where('type_id',$type->id)->get();
       $dates=  Dates_to_remember::where('type_id',$type->id)->get();
       $emails=Email::where('type_id',$type->id)->get();
       $address=Address::where('type_id',$type->id)->get();
       $tasks= \DB::table('tasks')
                ->join('pinned_tasks','tasks.id','=','pinned_tasks.task_id')
               ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->where('pinned_tasks.type_id',$type->id)
                ->whereNull('tasks.deleted_at')
                ->select('tasks.name','tasks.id','tasks.start_date','pinned_tasks.entity_id','tasks_status.status')
               ->orderBy('tasks_status.id','ASC')
                ->orderBy('tasks.created_at','DESC')
                ->distinct()
                ->get();
       $events=\DB::table('events')
                ->join('pinned_events','events.id','=','pinned_events.event_id')
                ->where('pinned_events.type_id',$type->id)
                ->whereNull('events.deleted_at')
                ->select('events.title','events.id','events.start','events.end','pinned_events.entity_id')
                ->distinct()
                ->get();
       $owners=\DB::table('users')
                 ->join('owners','owners.user_id','=','users.id')
                 ->select('users.*','owners.id AS owner_id','owners.entity_id')
                 
                 ->where('owners.type_id',$type->id)
                 ->get();
       
       $usser=AuthenticateController::index();
       
       return response()->json(['leads'=>$leads,
           'status'=>$status,
           'salutations'=>$salutations,
           'organizations'=>$organizations,
           'sources'=>$sources,
           'type'=>$type,
           'notes'=>$notes,
           'phone_natures'=>$phone_natures,
           'email_natures'=>$email_natures,
           'users'=>$users,
           'contact_type'=>$contact_type,
           'address'=>$address,
           'phone_numbers'=>$phone_numbers,
           'dates'=>$dates,
           'emails'=>$emails,
           'organizations'=>$organizations,
           'tasks'=>$tasks,
           'events'=>$events,
           'update_lead'=>$contact_update,
           'owners'=>$owners,

           'conversations'=>$conversations,
           'assigned_id'=>$assigned_id,
           'usser'=>$usser,
           'documents'=>$documents,
           'doc_type'=>Type::where('type','Document')->first()->id]);

          

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
       // $lead=Lead::create($request->all());
       
        
        if($request->has('source')&& sizeof($input['source'])>0){
            $source=Lead_source::create($input);
            $input['lead_source_id']=$source->id;
        }
        
        
        $lead=Lead::create($input);
         return response()->json(['lead'=>$lead]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact=\DB::table('leads')
               ->join('leads_status', 'leads.status_id','=','leads_status.id')
               ->join('leads_sources','leads.lead_source_id','=','leads_sources.id')
               ->join('contacts','contacts.id','=','leads.contact_id')
               ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
               ->select('contacts.*','contacts.id AS id','leads.id AS lead_id' ,'leads.status_id','leads.description','leads.lead_source_id',
                       'leads_sources.source','leads_status.status','organizations.name')
               ->where('leads.deleted_at',NULL)
                ->where('contacts.id',$id)
               ->first();
       $salutations=SalutationController::index();
        $organizations=OrganizationController::index();
       $sources=  LeadSourceController::index();
       $stages=  LeadStatusController::index();
        $phone_natures=Phone_NatureController::index();
        $email_natures=Email_NatureController::index();
        $type_id=Type::where('type','Contact')->first();
         $dates=  Dates_to_remember::where('entity_id',$id)->where('type_id',$type_id->id)->get();
       $source=  Lead_source::find($contact->lead_source_id);
 
         $contact_organization=Organization::find($contact->organization_id);
       $contact_phones=\DB::table('phone_numbers')
                            ->leftJoin('phone_natures','phone_numbers.phone_nature_id','=','phone_natures.id')
                            ->where('entity_id',$contact->id)
                            ->where('type_id',$type_id->id)
                            ->select('phone_numbers.id as phone_id','phone_nature_id','phone_natures.*','phone_number')
                            ->get();
       $contact_emails=Email::where('entity_id',$id)->where('type_id',$type_id->id)->get();
        $contact_emails=\DB::table('emails')
                            ->leftJoin('email_natures','emails.email_nature_id','=','email_natures.id')
                            ->where('entity_id',$id)
                            ->where('type_id',$type_id->id)
                            ->select('emails.id as email_id','email_nature_id','email_natures.*','email')
                            ->get();
        $tasks= \DB::table('tasks')
                ->join('pinned_tasks','tasks.id','=','pinned_tasks.task_id')
                ->where('pinned_tasks.type_id',$type_id->id)
                ->where('pinned_tasks.entity_id',$id)
                ->select('tasks.name','tasks.id','tasks.start_date')
                ->distinct()->get();
       $contact_salutation=Salutation::where('id',$contact->salutation_id)->first();
       $address=Address::where('type_id',$type_id->id)->where('entity_id',$id)->first();
        return response()->json(array('success'=>true, 'contact'=>$contact,
            'salutations'=>$salutations,
            'organizations'=>$organizations,
           'contact_salutation'=>$contact_salutation,
            'phone_natures'=>$phone_natures,
            'email_natures'=>$email_natures,
           'contact_organization'=>$contact_organization,
            'contact_phones'=>$contact_phones, 
            'contact_emails'=>$contact_emails,
            'type_id'=>$type_id,
            'tasks'=>$tasks,
            'dates'=>$dates,
               'sources'=>$sources,
           'source'=>$source,
            'stages'=>$stages,
            'address'=>$address
                ));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $input=$request->all();
         $qualified=Lead_status::where('status','Qualified')->first();
         $contact_update=  Contact_Type::where('contact_type','Customer')->first();
         $lead=Lead::find($id);
        
        if($request->has('source')&& sizeof($input['source'])>0){
            $source=Lead_source::create($input);
            $input['lead_source_id']=$source->id;
        }
        if($request->has('status_id')){
            if($input['status_id']===$qualified->id){
                $contact=Contact::where('id',$lead->contact_id)->first();
                $contact->contact_type_id=$contact_update->id;
                $contact->save();
            }
        }
        
        
        
        $lead->update($request->all());
        
        return response()->json(['lead'=>$lead]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lead::destroy($id);
        return response()->json(['deleted'=>true]);
        
    }
}
