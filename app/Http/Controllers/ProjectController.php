<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Product;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    public function index()
    {
        $projects=Project::all();
        $type=Type::where('type','Project')->first();
        $notes=\DB::table('notes')
                    ->leftJoin('users','users.id','=','notes.user_id')
                    ->where('type_id',$type->id)
                    ->whereNull('notes.deleted_at')
                    ->select('notes.note','notes.id as note_id','notes.entity_id','users.name')
                    ->get();
        $users=  AuthenticateController::index();
        $products=Product::all();
        $product_cash=\DB::table('products')
              ->leftJoin('payment_plans','products.id','=','payment_plans.product_id')
              ->leftJoin('plan_status','payment_plans.plan_status_id','=','plan_status.id')
              ->select('products.id','payment_plans.id as pay_id','payment_plans.deposit','payment_plans.installment',
                      'payment_plans.installment_no','plan_status.id as status_id','plan_status.status','payment_plans.product_id')
              ->whereNull('products.deleted_at')
              ->whereNull('payment_plans.deleted_at')
             
              ->where('payment_plans.installment_no','=',0)
              ->get();
        $product_installments=\DB::table('products')
              ->leftJoin('payment_plans','products.id','=','payment_plans.product_id')
              ->leftJoin('plan_status','payment_plans.plan_status_id','=','plan_status.id')
              ->select('products.id','payment_plans.id as pay_id','payment_plans.deposit',
                      'payment_plans.installment_no','payment_plans.installment','plan_status.id as status_id','plan_status.status','payment_plans.product_id')
              ->whereNull('products.deleted_at')
              ->whereNull('payment_plans.deleted_at')
              
              ->where('payment_plans.installment_no','>',0)
              ->get();
        
        return response()->json(['projects'=>$projects,
                                 'type'=>$type->id,
                                 'notes'=>$notes,
                                    'users'=>$users,
                                    'products'=>$products,
                                    'cash'=>$product_cash,
                                    'installments'=>$product_installments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Project::create($request->all());
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project=Project::find($id);
        return response()->json(['project'=>$project]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project=Project::find($id);
        $project->update($request->all());
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::destroy($id);
        return response()->json(['success'=>true]);
        
    }
}
