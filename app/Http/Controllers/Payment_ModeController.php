<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment_Mode;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Payment_ModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        //return all the email nature to the view success value
        $payment_modes=Payment_Mode::all();
        return $payment_modes;
//        return response()->json(array('success'=>true,'payment_modes'=>$payment_modes));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Payment_Mode::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment_mode=Payment_Mode::find($id);
        return response()->json(array('success'=>true, 'payment_mode'=>$payment_mode));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $payment_mode=Payment_Mode::find($id);
        $payment_mode->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Payment_Mode::destroy($id);
        
        //this will permanently delete the item for db
        //$payment_modes->forcedelete();
        return response()->json(array('success'=>true));
    }
}
