<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Receipt;
use App\Permission;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    
    public static function index()
    {
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;

        //Get the type id that is equal to Contact
        $type_id = Type::where('type', 'Invoice')->first()->id;

        //return all the contactes to the view success value
        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;
        //return all the addresses to the view success value
        //return all the email nature to the view success value
        $private_receipts=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('contacts','contacts.id','=','opportunities.customer_id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->join('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->leftJoin('private_entities', function ($join) {
                    $join->on('receipts.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                 ->select('invoices.*','contacts.firstname','contacts.lastname',
                         'products.name',\DB::raw('sum(opp_inventory.value) as total'),
                         'payment_modes.payment_mode',
                         'payment_reasons.payment_reason','opportunities.name','receipts.amount')
                ->where('receipts.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at');
        $assigned_receipts=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('contacts','contacts.id','=','opportunities.customer_id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->join('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->leftJoin('assigned_entities', function ($join) {
                    $join->on('receipts.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                 ->select('invoices.*','contacts.firstname','contacts.lastname',
                         'products.name',\DB::raw('sum(opp_inventory.value) as total'),
                         'payment_modes.payment_mode',
                         'payment_reasons.payment_reason','opportunities.name','receipts.amount')
                ->where('receipts.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at');
        $receipts=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('contacts','contacts.id','=','opportunities.customer_id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->join('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->select('invoices.*','contacts.firstname','contacts.lastname',
                         'products.name',\DB::raw('sum(opp_inventory.value) as total'),
                         'payment_modes.payment_mode',
                         'payment_reasons.payment_reason','opportunities.name','receipts.amount')
                 ->where('receipts.permission_id',$public_id)
                 ->union($private_receipts)
                 ->unioun($assigned_receipts)
                 ->groupBy('receipts.id')
                 ->get();
        $users = AuthenticateController::index();
//        return $receipts;
        return response()->json(array('success'=>true,
            'receipts'=>$receipts,
            'assigned_id'=>$assigned_id,
            'users'=>$users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $receipt=Receipt::create($input);
        return response()->json(array('success'=>true,'last_receipt'=>$receipt->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receipt=Receipt::find($id);
        return response()->json(array('success'=>true, 'receipt'=>$receipt));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $receipt=Receipt::find($id);
        $receipt->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Receipt::destroy($id);
        
        //this will permanently delete the item for db
        //$receipts->forcedelete();
        return response()->json(array('success'=>true));
    }
}
