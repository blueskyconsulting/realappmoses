<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\oppProducts;

class oppProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // save the contact
//        /$data=  oppProducts::all();
        $products_in_opportunity= \DB::table('opp_inventory')
                ->leftjoin('product_inventories','product_inventories.id','=','opp_inventory.inventory_id')
                ->select('opp_inventory.*','product_inventories.property_no','product_inventories.product_id')
                ->whereNull('opp_inventory.deleted_at')
                ->get();
        return response()->json(['opp_prods'=>$products_in_opportunity]); 
    }

    /**t
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // save the contact
        $input=$request->all();
        oppProducts::create($input);
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        oppProducts::destroy($id);     
       
        return response()->json(array('success'=>true));
    }
}
