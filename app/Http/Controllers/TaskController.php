<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Contact_Type;
use App\Note;
use App\Event;
use App\Dates_to_remember;
use App\Permission;
use App\Document;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class TaskController extends Controller {
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        
        $type = Type::where('type', 'Task')->first();
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id; 
        //Get the private permission id
        $private_id=Permission::where('permission','Private')->first()->id;
        //Get the public permission id
        $public_id=Permission::where('permission','Public')->first()->id;
        //Get the assigned id
        $assigned_id=Permission::where('permission','Assigned')->first()->id;
        
        $private_tasks = \DB::table('tasks')
                ->join('priority_types', 'priority_types.id', '=', 'tasks.priority_type_id')
                ->join('task_types', 'task_types.id', '=', 'tasks.task_type')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->leftJoin('private_entities','tasks.id','=','private_entities.entity_id')
                ->select('tasks.*', 'task_types.Type', 'tasks_status.status', 'priority_types.type')
                ->whereNull('tasks.deleted_at')
                ->where('tasks.permission_id',$private_id)
                ->where('private_entities.type_id',$type->id)
                ->where('private_entities.user_id',$user_id);
        
        $assigned_tasks = \DB::table('tasks')
                ->join('priority_types', 'priority_types.id', '=', 'tasks.priority_type_id')
                ->join('task_types', 'task_types.id', '=', 'tasks.task_type')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->leftJoin('assigned_entities','tasks.id','=','assigned_entities.entity_id')
                ->select('tasks.*', 'task_types.Type', 'tasks_status.status', 'priority_types.type')
                ->whereNull('tasks.deleted_at')
                ->where('tasks.permission_id',$assigned_id)
                ->where('assigned_entities.type_id',$type->id)
                ->where('assigned_entities.user_id',$user_id)
                ->orderBy('tasks_status.id','ASC')
                ->orderBy('tasks.created_at','DESC');
        
        
        $tasks = \DB::table('tasks')
                ->join('priority_types', 'priority_types.id', '=', 'tasks.priority_type_id')
                ->join('task_types', 'task_types.id', '=', 'tasks.task_type')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->select('tasks.*', 'task_types.Type', 'tasks_status.status', 'priority_types.type')
                ->where('tasks.permission_id',$public_id)
                ->whereNull('tasks.deleted_at')
                ->orderBy('tasks_status.id','ASC')
                ->orderBy('tasks.created_at','DESC')
                ->union($private_tasks)
                ->union($assigned_tasks)
                ->get();
        $owners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $type->id)
                ->get();
        $status = TaskStatusController::index();
        $task_types = TaskTypeController::index();
        $priorities = PriorityTypeController::index();
        $orgtype = Type::where('type', 'Organization')->first();
        $orgtasks = \DB::table('pinned_tasks')
                ->join('tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->join('organizations','organizations.id','=','pinned_tasks.entity_id')
                ->where('pinned_tasks.type_id', $orgtype->id)
                ->whereNull('organizations.deleted_at')
                ->select('organizations.name AS orgname', 'tasks.id AS id')
                ->distinct()
                ->get();
        $optype = Type::where('type', 'Opportunity')->first();
        $optasks = \DB::table('pinned_tasks')
                ->join('tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->join('opportunities','opportunities.id','=','pinned_tasks.entity_id')
                ->where('pinned_tasks.type_id', $optype->id)
                ->whereNull('opportunities.deleted_at')
                ->select('opportunities.name', 'tasks.id AS id')
                ->distinct()
                ->get();
        
        $contact_type = Type::where('type', 'Contact')->first();
        $lead_type=  Contact_Type::where('contact_type','Lead')->first();
        $contact_tasks = \DB::table('pinned_tasks')
                ->join('tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->join('contacts','contacts.id','=','pinned_tasks.entity_id')
                ->where('pinned_tasks.type_id', $contact_type->id)
                ->where('contacts.contact_type_id','!=', $lead_type->id)
                ->whereNull('contacts.deleted_at')
                ->select('contacts.firstname','contacts.lastname', 'tasks.id AS id')
                ->distinct()
                ->get();
        $lead_tasks = \DB::table('pinned_tasks')
                ->join('tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->join('contacts','contacts.id','=','pinned_tasks.entity_id')
                ->where('pinned_tasks.type_id', $contact_type->id)
                ->where('contacts.contact_type_id','=', $lead_type->id)
                ->whereNull('contacts.deleted_at')
                ->select('contacts.firstname','contacts.lastname', 'tasks.id AS id')
                ->distinct()
                ->get();
        $users = AuthenticateController::index();
        $ussers = AuthenticateController::index();
        $notes=\DB::table('notes')
                        ->leftJoin('users','users.id','=','notes.user_id')
                        ->where('type_id',$type->id)
                        ->whereNull('notes.deleted_at')
                        ->select('notes.note','notes.id as note_id','notes.entity_id','users.name')
                        ->get();
        $documents=Document::where('type_id',$type->id)->get();
        return response()->json(array(
                    'success' => true,
                    'status' => $status,
                    'task_types' => $task_types,
                    'tasks' => $tasks,
                    'priorities' => $priorities,
                    'users' => $users,
                    'type' => $type,
                    'owners' => $owners,
                    'lead_tasks'=>$lead_tasks,
                    'contact_tasks'=>$contact_tasks,
                    'orgtasks'=>$orgtasks,
                    'optasks'=>$optasks,
                    'notes'=>$notes,
                    'ussers'=>$ussers,
                    'assigned_id'=>$assigned_id,
                    'documents'=>$documents,
                    'doc_type'=>Type::where('type','Document')->first()->id
                ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $task = Task::create($request->all());
        $task_type=Type::where('type','Task')->first();
        return response()->json(array('success' => true, 
            'task' => $task,
            'task_type'=>$task_type->id
            ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $task = \DB::table('tasks')
                ->join('priority_types', 'priority_types.id', '=', 'tasks.priority_type_id')
                ->join('task_types', 'task_types.id', '=', 'tasks.task_type')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->select('tasks.*', 'task_types.Type', 'tasks_status.id AS status_id', 'priority_types.type')
                ->whereNull('tasks.deleted_at')
                ->where('tasks.id',$id)
                ->first();
        return response()->json(array('task' => $task));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $task = Task::find($id);
        $task->update($request->all());
        return response()->json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $task = Task::destroy($id);
        return response()->json(array('success' => true));
    }
     public function notification() {
        $events=Event::where('today',1)->get();
        $orgtype = Type::where('type', 'organization')->first();
        $contact_type = Type::where('type', 'Contact')->first();
        $leadtype = Contact_Type::where('contact_type', 'Lead')->first();
        $orgdates=\DB::table('organizations')
                ->join('dates_to_remember','dates_to_remember.entity_id','=','organizations.id')
                ->select('organizations.name','organizations.id AS id','dates_to_remember.entity_id','dates_to_remember.description','dates_to_remember.date')
                ->where('dates_to_remember.type_id',$orgtype->id)
                ->where('dates_to_remember.today',1)
                ->get();
        $contactdates=\DB::table('contacts')
                ->join('dates_to_remember','dates_to_remember.entity_id','=','contacts.id')
                ->select('contacts.firstname','contacts.lastname','dates_to_remember.entity_id','dates_to_remember.description','dates_to_remember.date')
                ->where('dates_to_remember.type_id',$contact_type->id)
                ->where('contact_type_id','!=',$leadtype->id)
                ->where('dates_to_remember.today',1)
                ->get();
        $leaddates=\DB::table('contacts')
                ->join('dates_to_remember','dates_to_remember.entity_id','=','contacts.id')
                ->select('contacts.firstname','contacts.lastname','dates_to_remember.entity_id','dates_to_remember.description','dates_to_remember.date')
                ->where('dates_to_remember.type_id',$contact_type->id)
                ->where('contact_type_id',$leadtype->id)
                ->where('dates_to_remember.today',1)
                ->get();
        $tasks=Task::where('today',1)->get();
        return response()->json(['events'=>$events,
            'orgdates'=>$orgdates,
            'contact_dates'=>$contactdates,
            'lead_dates'=>$leaddates,
            'tasks'=>$tasks]);
    }

}
