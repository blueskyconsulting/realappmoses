<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Document;
use Illuminate\Support\Facades\Storage;
use App\Type;
use App\Permission;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct() {
        $this->middleware('jwt.auth');
    }
    
    public function index()
    {
        //return all the documents to the view success value
        $user_id = JWTAuth::parseToken()->authenticate()->id;

        //Get the type id that is equal to Contact
        $type_id = Type::where('type', 'Invoice')->first()->id;

        //return all the contactes to the view success value
        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;
        
        $types=Type::all();
        $private_documents=\DB::table('documents')
                ->leftJoin('types','types.id','=','documents.type_id')
                ->leftJoin('private_entities', function ($join) {
                    $join->on('documents.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Document')->first()->id);
                })
                ->select('documents.id','documents.document_name','documents.path','types.type')
                ->where('documents.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->whereNull('documents.deleted_at');
        $assigned_documents=\DB::table('documents')
                ->leftJoin('types','types.id','=','documents.type_id')
                ->leftJoin('assigned_entities', function ($join) {
                    $join->on('documents.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Document')->first()->id);
                })
                ->select('documents.id','documents.document_name','documents.path','types.type')
                ->where('documents.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->whereNull('documents.deleted_at');
        $documents=\DB::table('documents')
                ->leftJoin('types','types.id','=','documents.type_id')
                ->select('documents.id','documents.document_name','documents.path','types.type')
                ->where('documents.permission_id',$public_id)
                ->whereNull('documents.deleted_at')
                ->union($private_documents)
                ->union($assigned_documents)
                ->get();
        $users = AuthenticateController::index();
        return response()->json(array('documents'=>$documents,
            'types'=>$types,
            'assigned_id'=>$assigned_id,
            'users'=>$users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $document=new Document;
        $document->entity_id=$input['info']['entity_id'];
        $document->type_id=$input['info']['type_id'];
        $document->document_name=$input['info']['document_name'];
        $document->permission_id=$input['info']['permission_id'];
        $extension=$request->file('file')->getClientOriginalExtension();
        $filename=sha1(time().$input['info']['entity_id'].$input['info']['type_id'].  rand(0, 999999)).'.'.$extension;
        $request->file('file')->move('uploads/',$filename
        );
        $document->path='uploads/'.$filename;
        $document->save();
        return response()->json(array('success'=>true,'lastdoc'=>$document->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document=Document::find($id);
        return response()->json(array('success'=>true, 'document'=>$document));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $document=Document::find($id);
        $document->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Document::destroy($id);
        
        //this will permanently delete the item for db
        //$document_natures->forcedelete();
        return response()->json(array('success'=>true));
    }
}
