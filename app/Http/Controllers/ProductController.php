<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Project;
use App\Type;
use App\Payment_Plan;
use App\Plan_Status;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects=Project::all();
        $type_id=Type::where('type','Product')->first();
        $products=\DB::table('products')
                      ->join('projects','projects.id','=','products.project_id')
                      ->select('products.*','projects.name as project_name','projects.description')
                      ->whereNull('products.deleted_at')
                      ->get();
        $active_id=Plan_Status::where('status','Active')->first()->id;
        $inactive_id=Plan_Status::where('status','Not Active')->first()->id;
        return response()->json(array('products'=>$products,
                                'projects'=>$projects,
                                'type_id'=>$type_id->id,
                                'active_id'=>$active_id,
                                'inactive_id'=>$inactive_id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $product=Product::create($input);
        return response()->json(array('success'=>true,'last_product_id'=>$product->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type_id=Type::where('type','Product')->first();
        $product=Product::find($id);
        $product_cash=\DB::table('products')
              ->leftJoin('payment_plans','products.id','=','payment_plans.product_id')
              ->leftJoin('plan_status','payment_plans.plan_status_id','=','plan_status.id')
              ->select('products.id','payment_plans.id as pay_id','payment_plans.deposit','payment_plans.installment',
                      'payment_plans.installment_no','plan_status.id as status_id','plan_status.status')
              ->whereNull('products.deleted_at')
              ->whereNull('payment_plans.deleted_at')
              ->where('products.id',$id)
              ->where('payment_plans.installment_no','=',0)
              ->get();
        $product_installments=\DB::table('products')
              ->leftJoin('payment_plans','products.id','=','payment_plans.product_id')
              ->leftJoin('plan_status','payment_plans.plan_status_id','=','plan_status.id')
              ->select('products.id','payment_plans.id as pay_id','payment_plans.deposit',
                      'payment_plans.installment_no','payment_plans.installment','plan_status.id as status_id','plan_status.status')
              ->whereNull('products.deleted_at')
              ->whereNull('payment_plans.deleted_at')
              ->where('products.id',$id)
              ->where('payment_plans.installment_no','>',0)
              ->get();
        $product_notes=\DB::table('notes')
                        ->leftJoin('users','users.id','=','notes.user_id')
                        ->where('entity_id',$id)
                        ->where('type_id',$type_id->id)
                        ->whereNull('notes.deleted_at')
                        ->select('notes.note','notes.id as note_id','users.name')
                        ->get();
        $product_inventories=\DB::table('product_inventories')
                      ->join('products','products.id','=','product_inventories.product_id')
                      ->join('projects','projects.id','=','products.project_id')
                      ->join('payment_plans','payment_plans.product_id','=','product_inventories.product_id')
                      ->join('inventory_status','product_inventories.inventory_status_id','=','inventory_status.id')
                      ->select('products.name','products.size','projects.name as project_name',
                              'payment_plans.deposit','projects.project_type','product_inventories.*'
                              ,'inventory_status.status')
                      ->whereNull('product_inventories.deleted_at')
                      ->where('payment_plans.installment_no','=',0)
                      ->where('product_inventories.product_id',$id)
                      ->groupBy('product_inventories.id')
                      ->get();
        return response()->json(array(
                'product_cash'=>$product_cash,
                'product_installments'=>$product_installments,
                'product'=>$product,
                'product_notes'=>$product_notes,
                'product_inventories'=>$product_inventories));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $product=Product::find($id);
        $product->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return response()->json(array('success'=>true));
    }
}
