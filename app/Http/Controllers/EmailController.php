<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Email;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Mail;
use Mailgun\Mailgun;
use App\EmailMessage;
use App\Type;
use App\Contact_Type;
use App\Attachment;
class EmailController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function __construct()
//    {
//        $this->middleware('jwt.auth');
//    }

    public function index() {
        //return all the emails to the view success value
        $emails = Email::all();
        return response()->json(array('success' => true, 'emails' => $emails));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();
        Email::create($input);
        return response()->json(array('success' => true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $email = Email::find($id);
        return response()->json(array('success' => true, 'email' => $email));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $email = Email::find($id);
        $email->update($input);
        return response()->json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //this will soft delete ie update the deleted_at column of the DB
        Email::destroy($id);

        //this will permanently delete the item for db
        //$email_natures->forcedelete();
        return response()->json(array('success' => true));
    }

    public function sendMail(Request $request) {
        $input = $request->all();
        
        $at=[];
        if($input['attach_ids']){
        for($j=0;$j<count($input['attach_ids']);$j++){
            $attachment= Attachment::find($input['attach_ids'][$j]);
            $at[$j]=$attachment->url;
        }
      }
        $mail = Mail::send('emails.usermail', ['usermessage' => $input['message']], function ($m) use($input,$at) {
                    $m->from($input['from'], $input['sender']);

                    $m->to($input['to'], $input['receiver'])->subject($input['subject']);
                    
                    if(count($at)){
//                       foreach ($at as $a){
//                        $m->attach($a->url);
//                    }; 
                        for($i=0;$i<count($at);$i++){
                            $m->attach($at[$i]);
                        }
                    }
                    
                    
                });
        if ($mail) {
            $message=EmailMessage::create([
                'from'=>$input['from'],
                'to'=>$input['to'],
                'subject'=>$input['subject'],
                'status'=>'sent',
                'body_text'=> $input['message'],
                'entity_id'=>$input['entity_id'],
                'type_id'=>$input['type_id']
            ]);
            return response()->json(['success' => true,'email_id'=>$message->id]);
        }
    }
    public function uploadAttachment(Request $request){
         $input=$request->all();
         
        $document=new Attachment;
        $document->filename=$request->file('file')->getClientOriginalName();
        $extension=$request->file('file')->getClientOriginalExtension();
        $filename=$document->filename;
        $request->file('file')->move('uploads/attachments/',$filename
        );
        $document->url='uploads/attachments/'.$filename;
        $document->save();
        return response()->json(array('success'=>true,'attach_id'=>$document->id));
        
    }

   



}
