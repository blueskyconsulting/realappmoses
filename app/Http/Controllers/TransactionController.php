<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opportunities;
use App\Pipeline_Stages;
use App\Type;
use App\Invoice;
use App\Payment_Mode;
use App\Permission;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class TransactionController extends Controller {
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;

        //Get the type id that is equal to Contact
        $type_id = Type::where('type', 'Invoice')->first()->id;

        //return all the contactes to the view success value
        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;
        //return all the addresses to the view success value
        //return all the email nature to the view success value
        
        
        $org_type=Type::where('type','Organization')->first()->id;
        $contact_type=Type::where('type','Contact')->first()->id;
        $cheque_payment=Payment_Mode::where('payment_mode','Cheque')->first()->id;
        $receipt_type_id = Type::where('type', 'Receipt')->first()->id;
        $invoice_type_id = Type::where('type', 'Invoice')->first()->id;
        $pipeline_id = Pipeline_Stages::where('stage_name', 'Close')->first()->id;
        $won_oppor = \DB::select(
                        \DB::raw('SELECT opp.id AS id, opp.name, opp.description,
                                    opp.close_date, opp.deleted_at,
                                    changes.current_stage_id
                                    FROM opportunities AS opp
                                    JOIN pipeline_state_changes AS changes ON 
                                    changes.opportunity_id = opp.id
                                    WHERE changes.id = (SELECT MAX( id ) 
                                    FROM pipeline_state_changes
                                    WHERE pipeline_state_changes.opportunity_id = opp.id
                                    AND pipeline_state_changes.current_stage_id ='.$pipeline_id.')'));
        $payment_reasons = \App\Payment_Reason::all();
        $payment_modes = \App\Payment_Mode::all();
        $invoice_opp = \DB::table('invoices')
                ->join('opportunities', 'invoices.opportunity_id', '=', 'opportunities.id')
                ->select('invoices.id', 'opportunities.name')
                ->get();
        $private_receipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->leftJoin('private_entities', function ($join) {
                    $join->on('receipts.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                ->select('receipts.*', 'contacts.firstname', 'contacts.lastname', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$contact_type)
                ->where('receipts.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at')
                ->groupBy('receipts.id');
        $assigned_receipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->leftJoin('assigned_entities', function ($join) {
                    $join->on('receipts.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                ->select('receipts.*', 'contacts.firstname', 'contacts.lastname', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$contact_type)
                ->where('receipts.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at')
                ->groupBy('receipts.id');
        $receipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->select('receipts.*', 'contacts.firstname', 'contacts.lastname', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$contact_type)
                ->where('receipts.permission_id',$public_id)
                ->union($private_receipts)
                ->union($assigned_receipts)
                ->groupBy('receipts.id')
                ->get();
        $orgprivate_receipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->leftJoin('private_entities', function ($join) {
                    $join->on('receipts.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                ->select('receipts.*', 'organizations.name As org_name', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$org_type)
                ->where('receipts.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at')
                ->groupBy('receipts.id');
        $orgassigned_receipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->leftJoin('assigned_entities', function ($join) {
                    $join->on('receipts.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Receipt')->first()->id);
                 })
                ->select('receipts.*', 'organizations.name As org_name', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$org_type)
                ->where('receipts.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->whereNull('receipts.deleted_at')
                ->groupBy('receipts.id');
        $orgreceipts = \DB::table('receipts')
                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
                ->select('receipts.*','organizations.name As org_name', 
                        'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total'), 
                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
                ->where('opportunities.type_id',$org_type)
                ->where('receipts.permission_id',$public_id)
                ->union($orgprivate_receipts)
                ->union($orgassigned_receipts)
                ->groupBy('receipts.id')
                ->get();
//        $orgreceipts = \DB::table('receipts')
//                ->join('invoices', 'invoices.id', '=', 'receipts.invoice_id')
//                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
//                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
//                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
//                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
//                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
//                ->join('products', 'products.id', '=', 'payment_plans.product_id')
//                ->join('payment_modes', 'payment_modes.id', '=', 'receipts.payment_mode_id')
//                ->join('payment_reasons', 'payment_reasons.id', '=', 'receipts.payment_reason_id')
//                ->select('receipts.*', 'organizations.name As org_name',  
//                        'products.name', 
//                        \DB::raw('sum(opp_inventory.value) as total'), 
//                        'payment_modes.payment_mode', 'payment_reasons.payment_reason')
//                ->groupBy('receipts.id')
//                ->where('opportunities.type_id',$org_type)
//                ->get();
        $private_invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->leftJoin('private_entities', function ($join) {
                    $join->on('invoices.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Invoice')->first()->id);
                 })
                ->select('invoices.*', 'contacts.firstname', 'contacts.lastname', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->where('opportunities.type_id',$contact_type)
                ->groupBy('invoices.id');
        $assigned_invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->leftJoin('assigned_entities', function ($join) {
                    $join->on('invoices.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Invoice')->first()->id);
                 })
                ->select('invoices.*', 'contacts.firstname', 'contacts.lastname', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->where('opportunities.type_id',$contact_type)
                ->groupBy('invoices.id');
        $invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->select('invoices.*', 'contacts.firstname', 'contacts.lastname', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$public_id)
                ->where('opportunities.type_id',$contact_type)
                ->groupBy('invoices.id')
                ->union($private_invoices)
                ->union($assigned_invoices)
                ->get();
        $orgprivate_invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->leftJoin('private_entities', function ($join) {
                    $join->on('invoices.id', '=', 'private_entities.entity_id')
                    ->where('private_entities.type_id','=',Type::where('type', 'Invoice')->first()->id);
                 })
                ->select('invoices.*', 'organizations.name As org_name', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$private_id)
                ->where('private_entities.user_id', $user_id)
                ->where('opportunities.type_id',$org_type)
                ->groupBy('invoices.id');
        $orgassigned_invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->leftJoin('assigned_entities', function ($join) {
                    $join->on('invoices.id', '=', 'assigned_entities.entity_id')
                    ->where('assigned_entities.type_id','=',Type::where('type', 'Invoice')->first()->id);
                 })
                ->select('invoices.*','organizations.name As org_name', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$assigned_id)
                ->where('assigned_entities.user_id', $user_id)
                ->where('opportunities.type_id',$org_type)
                ->groupBy('invoices.id');
        $orginvoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->select('invoices.*', 'organizations.name As org_name', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
                ->where('invoices.permission_id',$public_id)
                ->where('opportunities.type_id',$org_type)
                ->groupBy('invoices.id')
                ->union($orgprivate_invoices)
                ->union($orgassigned_invoices)
                ->groupBy('invoices.id')
                ->get();
//        $orginvoices = \DB::table('invoices')
//                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
//                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
//                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
//                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
//                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
//                ->join('products', 'products.id', '=', 'payment_plans.product_id')
//                ->select('invoices.*', 'organizations.name As org_name', 'products.name', \DB::raw('sum(opp_inventory.value) as total'))
//                ->groupBy('invoices.id')
//                ->where('opportunities.type_id',$org_type)
//                ->get();
        $dialog_invoices=Invoice::all();
        $users = AuthenticateController::index();
        return response()->json(array('receipts' => $receipts,
                    'invoices' => $invoices,
                    'won_oppor' => $won_oppor,
                    'payment_reasons' => $payment_reasons,
                    'payment_modes' => $payment_modes,
                    'invoice_opp' => $invoice_opp,
                    'invoice_type_id' => $invoice_type_id,
                    'receipt_type_id' => $receipt_type_id,
                    'dialog_invoices'=>$dialog_invoices,
                    'cheque_payment'=>$cheque_payment,
                    'orginvoices'=>$orginvoices,
                    'orgreceipts'=>$orgreceipts,
                    'assigned_id'=>$assigned_id,
                    'users'=>$users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
