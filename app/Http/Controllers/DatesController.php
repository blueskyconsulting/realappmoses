<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dates_to_remember;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Contact_Type;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class datesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function __construct()
//    {
//        $this->middleware('jwt.auth');
//    }
    public function index() {
        $dates = Dates_to_remember::all();
        return $dates;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $dates = Dates_to_remember::create($request->all());
        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $dates = Dates_to_remember::find($id);
        return response()->json(['success' => true,
                    'date' => $dates]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $dates = Dates_to_remember::find($id);
        $dates->update($request->all());
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Dates_to_remember::destroy($id);
        return response()->json(['success' => true]);
    }

    public function getDate($id) {
        $orgtype = Type::where('type', 'Organization')->first();

        $contact_type = Type::where('type', 'Contact')->first();

        $leadtype = Contact_Type::where('contact_type', 'Lead')->first();

        $orgdates = \DB::table('organizations')
                ->join('dates_to_remember', 'dates_to_remember.entity_id', '=', 'organizations.id')
                ->select('organizations.name', 'organizations.id AS id', 'dates_to_remember.entity_id', 'dates_to_remember.description', 'dates_to_remember.date')
                ->where('dates_to_remember.type_id', $orgtype->id)
                ->where('dates_to_remember.id', $id)
                ->first();


        $contactdates = \DB::table('contacts')
                ->join('dates_to_remember', 'dates_to_remember.entity_id', '=', 'contacts.id')
                ->select('contacts.firstname', 'contacts.lastname', 'dates_to_remember.entity_id', 'dates_to_remember.description', 'dates_to_remember.date')
                ->where('dates_to_remember.type_id', $contact_type->id)
                ->where('contact_type_id', '!=', $leadtype->id)
                ->where('dates_to_remember.id', $id)
                ->first();
        $leaddates = \DB::table('contacts')
                ->join('dates_to_remember', 'dates_to_remember.entity_id', '=', 'contacts.id')
                ->select('contacts.firstname', 'contacts.lastname', 'dates_to_remember.entity_id', 'dates_to_remember.description', 'dates_to_remember.date')
                ->where('dates_to_remember.type_id', $contact_type->id)
                ->where('contact_type_id', $leadtype->id)
                ->where('dates_to_remember.id', $id)
                ->first();
        if (!empty($orgdates)) {
            $date = $orgdates;
            $object = 'organization';
        } else if (!empty($contactdates)) {
            $date = $contactdates;
            $object = 'contact';
        } else if (!empty($leaddates)) {
            $date = $leaddates;
            $object = 'lead';
        }
        return response()->json(
                        ['object' => $object,
                            'date' => $date]);
    }

}
