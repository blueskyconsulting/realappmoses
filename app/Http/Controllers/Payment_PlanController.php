<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Payment_Plan;
use App\Plan_Status;
use App\Product;
class Payment_PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payment_plans=\DB::table('payment_plans')
                            ->leftJoin('plan_status','payment_plans.plan_status_id','=','plan_status.id')
                            ->join('products','payment_plans.product_id','=','products.id')
                            ->select('payment_plans.*','products.name','plan_status.status')
                            ->whereNull('payment_plans.deleted_at')
                            ->get();
        $products=Product::all();
        $plan_status=Plan_Status::all();
        $active_id=Plan_Status::where('status','Active')->first()->id;
        return response()->json(array('payment_plans'=>$payment_plans,
                                      'products'=>$products,
                                      'active_id'=>$active_id,
                                      'plan_status'=>$plan_status));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        if($request->has('newcash')){
            $active_cash=Payment_Plan::where('plan_status_id',Plan_Status::where('status','Active')->first()->id)
                                 ->where('payment_plans.installment_no','=',0)
                                 ->where('product_id',$input['product_id'])
                                 ->orderBy('id','DESC')
                                 ->first();
            if($active_cash){
                $active_cash->plan_status_id=Plan_Status::where('status','Not Active')->first()->id;
                $active_cash->save();
            }
        }
        Payment_Plan::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment_plan=Payment_Plan::find($id);
        return $payment_plan;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $payment_plan=Payment_Plan::find($id);
        $payment_plan->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Payment_Plan::destroy($id);
        return response()->json(array('success'=>true));
    }
}