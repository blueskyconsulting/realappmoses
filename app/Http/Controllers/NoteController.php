<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Note;
use App\Type;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return all the notess to the view success value
        $types=Type::all();
        $notes=\DB::table('notes')
            ->leftJoin('users','users.id','=','notes.user_id')
            ->leftJoin('types','notes.type_id','=','types.id')
            ->whereNull('notes.deleted_at')
            ->select('notes.note','notes.id as note_id','users.name','types.type')
            ->get();
//        $notes=Note::all();
        return response()->json(array('success'=>true,'notes'=>$notes,
                                       'types'=>$types));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Note::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note=Note::find($id);
        return response()->json(array('success'=>true, 'note'=>$note));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $note=Note::find($id);
        $note->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Note::destroy($id);
        
        //this will permanently delete the item for db
        //$notes_natures->forcedelete();
        return response()->json(array('success'=>true));
    }
}
