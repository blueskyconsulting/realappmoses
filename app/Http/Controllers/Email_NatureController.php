<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email_Nature;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Email_NatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        //return all the email nature to the view success value
        $email_natures=Email_Nature::all();
        return $email_natures;
//        return response()->json(array('success'=>true,'email_natures'=>$email_natures));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Email_Nature::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $email_nature=Email_Nature::find($id);
        return response()->json(array('success'=>true, 'email_nature'=>$email_nature));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $email_nature=Email_Nature::find($id);
        $email_nature->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Email_Nature::destroy($id);
        
        //this will permanently delete the item for db
        //$email_natures->forcedelete();
        return response()->json(array('success'=>true));
    } 
}