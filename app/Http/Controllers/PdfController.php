<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Receipt;

class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.receipt');
        return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $contact_type=Type::where('type','Contact')->first()->id;
        $org_type=Type::where('type','Organization')->first()->id;
        $input=$request->all();
        if($input['type']=='receipt'){
            $received=Receipt::where('invoice_id',Receipt::find($id)->invoice_id)
                    ->get();
            $received_total=Receipt::where('invoice_id',Receipt::find($id)->invoice_id)
                    ->select(\DB::raw('sum(amount) as received_sum'))
                    ->first();
            $receipt=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('contacts','contacts.id','=','opportunities.entity_id')
                 ->leftJoin('phone_numbers','contacts.id','=','phone_numbers.entity_id')
                 ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('projects','projects.id','=','products.project_id')
                 ->leftJoin('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->leftJoin('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->select('product_inventories.property_no','contacts.firstname','contacts.lastname',
                         'products.name',\DB::raw('organizations.name as org_name'),
                         \DB::raw('projects.name as proj_name'),
                         'payment_plans.installment',
                         'payment_modes.payment_mode',\DB::raw('sum(opp_inventory.value) as total'),
                         'payment_reasons.payment_reason','receipts.receipt_date'
                         ,'receipts.amount','phone_numbers.phone_number',
                         'receipts.receipt_no','receipts.amount'
                         ,'receipts.cheque_no','receipts.next_payment_date')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$contact_type)
                 ->where('phone_numbers.type_id',$contact_type)
                 ->first();
            $address=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('contacts','contacts.id','=','opportunities.entity_id')
                 ->join('address','contacts.id','=','address.entity_id')
                 ->select('address.address','address.postal_code','address.city','address.country')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$contact_type)
                 ->where('address.type_id',$contact_type)
                 ->first();
            $receipts=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('contacts','contacts.id','=','opportunities.entity_id')
                 ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->leftJoin('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->leftJoin('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->select('product_inventories.property_no','contacts.firstname','contacts.lastname',
                         'products.name','organizations.name',
                         'payment_modes.payment_mode','opp_inventory.value',
                         'payment_reasons.payment_reason','receipts.receipt_date','receipts.amount')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$contact_type)
                 ->get();
//        return $receipts;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadView('pdf.receipt',compact('receipts','receipt','received','received_total','address'));
        }
        if($input['type']=='invoice'){
            $invoice = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->leftJoin('phone_numbers','contacts.id','=','phone_numbers.entity_id')
                ->leftJoin('organizations','organizations.id','=','contacts.organization_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('projects', 'projects.id', '=', 'products.project_id')
                ->select('invoices.*', 'contacts.firstname', 'contacts.lastname', 'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total')
                        ,\DB::raw('organizations.name as org_name'),
                        \DB::raw('projects.name as project_name'),'phone_numbers.phone_number')
                ->where('invoices.id',$id)
                ->where('phone_numbers.type_id',$contact_type)
                ->where('opportunities.type_id',$contact_type)
                ->first();
            $address= \DB::table('invoices')
                 ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                 ->join('contacts','contacts.id','=','opportunities.entity_id')
                 ->join('address','contacts.id','=','address.entity_id')
                 ->select('address.address','address.postal_code','address.city','address.country')
                 ->where('invoices.id',$id)
                 ->where('opportunities.type_id',$contact_type)
                 ->where('address.type_id',$contact_type)
                 ->first();
            $invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->select('invoices.*', 'contacts.firstname', 'contacts.lastname', 
                        'products.name','opp_inventory.value','product_inventories.property_no',
                        'payment_plans.deposit','payment_plans.installment','payment_plans.installment_no',
                        \DB::raw('payment_plans.deposit + (payment_plans.installment*payment_plans.installment_no) as price'))
                ->where('invoices.id',$id)
                ->where('opportunities.type_id',$contact_type)
                ->get();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadView('pdf.invoice',compact('invoices','invoice','address'));
        }
        if($input['type']=='orgreceipt'){
            $received=Receipt::where('invoice_id',Receipt::find($id)->invoice_id)
                    ->get();
            $received_total=Receipt::where('invoice_id',Receipt::find($id)->invoice_id)
                    ->select(\DB::raw('sum(amount) as received_sum'))
                    ->first();
            $receipt=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('projects','projects.id','=','products.project_id')
                 ->leftJoin('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->leftJoin('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->select('product_inventories.property_no',
                         'products.name',\DB::raw('organizations.name as org_name'),
                         \DB::raw('projects.name as proj_name'),
                         'payment_plans.installment',
                         'payment_modes.payment_mode',\DB::raw('sum(opp_inventory.value) as total'),
                         'payment_reasons.payment_reason','receipts.receipt_date'
                         ,'receipts.amount',
                         'receipts.receipt_no','receipts.amount'
                         ,'receipts.cheque_no','receipts.next_payment_date')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->first();
            $address=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('address','organizations.id','=','address.entity_id')
                 ->select('address.address','address.postal_code','address.city','address.country')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->where('address.type_id',$org_type)
                 ->first();
            $phone=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('phone_numbers','organizations.id','=','phone_numbers.entity_id')
                 ->select('phone_numbers.phone_number')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->where('phone_numbers.type_id',$org_type)
                 ->first();
            $receipts=\DB::table('receipts')
                 ->join('invoices','invoices.id','=','receipts.invoice_id')
                 ->join('opportunities','opportunities.id','=','invoices.opportunity_id')
                 ->join('opp_inventory','opp_inventory.opportunity_id','=','opportunities.id')
                 ->join('payment_plans','payment_plans.id','=','opp_inventory.payment_plan_id')
                 ->join('product_inventories',
                         'product_inventories.id','=','opp_inventory.inventory_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('products','products.id','=','payment_plans.product_id')
                 ->join('payment_modes','payment_modes.id','=','receipts.payment_mode_id')
                 ->join('payment_reasons','payment_reasons.id','=',
                         'receipts.payment_reason_id')
                 ->select('product_inventories.property_no',
                         'products.name','organizations.name',
                         'payment_modes.payment_mode','opp_inventory.value',
                         'payment_reasons.payment_reason','receipts.receipt_date','receipts.amount')
                 ->where('receipts.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->get();
//        return $receipts;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadView('pdf.orgreceipt',compact('receipts','receipt','received','received_total','address','phone'));
        }
        
        if($input['type']=='orginvoice'){
            $invoice = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->join('projects', 'projects.id', '=', 'products.project_id')
                ->select('invoices.*', 'products.name', 
                        \DB::raw('sum(opp_inventory.value) as total')
                        ,\DB::raw('organizations.name as org_name'),
                        \DB::raw('projects.name as project_name'))
                ->where('invoices.id',$id)
                ->where('opportunities.type_id',$org_type)
                ->first();
            $address= \DB::table('invoices')
                 ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('address','organizations.id','=','address.entity_id')
                 ->select('address.address','address.postal_code','address.city','address.country')
                 ->where('invoices.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->where('address.type_id',$org_type)
                 ->first();
            $phone=\DB::table('invoices')
                 ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                 ->join('organizations','organizations.id','=','opportunities.entity_id')
                 ->join('phone_numbers','organizations.id','=','phone_numbers.entity_id')
                 ->select('phone_numbers.phone_number')
                 ->where('invoices.id',$id)
                 ->where('opportunities.type_id',$org_type)
                 ->where('phone_numbers.type_id',$org_type)
                 ->first();
            $invoices = \DB::table('invoices')
                ->join('opportunities', 'opportunities.id', '=', 'invoices.opportunity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->select('invoices.*',  
                        'products.name','opp_inventory.value','product_inventories.property_no',
                        'payment_plans.deposit','payment_plans.installment','payment_plans.installment_no',
                        \DB::raw('payment_plans.deposit + (payment_plans.installment*payment_plans.installment_no) as price'))
                ->where('invoices.id',$id)
                ->where('opportunities.type_id',$org_type)
                ->get();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadView('pdf.orginvoice',compact('invoices','invoice','address','phone'));
        }
        return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
