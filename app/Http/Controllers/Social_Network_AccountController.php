<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialNetwork_Account;

class Social_Network_AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    
    public function index()
    {
        //return all the socialnetwork_accountes to the view success value
        $socialnetwork_accounts=SocialNetwork_Account::all();
        return response()->json(array('success'=>true,'socialnetwork_accounts'=>$socialnetwork_accounts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        SocialNetwork_Account::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $socialnetwork_account=SocialNetwork_Account::find($id);
        return response()->json(array('success'=>true, 'socialnetwork_account'=>$socialnetwork_account));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $socialnetwork_account=SocialNetwork_Account::find($id);
        $socialnetwork_account->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        SocialNetwork_Account::destroy($id);
        
        //this will permanently delete the item for db
        //$socialnetwork_account->forcedelete();
        return response()->json(array('success'=>true));
    }
}
