<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Contact_Type;
use App\Task;
use App\Dates_to_remember;
use App\User;
use Mail;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        $tasks=Task::all();
        $dates=  Dates_to_remember::all();
       
       
        return response()->json([
            'events'=>$events,
            'tasks'=>$tasks,
            'dates'=>$dates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        
        $event=Event::create($input);
        return response()->json(['event'=>$event]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orgtype=Type::where('type','Organization')->first();
        $contact_type=Type::where('type','Contact')->first();
        $optype=Type::where('type','Opportunity')->first();
        $orgevents=\DB::table('pinned_events')
                ->join('events', 'events.id', '=', 'pinned_events.event_id')
                ->join('organizations','organizations.id','=','pinned_events.entity_id')
                ->where('pinned_events.type_id', $orgtype->id)
                ->where('events.id', $id)
                ->select('organizations.name AS orgname', 'events.id AS id')
                ->distinct()
                ->get();
        $lead_type=  Contact_Type::where('contact_type','Lead')->first();
        $contact_events=\DB::table('pinned_events')
                ->join('events', 'events.id', '=', 'pinned_events.event_id')
                ->join('contacts','contacts.id','=','pinned_events.entity_id')
                ->where('pinned_events.type_id', $contact_type->id)
                ->where('contacts.contact_type_id','!=', $lead_type->id)
                ->where('events.id', $id)
                ->select('contacts.firstname','contacts.lastname', 'events.id AS id')
                ->distinct()
                ->get();
        $opevents=\DB::table('pinned_events')
                ->join('events', 'events.id', '=', 'pinned_events.event_id')
                ->join('opportunities','opportunities.id','=','pinned_events.entity_id')
                ->where('pinned_events.type_id', $optype->id)
                ->where('events.id', $id)
                ->select('opportunities.name AS orgname', 'events.id AS id')
                ->distinct()
                ->get();
        
        $lead_events=\DB::table('pinned_events')
                ->join('events', 'events.id', '=', 'pinned_events.event_id')
                ->join('contacts','contacts.id','=','pinned_events.entity_id')
                ->where('pinned_events.type_id', $contact_type->id)
                ->where('contacts.contact_type_id','=', $lead_type->id)
                ->where('events.id', $id)
                ->select('contacts.firstname','contacts.lastname', 'events.id AS id')
                ->distinct()
                ->get();
        $event=Event::find($id);
        return response()->json([
            'event'=>$event,
            'lead_events'=>$lead_events,
            'opevents'=>$opevents,
            'contact_events'=>$contact_events,
            'orgevents'=>$orgevents]);
       ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event=Event::find($id);
        $event->update($request->all());
         return response()->json(['event'=>$event]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::destroy($id);
         return response()->json(['event'=>$event]);
    }
}
