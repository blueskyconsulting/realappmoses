<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Email;
use App\Phone_Number;
use App\Contact_Type;
use App\Contact;
use App\Organization;
use App\Salutation;
use App\Dates_to_remember;
use App\Permission;
use App\Document;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\EmailMessage;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }

    public function index() {
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;

        //Get the type id that is equal to Contact
        $type_id = Type::where('type', 'Contact')->first();

        //return all the contactes to the view success value

        $lead_type_id = Contact_Type::where('contact_type', 'Lead')->first();
        $contact_type_id = Contact_Type::where('contact_type', 'Customer')->first();

        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;
        $emails = Email::where('type_id', $type_id->id)->get();

        //Get contacts that are private
        $private_contacts = \DB::table('contacts')
                ->leftJoin('organizations', 'contacts.organization_id', '=', 'organizations.id')
                ->leftJoin('private_entities', 'contacts.id', '=', 'private_entities.entity_id')
                ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id', '=', Type::where('type', 'Contact')->first()->id);
                })
                ->where('profile_pictures.type_id', $type_id->id)
                ->select('contacts.*', 'organizations.name', 'profile_pictures.path')
                ->where('contacts.contact_type_id', '!=', $lead_type_id->id)
                ->where('contacts.permission_id', $private_id)
                ->where('private_entities.type_id', $type_id->id)
                ->where('private_entities.user_id', $user_id)
                ->whereNull('contacts.deleted_at');

        $assigned_contacts = \DB::table('contacts')
                ->leftJoin('organizations', 'contacts.organization_id', '=', 'organizations.id')
                ->leftJoin('assigned_entities', 'contacts.id', '=', 'assigned_entities.entity_id')
                ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id', '=', Type::where('type', 'Contact')->first()->id);
                })
                ->select('contacts.*', 'organizations.name', 'profile_pictures.path')
                ->where('contacts.contact_type_id', '!=', $lead_type_id->id)
                ->where('contacts.permission_id', $assigned_id)
                ->where('assigned_entities.type_id', $type_id->id)
                ->where('assigned_entities.user_id', $user_id)
                ->whereNull('contacts.deleted_at');

        //Get contacts which are public and union with private and and assigned contacts
        $contacts = \DB::table('contacts')
                ->leftJoin('organizations', 'contacts.organization_id', '=', 'organizations.id')
                ->leftJoin('profile_pictures', function ($join) {
                    $join->on('contacts.id', '=', 'profile_pictures.entity_id')
                    ->where('profile_pictures.type_id', '=', Type::where('type', 'Contact')->first()->id);
                })
                ->select('contacts.*', 'organizations.name', 'profile_pictures.path')
                ->where('contacts.contact_type_id', '!=', $lead_type_id->id)
                ->where('contacts.permission_id', $public_id)
                ->whereNull('contacts.deleted_at')
                ->union($private_contacts)
                ->union($assigned_contacts)
                ->get();

//        $contacts=\DB::table('contacts')
//                      ->leftJoin('organizations','contacts.organization_id','=','organizations.id')
//                      ->select('contacts.*','organizations.name')
//                      ->where('contacts.contact_type_id','!=',$lead_type_id->id)
//                      ->whereNull('contacts.deleted_at')
//                      ->get();
        $permissions = Permission::all();
        $phone_natures = Phone_NatureController::index();
        $email_natures = Email_NatureController::index();
        $salutations = SalutationController::index();
        $organizations = Organization::all();
        $users = AuthenticateController::index();
        $usser = AuthenticateController::index();
//        return response()->json(array('success'=>true,'organizations'=>$organizations));
        return response()->json(array('contacts' => $contacts,
                    'organizations' => $organizations,
                    'users' => $users, 'type_id' => $type_id,
                    'contact_type_id' => $contact_type_id,
                    'salutations' => $salutations,
                    'phone_natures' => $phone_natures,
                    'email_natures' => $email_natures,
                    'assigned_id' => $assigned_id,
                    'permissions' => $permissions,
                    'usser' => $usser,
                    'emails' => $emails,
                    'doc_type' => Type::where('type', 'Document')->first()->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // save the contact
        $input = $request->all();
        if ($request->has('salutation')) {
            $salutation = Salutation::create($input);
            $input['salutation_id'] = $salutation->id;
        }
        if ($request->has('name') && sizeof($input['name']) > 0) {
            $organization = Organization::create($input);
            $input['organization_id'] = $organization->id;
        }
        $contact = Contact::create($input);
        $type_id = Type::where('type', 'Contact')->first();
        return response()->json(array('type_id' => $type_id->id, 'contact_last_id' => $contact->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;

        $type_id = Type::where('type', 'Contact')->first();
        $permission = Contact::find($id)->permission_id;

        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;

        /*
         * =====================================================================
         * Check if the contact is public then initialise the public flag to
         * true
         * =====================================================================
         */

        if ($permission == $public_id) {
            $public_flag = true;
        } else {
            $public_flag = false;
        }


        //Check if contact is private the initialise a flag to true if user logged in 
        //is the owner
        if ($permission == $private_id) {
            $private_check = \App\Private_Entity::where('user_id', $user_id)
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->exists();
            if ($private_check) {
                $private_flag = true;
            } else {
                $private_flag = false;
            }
        } else {
            $private_flag = false;
        }

        /*
         * =====================================================================
         * Check if the contact has assigned permission then initialise the  
         * a flag to check if user logged is assigned
         * =====================================================================
         */
        if ($permission == $assigned_id) {
            $assign_check = \App\Assigned_Entity::where('user_id', $user_id)
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->exists();
            if ($assign_check) {
                $assign_flag = true;
            } else {
                $assign_flag = false;
            }
        } else {
            $assign_flag = false;
        }

        if ($public_flag || $private_flag || $assign_flag) {
            $opportunities = \DB::table('opportunities')
                    ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                    ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                    ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                    ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                    ->join('pipeline_stages', 'pipeline_stages.id', '=', 'opportunities.pipeline_id')
                    ->join('products', 'products.id', '=', 'payment_plans.product_id')
                    ->select(\DB::raw('opportunities.name as opp_name'), 'contacts.firstname', 'contacts.lastname', 'products.name', \DB::raw('sum(opp_inventory.value) as total')
                            , 'opportunities.description', 'pipeline_stages.stage_name')
                    ->where('opportunities.entity_id', $id)
                    ->where('opportunities.type_id', $type_id->id)
                    ->groupBy('opportunities.entity_id')
                    ->get();
            $opp_owners = \DB::table('owners')
                    ->join('users', 'users.id', '=', 'owners.user_id')
                    ->join('opportunities', 'opportunities.id', '=', 'owners.entity_id')
                    ->join('contacts', 'contacts.id', '=', 'opportunities.entity_id')
                    ->select('users.name', 'opportunities.id')
                    ->where('opportunities.entity_id', $id)
                    ->where('opportunities.type_id', $type_id->id)
                    ->get();
            $opp_products = \DB::table('opportunities')
                    ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                    ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                    ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                    ->join('products', 'products.id', '=', 'payment_plans.product_id')
                    ->select('products.name', 'product_inventories.property_no', 'opp_inventory.value')
                    ->where('opportunities.entity_id', $id)
                    ->where('opportunities.type_id', $type_id->id)
                    ->get();
            $salutations = SalutationController::index();
            $organizations = Organization::all();
            $phone_natures = Phone_NatureController::index();
            $email_natures = Email_NatureController::index();

            $dates = Dates_to_remember::where('entity_id', $id)->where('type_id', $type_id->id)->get();
            $contact = Contact::find($id);
            $contact_organization = Organization::where('id', $contact->organization_id)->first();
            //        $contact_phones=Phone_Number::where('entity_id',$id)->where('type_id',$type_id->id)->get();
            $contact_phones = \DB::table('phone_numbers')
                    ->leftJoin('phone_natures', 'phone_numbers.phone_nature_id', '=', 'phone_natures.id')
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->select('phone_numbers.id as phone_id', 'phone_nature_id', 'phone_natures.*', 'phone_number')
                    ->get();
            //        $contact_emails=Email::where('entity_id',$id)->where('type_id',$type_id->id)->get();
            $contact_emails = \DB::table('emails')
                    ->leftJoin('email_natures', 'emails.email_nature_id', '=', 'email_natures.id')
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->select('emails.id as email_id', 'email_nature_id', 'email_natures.*', 'email')
                    ->get();
            $tasks = \DB::table('tasks')
                            ->join('pinned_tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                            ->join('priority_types', 'priority_types.id', '=', 'tasks.priority_type_id')
                            ->join('task_types', 'task_types.id', '=', 'tasks.task_type')
                            ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                            ->where('pinned_tasks.type_id', $type_id->id)
                            ->where('pinned_tasks.entity_id', $id)
                            ->select('tasks.*', 'task_types.Type', 'tasks_status.status', 'priority_types.type')
                            ->orderBy('tasks_status.id', 'ASC')
                            ->orderBy('tasks.created_at', 'DESC')
                            ->distinct()->get();
            $events = \DB::table('events')
                    ->join('pinned_events', 'events.id', '=', 'pinned_events.event_id')
                    ->where('pinned_events.type_id', $type_id->id)
                    ->where('pinned_events.entity_id', $id)
                    ->whereNull('events.deleted_at')
                    ->select('events.title', 'events.id', 'events.start', 'events.end', 'pinned_events.entity_id')
                    ->distinct()
                    ->get();
            $contact_salutation = Salutation::where('id', $contact->salutation_id)->first();

            $contact_notes = \DB::table('notes')
                    ->leftJoin('users', 'users.id', '=', 'notes.user_id')
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->whereNull('notes.deleted_at')
                    ->select('notes.*', 'users.name')
                    ->get();
            $contact_address = \DB::table('address')
                    ->where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->select('address.*')
                    ->first();
            $contact_documents = Document::where('entity_id', $id)
                    ->where('type_id', $type_id->id)
                    ->get();
            $users = AuthenticateController::index();
            $conversations = \DB::table('email_messages')
                    ->leftJoin('email_attachments', 'email_messages.id', '=', 'email_attachments.email_id')
                    ->select('email_messages.*', 'email_messages.id AS id', 'email_attachments.*', 'email_attachments.id AS attach_id')
                    ->where('email_messages.entity_id', '=', $id)
                    ->where('email_messages.type_id', '=', $type_id->id)
                    ->orderBy('email_messages.created_at', 'DESC')
                    ->get();
            return response()->json(array('success' => true, 'contact' => $contact,
                        'salutations' => $salutations,
                        'organizations' => $organizations,
                        'contact_salutation' => $contact_salutation,
                        'phone_natures' => $phone_natures,
                        'email_natures' => $email_natures,
                        'contact_organization' => $contact_organization,
                        'contact_phones' => $contact_phones,
                        'contact_emails' => $contact_emails,
                        'type_id' => $type_id,
                        'tasks' => $tasks, 'dates' => $dates,
                        'contact_notes' => $contact_notes,
                        'events' => $events,
                        'opportunities' => $opportunities,
                        'opp_products' => $opp_products,
                        'users' => $users,
                        'contact_address' => $contact_address,
                        'contact_documents' => $contact_documents,
                        'conversations' => $conversations));
        } else {
            return response()->json(array('success' => false));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        if ($request->has('salutation')) {
            $salutation = Salutation::create($input);
            $input['salutation_id'] = $salutation->id;
        }
        if ($request->has('name') && sizeof($input['name']) > 0) {
            $organization = Organization::create($input);
            $input['organization_id'] = $organization->id;
        }
        $contact = Contact::find($id);
        $contact->update($input);
        return response()->json(array('success' => true, 'contact_last_id' => $contact->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //this will soft delete ie update the deleted_at column of the DB
        Contact::destroy($id);

        //the line below will permanently delete the item for db
        //$contact->forcedelete();
        return response()->json(array('success' => true));
    }

}
