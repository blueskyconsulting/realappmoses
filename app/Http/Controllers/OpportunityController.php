<?php

/* =============================================================================
 * BlueSky Consulting (c)
 * Opportunity Controller Class
 * Manages Opportunities and custom relations
 * @authors <james>, <simon>, <moses>
 * BlueSky CRM
 * =============================================================================
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opportunities;
use App\Type;
use App\Organization;
use App\Contact;
use App\Owner;
use JWTAuth;
use App\Permission;
use App\Document;
use Tymon\JWTAuth\Exceptions\JWTException;

class OpportunityController extends Controller {

    public function __construct() {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $type=  Type::where('type','Opportunity')->first();
        //get the current stages for each opportunities
        $org_type=Type::where('type','Organization')->first();
        $type_id=Type::where('type','Contact')->first();
        $user_id = JWTAuth::parseToken()->authenticate()->id; 
        //Get the private permission id
        $private_id=Permission::where('permission','Private')->first()->id;
        //Get the public permission id
        $public_id=Permission::where('permission','Public')->first()->id;
        //Get the assigned id
        $assigned_id=Permission::where('permission','Assigned')->first()->id;
        
       
         $private_opportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.firstname,
                            cont.lastname, cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN contacts as cont ON
                            cont.id=opp.entity_id
                            LEFT JOIN private_entities ON
                            private_entities.entity_id=opp.id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                            AND opp.type_id='.$type_id->id.'
                            AND  opp.permission_id='.$private_id.'
                            AND private_entities.type_id='.$type->id.'
                            AND  private_entities.user_id='.$user_id.'
                            AND opp.deleted_at IS NULL   
                            
                            '));
        $private_orgopportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.name as firstname,
                             cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN organizations as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                            LEFT JOIN private_entities ON
                            private_entities.entity_id=opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$org_type->id.'
                            AND  opp.permission_id='.$private_id.'
                            AND  private_entities.type_id='.$type->id.'
                            AND  private_entities.user_id='.$user_id.'
                               AND opp.deleted_at IS NULL 
                            
                            '));
        
        $assigned_opportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.firstname,
                            cont.lastname, cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN contacts as cont ON
                            cont.id=opp.entity_id
                            LEFT JOIN assigned_entities ON
                            assigned_entities.entity_id=opp.id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$type_id->id.'
                            AND opp.permission_id='.$assigned_id.'
                            AND assigned_entities.type_id='.$type->id.'
                            AND assigned_entities.user_id='.$user_id.'
                            AND opp.deleted_at IS NULL   
                            
                            '));
        $assigned_orgopportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.name as firstname,
                             cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN organizations as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                            LEFT JOIN assigned_entities ON
                            assigned_entities.entity_id=opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$org_type->id.'
                            AND  opp.permission_id='.$assigned_id.'
                            AND  assigned_entities.type_id='.$type->id.'
                            AND  assigned_entities.user_id='.$user_id.'
                               AND opp.deleted_at IS NULL 
                            
                            '));
        $opportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.firstname,
                            cont.lastname, cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN contacts as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$type_id->id.'
                               AND  opp.permission_id='.$public_id.'
                            AND opp.deleted_at IS NULL   
                            
                            UNION    
                            SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.firstname,
                            cont.lastname, cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN contacts as cont ON
                            cont.id=opp.entity_id
                            LEFT JOIN private_entities ON
                            private_entities.entity_id=opp.id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                            AND opp.type_id='.$type_id->id.'
                            AND  opp.permission_id='.$private_id.'
                            AND private_entities.type_id='.$type->id.'
                            AND  private_entities.user_id='.$user_id.'
                            AND opp.deleted_at IS NULL
                            
                            UNION
                            
                            SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.firstname,
                            cont.lastname, cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN contacts as cont ON
                            cont.id=opp.entity_id
                            LEFT JOIN assigned_entities ON
                            assigned_entities.entity_id=opp.id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$type_id->id.'
                            AND opp.permission_id='.$assigned_id.'
                            AND assigned_entities.type_id='.$type->id.'
                            AND assigned_entities.user_id='.$user_id.'
                            AND opp.deleted_at IS NULL   
                            
                            '));
        $orgopportunities = \DB::select
                        (\DB::raw('SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.name as firstname,
                             cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN organizations as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$org_type->id.'
                               AND opp.permission_id='.$public_id.'
                               AND opp.deleted_at IS NULL 
                            
                               UNION
                               
                               SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.name as firstname,
                             cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN organizations as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                            LEFT JOIN private_entities ON
                            private_entities.entity_id=opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$org_type->id.'
                            AND  opp.permission_id='.$private_id.'
                            AND  private_entities.type_id='.$type->id.'
                            AND  private_entities.user_id='.$user_id.'
                               AND opp.deleted_at IS NULL
                               
                               UNION
                               
                               SELECT  opp.id as opp_id, opp.name, opp.description, 
                            opp.entity_id,opp.type_id, opp.close_date, opp.deleted_at, cont.name as firstname,
                             cont.id, changes.current_stage_id, stages.stage_name FROM opportunities as opp                            
                            JOIN organizations as cont ON
                            cont.id=opp.entity_id
                            JOIN pipeline_state_changes as changes ON
                            changes.opportunity_id = opp.id
                            LEFT JOIN assigned_entities ON
                            assigned_entities.entity_id=opp.id
                             JOIN pipeline_stages as stages ON
                            changes.current_stage_id=stages.id
                            
                            WHERE changes.id = (SELECT max(id) from pipeline_state_changes  
                            
                            WHERE pipeline_state_changes.opportunity_id=opp.id)
                           AND opp.type_id='.$org_type->id.'
                            AND  opp.permission_id='.$assigned_id.'
                            AND  assigned_entities.type_id='.$type->id.'
                            AND  assigned_entities.user_id='.$user_id.'
                            AND opp.deleted_at IS NULL 
                            '));
        // users
        $users = AuthenticateController::index();
        $usser=AuthenticateController::index();
        // type
        
        //get the stages from the db
        $pipeline_stages = Pipeline_StagesController::index();
        $pipelines = Sales_PipelineController::index();
        $bid_type = BidTypeController::index();
        //$type=Type::where('type','Opportunity')->first();
        //get the sums of the stages   
        $organizations=  \App\Organization::all();
       $owners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $type->id)
                ->get();
       $tasks= \DB::table('tasks')
                ->join('pinned_tasks','tasks.id','=','pinned_tasks.task_id')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->where('pinned_tasks.type_id',$type->id)
                ->whereNull('tasks.deleted_at')
                ->select('tasks.name','tasks.id','tasks.start_date','pinned_tasks.entity_id','tasks_status.status')
                ->distinct()
                ->orderBy('tasks_status.id','ASC')
                ->orderBy('tasks.created_at','DESC')
                ->get();
       $events=\DB::table('events')
                ->join('pinned_events','events.id','=','pinned_events.event_id')
                ->where('pinned_events.type_id',$type->id)
                ->whereNull('events.deleted_at')
                ->select('events.title','events.id','events.start','events.end','pinned_events.entity_id')
                ->distinct()
                ->get();
       $documents=Document::where('type_id',$type->id)            
               ->get();
        return response()->json(['opportunities' => $opportunities,

                    'stages' => $pipeline_stages, 'pipelines' => $pipelines, 'bid_type' => $bid_type
                    ,'users' => $users,
                     'type'=>$type,
                     'contact_type'=>$type_id,
                'orgtype'=>$org_type,
            'organizations'=>$organizations,
            'orgop'=>$orgopportunities,
            'owners'=>$owners,
            'tasks'=>$tasks,
            'events'=>$events,
            'usser'=>$usser,
            'assigned_id'=>$assigned_id,
            'documents'=>$documents,
            'doc_type'=>Type::where('type','Document')->first()->id]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //create a new opportunity
        $input = $request->all();
        $opportunity = Opportunities::create($input);
        $type=  Type::where('type','Opportunity')->first();
        return response()->json(['success' => TRUE, 
            'opportunity_id' => $opportunity->id,
            'op_type'=>$type->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $org_type=Type::where('type','Organization')->first();
        $type_id=Type::where('type','Contact')->first();
        $customer='';
        $organization='';
        $opp = Opportunities::find($id);
        if($opp->type_id==$org_type->id){
            $organization=  Organization::find($opp->entity_id);
            $org=true;
            $cust=false;
        }
        else if($opp->type_id==$type_id->id){
            $customer=Contact::find($opp->entity_id);
            $cust=true;
            $org=false;
        }
        // we also need related data like pipeline_stages, pipelines and 
        // bid types
        $pipeline_stages = Pipeline_StagesController::index();
        $pipelines = Sales_PipelineController::index();
        $bid_type = BidTypeController::index();
        /* $current_stage = \DB::table('pipeline_state_changes')
          ->select('pipeline_state_changes.*')
          ->where('pipeline_state_changes.opportunity_id', $id)
          ->where('Id',\DB::table(pipeline_state_changes)->raw('SELECT MAX(')
          ->get(); */
        $current_stage = \DB::select(\DB::raw('SELECT * FROM pipeline_state_changes
            WHERE ( opportunity_id =' . $id . ' AND
            `Id` = (SELECT MAX( id ) FROM pipeline_state_changes WHERE 
            opportunity_id =' . $id . '))'));
        return response()->json(['opportunity' => $opp, 'pipeline_stages' =>
                    $pipeline_stages, 'pipelines' => $pipelines, 'bid_types' =>
                    $bid_type, 'current_stage' => $current_stage,
            'customer'=>$customer,'cust'=>$cust,'org'=>$org,'organization'=>$organization]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $opportunity = Opportunities::find($id);
        $opportunity->update($input);
        return response()->json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $opportunities = Opportunities::destroy($id);
        return redirect('/api/opportunities');
//response()->json(array('success' => true));
    }

    /*
     *  Get The Current Stage Value/Sum
     */

    public static function stageValue($id) {
        $sum = 0;
        $opportunities = \DB::select
                        (\DB::raw('SELECT * 
                                FROM opportunities AS opp
                                JOIN pipeline_state_changes AS changes ON 
                                changes.opportunity_id = opp.id
                                WHERE (changes.id = ( SELECT MAX( id ) 
                                FROM pipeline_state_changes
                                WHERE pipeline_state_changes.opportunity_id =
                                opp.id ) AND current_stage_id =' . $id . '
                                )'));
        foreach ($opportunities as $opp) {
            $sum = $sum + $opp->amount;
        }
        return response()->json(['sum' => $sum]);
    }

}
