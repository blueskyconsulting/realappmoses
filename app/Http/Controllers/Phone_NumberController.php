<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone_Number;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Phone_NumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
//    public function __construct()
//    {
//        $this->middleware('jwt.auth');
//    }
    
    public function index()
    {
        //return all the phones to the view with success value
        $phone_numbers=Phone_Number::all();
        return response()->json(array('success'=>true,'phone_numbers'=>$phone_numbers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Phone_Number::create($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phone_number=Phone_Number::find($id);
        return response()->json(array('success'=>true, 'phone_number'=>$phone_number));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->all();
        $phone_number=Phone_Number::find($id);
        $phone_number->update($input);
        return response()->json(array('success'=>true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //this will soft delete ie update the deleted_at column of the DB
        Phone_Number::destroy($id);
        
        //this will permanently delete the item for db
        //$phone_numbers->forcedelete();
        return response()->json(array('success'=>true));
    }
}
