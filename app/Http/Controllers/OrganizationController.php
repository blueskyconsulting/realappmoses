<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Phone_Number;
use App\Organization;
use App\Type;
use App\Dates_to_remember;
use App\Email;
use App\Industry;
use App\Address;
use App\Note;
use App\Permission;
use App\Document;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\EmailMessage;

class OrganizationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }

    public static function index() {
        $type = Type::where('type', 'Organization')->first();
        //Get the id of the authenticated user
        $user_id = JWTAuth::parseToken()->authenticate()->id;
        //Get the private permission id
        $private_id = Permission::where('permission', 'Private')->first()->id;
        //Get the public permission id
        $public_id = Permission::where('permission', 'Public')->first()->id;
        //Get the assigned id
        $assigned_id = Permission::where('permission', 'Assigned')->first()->id;
        //return all the organizationes to the view success value
        $industries = IndustryController::index();
        //$organizations=Organization::all();

        $users = AuthenticateController::index();

        $usser = AuthenticateController::index();

        $private_organizations = \DB::table('organizations')
                ->leftJoin('industries', 'industries.id', '=', 'organizations.industry_id')
                ->leftJoin('private_entities', 'organizations.id', '=', 'private_entities.entity_id')
                ->whereNull('organizations.deleted_at')
                ->select('organizations.*', 'industries.id AS undust_id', 'industries.industry')
                ->where('organizations.permission_id', $private_id)
                ->where('private_entities.type_id', $type->id)
                ->where('private_entities.user_id', $user_id);

        $assigned_organizations = \DB::table('organizations')
                ->leftJoin('industries', 'industries.id', '=', 'organizations.industry_id')
                ->leftJoin('assigned_entities', 'organizations.id', '=', 'assigned_entities.entity_id')
                ->whereNull('organizations.deleted_at')
                ->select('organizations.*', 'industries.id AS undust_id', 'industries.industry')
                ->where('organizations.permission_id', $assigned_id)
                ->where('assigned_entities.type_id', $type->id)
                ->where('assigned_entities.user_id', $user_id);

        $organizations = \DB::table('organizations')
                ->leftJoin('industries', 'industries.id', '=', 'organizations.industry_id')
                ->whereNull('organizations.deleted_at')
                ->select('organizations.*', 'industries.id AS undust_id', 'industries.industry')
                ->where('organizations.permission_id', $public_id)
                ->union($private_organizations)
                ->union($assigned_organizations)
                ->get();

        $owners = \DB::table('users')
                ->join('owners', 'owners.user_id', '=', 'users.id')
                ->select('users.*', 'owners.id AS owner_id', 'owners.entity_id')
                ->where('owners.type_id', $type->id)
                ->get();
        $phone_numbers = Phone_number::where('type_id', $type->id)->get();
        $dates = Dates_to_remember::where('type_id', $type->id)->get();
        $emails = Email::where('type_id', $type->id)->get();
        $tasks = \DB::table('tasks')
                ->join('pinned_tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->join('tasks_status', 'tasks_status.id', '=', 'tasks.status')
                ->where('pinned_tasks.type_id', $type->id)
                ->whereNull('tasks.deleted_at')
                ->select('tasks.name', 'tasks.id', 'tasks.start_date', 'pinned_tasks.entity_id', 'tasks_status.status')
                ->distinct()
                ->orderBy('tasks_status.id', 'ASC')
                ->orderBy('tasks.created_at', 'DESC')
                ->get();
        $notes = \DB::table('notes')
                ->leftJoin('users', 'users.id', '=', 'notes.user_id')
                ->where('type_id', $type->id)
                ->whereNull('notes.deleted_at')
                ->select('notes.note', 'notes.id as note_id', 'notes.entity_id', 'users.name')
                ->get();
        $events = \DB::table('events')
                ->join('pinned_events', 'events.id', '=', 'pinned_events.event_id')
                ->where('pinned_events.type_id', $type->id)
                ->whereNull('events.deleted_at')
                ->select('events.title', 'events.id', 'events.start', 'events.end', 'pinned_events.entity_id')
                ->distinct()
                ->get();
        $address = Address::where('type_id', $type->id)->get();
        $opportunities = \DB::table('opportunities')
                ->join('organizations', 'organizations.id', '=', 'opportunities.entity_id')
                ->join('opp_inventory', 'opp_inventory.opportunity_id', '=', 'opportunities.id')
                ->join('payment_plans', 'payment_plans.id', '=', 'opp_inventory.payment_plan_id')
                ->join('product_inventories', 'product_inventories.id', '=', 'opp_inventory.inventory_id')
                ->join('pipeline_stages', 'pipeline_stages.id', '=', 'opportunities.pipeline_id')
                ->join('products', 'products.id', '=', 'payment_plans.product_id')
                ->select(\DB::raw('opportunities.name as opp_name,opportunities.entity_id,opportunities.id AS id'), 'organizations.name AS org_name', 'products.name', \DB::raw('sum(opp_inventory.value) as total')
                        , 'opportunities.description', 'pipeline_stages.stage_name')
                ->where('opportunities.type_id', $type->id)
                ->groupBy('opportunities.entity_id')
                ->get();

        $conversations = \DB::table('email_messages')
                ->leftJoin('email_attachments', 'email_messages.id', '=', 'email_attachments.email_id')
                ->select('email_messages.*', 'email_messages.id AS id', 'email_attachments.*', 'email_attachments.id AS attach_id')
                ->where('email_messages.type_id', '=', $type->id)
                ->orderBy('email_messages.created_at', 'DESC')
                ->get();

        $documents = Document::where('type_id', $type->id)
                ->get();

        return response()->json(array(
                    'organizations' => $organizations,
                    'industries' => $industries,
                    'users' => $users,
                    'type' => $type,
                    'owners' => $owners,
                    'phone_numbers' => $phone_numbers,
                    'dates' => $dates,
                    'emails' => $emails,
                    'tasks' => $tasks,
                    'address' => $address,
                    'notes' => $notes,
                    'events' => $events,
                    'opportunities' => $opportunities,
                    'conversations' => $conversations,
                    'assigned_id' => $assigned_id,
                    'usser' => $usser,
                    'documents' => $documents,
                    'doc_type' => Type::where('type', 'Document')->first()->id
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();
        if ($request->has('industry') && sizeof($input['industry']) > 0) {
            $industry = Industry::create($input);
            $input['industry_id'] = $industry->id;
        }


        $organization = Organization::create($input);
        return response()->json(array('success' => true, 'organization_last_id' => $organization->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $organization = Organization::find($id);
        $industry = IndustryController::show($organization->industry_id);
        $type = Type::where('type', 'Organization')->first();
        $address = Address::where('entity_id', $id)->where('type_id', $type->id)->first();

        $phone_numbers = Phone_number::where('entity_id', $id)->where('type_id', $type->id)->get();
        $dates = Dates_to_remember::where('entity_id', $id)->where('type_id', $type->id)->get();
        $emails = Email::where('entity_id', $id)->where('type_id', $type->id)->get();
        $tasks = \DB::table('tasks')
                ->join('pinned_tasks', 'tasks.id', '=', 'pinned_tasks.task_id')
                ->where('pinned_tasks.type_id', $type->id)
                ->where('pinned_tasks.entity_id', $id)
                ->select('tasks.name', 'tasks.id', 'tasks.start_date')
                ->distinct()
                ->get();
        return response()->json(array
                    ('organization' => $organization,
                    'industry' => $industry,
                    'type' => $type,
                    'phone_numbers' => $phone_numbers,
                    'emails' => $emails,
                    'tasks' => $tasks,
                    'dates' => $dates,
                    'address' => $address
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $organization = Organization::find($id);



        if ($request->has('industry') && sizeof($input['industry']) > 0) {
            $industry = Industry::create($input);
            $input['industry_id'] = $industry->id;
        }


        $organization->update($input);
        return response()->json(array('success' => true, 'organization_last_id' => $organization->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //this will soft delete ie update the deleted_at column of the DB
        Organization::destroy($id);

        //this will permanently delete the item for db
        //$organization->forcedelete();
        return response()->json(array('success' => true));
    }

}
