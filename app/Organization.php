<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['name','background','industry_id','number_of_employees',
        'user_responsible','permission_id'];
    protected $dates = ['deleted_at'];
}
