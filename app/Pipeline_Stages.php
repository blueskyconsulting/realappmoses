<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pipeline_Stages extends Model
{
   // alllow for softdeleted
    use SoftDeletes;
    
    // table to use
    protected $table = 'pipeline_stages';
    // mass assigned
    protected $fillable = ['name','pipeline_id'];
    // for soft deletes
    protected $dates = ['deleted_at'];
}
