<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;
    protected $table='notes';
    protected $fillable=['note','entity_id','type_id','user_id'];
    protected $dates=['deleted_at'];
}
