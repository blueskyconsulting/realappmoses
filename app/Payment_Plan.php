<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_Plan extends Model
{
    use SoftDeletes;
    protected $table='payment_plans';
    protected $fillable=['deposit','installment','installment_no','product_id','plan_status_id'];
    protected $dates = ['deleted_at'];
}
