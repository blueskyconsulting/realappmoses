<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialNetwork extends Model
{
    use SoftDeletes;
    protected $table='social_networks';
    protected $fillable=['social_network'];

    protected $dates = ['deleted_at'];
}
