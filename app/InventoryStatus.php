<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class InventoryStatus extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['status'];
    protected $table='inventory_status';
    protected $dates = ['deleted_at'];
}
