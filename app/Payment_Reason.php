<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_Reason extends Model
{
    use SoftDeletes;
    protected $table='payment_reasons';
    protected $fillable=['payment_reason'];
    protected $dates = ['deleted_at'];
}
