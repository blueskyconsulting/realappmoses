<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailMessage extends Model
{
   
    //
    protected $table="email_messages";
    protected $fillable=['from','to','subject','body_text','body_html','headers','status','entity_id','type_id'];

    protected $dates = ['deleted_at'];

}
