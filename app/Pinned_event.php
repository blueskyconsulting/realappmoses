<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pinned_event extends Model
{
    use SoftDeletes;
    protected $fillable=['event_id','entity_id','type_id'];
    protected $dates=['deleted_at'];
}
