<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_Mode extends Model
{
    use SoftDeletes;
    protected $table='payment_modes';
    protected $fillable=['payment_mode'];
    protected $dates=['deleted_at'];
}
