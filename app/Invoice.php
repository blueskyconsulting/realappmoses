<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['opportunity_id','due_date','invoice_no','permission_id'];
    protected $table='invoices';
    protected $dates = ['deleted_at'];
}
