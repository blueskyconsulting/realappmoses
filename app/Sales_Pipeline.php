<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales_Pipeline extends Model
{
    // alllow for softdeleted
    use SoftDeletes;
    
    // table to use
    protected $table = 'sales_pipeline';
    // mass assigned
    protected $fillable = ['name'];
    // for soft deletes
    protected $dates = ['deleted_at'];
}
