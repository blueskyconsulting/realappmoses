<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    protected $fillable=['type_id','entity_id','path','document_name','permission_id'];
    protected $dates = ['deleted_at'];
}
