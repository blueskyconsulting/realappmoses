<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan_Status extends Model
{
    use SoftDeletes;
    protected $table = 'plan_status';
    protected $fillable=['status'];
    protected $dates = ['deleted_at'];
}
