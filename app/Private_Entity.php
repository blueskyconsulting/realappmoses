<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Private_Entity extends Model
{
    use SoftDeletes;
    protected $table = 'private_entities';
    protected $fillable=['type_id','entity_id','user_id'];
    protected $dates = ['deleted_at'];
}
