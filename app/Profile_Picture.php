<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile_Picture extends Model
{
    use SoftDeletes;
    protected $table='profile_pictures';
    protected $fillable=['type_id','entity_id','path'];
    protected $dates = ['deleted_at'];
}
