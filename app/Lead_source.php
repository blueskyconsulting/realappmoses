<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead_source extends Model
{
    use SoftDeletes;
       /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leads_sources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['source'];
    protected $dates = ['deleted_at'];
}
