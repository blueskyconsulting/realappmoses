<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialNetwork_Account extends Model
{
    use SoftDeletes;
    //
    protected $table='social_network_accounts';
    protected $fillable=['id','handle','type_id','entity_id','social_network_id'];

    protected $dates = ['deleted_at'];
}
