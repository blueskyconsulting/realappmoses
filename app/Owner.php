<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['user_id','type_id','entity_id'];
    protected $dates = ['deleted_at'];
}
