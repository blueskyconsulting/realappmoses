<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address_Nature extends Model
{
    use SoftDeletes;
    protected $table = 'address_natures';
    protected $fillable=['address_nature'];
    protected $dates = ['deleted_at'];
}
