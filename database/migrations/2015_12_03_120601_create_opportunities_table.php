<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('opportunities',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('entity_id');
            $table->integer('type_id');
            $table->string('description',400);
            $table->integer('winning_probability');
            $table->integer('user_responsible_id');            
            $table->integer('bid_type_id');
            $table->integer('pipeline_id');
            $table->integer('permission_id');
            $table->date('close_date');
            $table->timestamps();
            $table->softDeletes();
        });
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunities');
    }
}