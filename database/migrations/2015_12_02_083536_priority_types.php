<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PriorityTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
         public function up()
    {
         //migration to create a contacts type table
        Schema::create('priority_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the contacts_type table
        Schema::drop('priority_types');
    }
}
