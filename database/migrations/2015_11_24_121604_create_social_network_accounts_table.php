<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialNetworkAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //migration to create a socila network accounts table
        Schema::create('social_network_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_network_account');
            $table->integer('type_id');
            $table->integer('entity_id');
            $table->integer('social_network_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the social network accounts table
        Schema::drop('social_network_accounts');
    }
}
