<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id');
            $table->string('receipt_no');
            $table->integer('payment_mode_id');
            $table->integer('payment_reason_id');
            $table->integer('cheque_no');
            $table->date('receipt_date');
            $table->date('next_payment_date');
            $table->integer('amount');
            $table->integer('permission_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('receipts');
    }
}
