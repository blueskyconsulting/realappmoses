<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //migration to create address table
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->string('address');
            $table->string('postal_code');
            $table->string('country');
            $table->integer('type_id');
            $table->integer('entity_id');
            $table->integer('address_nature_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the address table
        Schema::drop('address');
    }
}
