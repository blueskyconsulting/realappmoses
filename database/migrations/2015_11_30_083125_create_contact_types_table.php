<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //migration to create a contacts type table
        Schema::create('contact_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contact_type')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the contacts_type table
        Schema::drop('contact_types');
    }
}
