<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesToRemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates_to_remember', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('description');
            $table->integer('type_id');
            $table->integer('entity_id');
            $table->integer('today');
            $table->integer('mail');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dates_to_remember');
    }
}
