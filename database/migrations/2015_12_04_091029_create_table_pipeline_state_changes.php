<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePipelineStateChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pipeline_state_changes',function(Blueprint $table){
            $table->increments('id');
            $table->integer('opportunity_id');
            $table->integer('current_stage_id');
            $table->integer('previous_stage_id');
            $table->string('notes',1500);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pipeline_state_changes');
    }
}
