<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndusriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //migration to create an industries table
        Schema::create('industries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('industry')->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //run migration rollback to delete this table
        Schema::drop('industries');
    }
}
