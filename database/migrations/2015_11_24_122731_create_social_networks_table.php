<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //migration to create a socila network natures table
        Schema::create('social_networks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_network')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete social networks table
        Schema::drop('social_networks');
    }
}
