<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //migration to create contacts atble
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salutation_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('title');
            $table->string('background');
            $table->string('contact_type_id');
            $table->integer('organization_id');
            $table->integer('permission_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to drop table
        Schema::drop('contacts');
    }
}
