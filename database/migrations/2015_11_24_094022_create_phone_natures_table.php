<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //migration to create a phone natures table
        Schema::create('phone_natures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_nature')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the phone natures table
        Schema::drop('phone_natures');
    }
}
