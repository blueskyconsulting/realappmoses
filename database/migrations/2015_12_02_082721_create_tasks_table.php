<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
       public function up()
    {
         //migration to create a contacts type table
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('start_date');
            $table->dateTime('reminder')->nullable();
            $table->integer('task_type');
            $table->integer('status');
            $table->integer('priority_type_id');
            $table->integer('today');
            $table->integer('mail');
            $table->integer('permission_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete the contacts_type table
        Schema::drop('tasks');
    }
}
