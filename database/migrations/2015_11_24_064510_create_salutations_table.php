<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalutationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        //migration to create an salutations table
        Schema::create('salutations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salutation')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //rollback to delete the salutations table
        Schema::drop('salutations');
    }
}
