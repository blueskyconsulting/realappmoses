<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpportunitiesStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('opportunities_status',function(Blueprint $table){
            $table->increments('id');
            $table->integer('opportunity_id');
            $table->integer('status_stage_id');
            $table->integer('status_name_id');
            $table->string('notes',400);
            $table->string('reasons');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opportunities_status');
    }
}
