<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //migration to create a phone numbers table
        Schema::create('phone_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number')->unique();
            $table->integer('type_id');
            $table->integer('entity_id');
            $table->integer('phone_nature_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback to delete phone number table
        Schema::drop('phone_numbers');
    }
}
