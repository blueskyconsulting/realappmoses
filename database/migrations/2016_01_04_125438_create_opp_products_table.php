<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOppProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opp_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('opportunity_id');
            $table->integer('inventory_id');
            $table->integer('payment_plan_id');
           // $table->integer('quantity');
            $table->integer('value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('opp_inventory');
    }
}
