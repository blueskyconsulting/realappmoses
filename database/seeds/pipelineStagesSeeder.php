<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Pipeline_Stages;

class pipelineStagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        Model::unguard();
        
        DB::table('pipeline_stages')->truncate();

        $stages = array(
            [ 'stage_name' => 'Initial Contact', 'pipeline_id' => 9],
            ['stage_name' => 'Qualification', 'pipeline_id' => 9],
            ['stage_name' => 'Meeting', 'pipeline_id' => 9],
            ['stage_name' => 'Proposal', 'pipeline_id' => 9],
            ['stage_name' => 'Negotiation', 'pipeline_id' => 9],
            ['stage_name' => 'Close', 'pipeline_id' => 9]
        );

        // Loop through each user above and create the record for them in the database
        foreach ($stages as $stage) {
            Pipeline_Stages::create($stage);
        }
        Model::reguard();
    }

}
