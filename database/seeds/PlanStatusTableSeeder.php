<?php

use Illuminate\Database\Seeder;
use App\Plan_Status;
use Illuminate\Database\Eloquent\Model;

class PlanStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the plan status table with this data
        Model::unguard();
        DB::table('plan_status')->truncate();
        $plan_status = array(
            ['status' => 'Active'],
            ['status' => 'Not Active']
        );
            
        // Loop through each salutation above and create the record for them in the database
        foreach ($plan_status as $key)
        {
            Plan_Status::create($key);
        }

        Model::reguard();
    }
}
