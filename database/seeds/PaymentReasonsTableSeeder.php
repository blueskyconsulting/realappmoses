<?php

use Illuminate\Database\Seeder;
use App\Payment_Reason;
use Illuminate\Database\Eloquent\Model;

class PaymentReasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Model::unguard();
        DB::table('payment_reasons')->truncate();
        $payment_reasons = array(
            ['payment_reason' => 'Deposit payment'],
            ['payment_reason' => 'Cash price payment'],
            ['payment_reason' => 'Installment payment']
        );
        
        foreach ($payment_reasons as $key) {
            Payment_Reason::create($key);
        }

        Model::reguard();
    }
}
