<?php

use Illuminate\Database\Seeder;
use App\Lead_source;
use Illuminate\Database\Eloquent\Model;
class Lead_sources_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('leads_sources')->truncate();
         $sources = array(
                ['source' => 'Phone Enquiry'],
                ['source' => 'Patner referral'],
                ['source' => 'Purchased list'],
                ['source' => 'Other'],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($sources as $source)
        {
            Lead_source::create($source);
        }
        Model::reguard();
    }
}
