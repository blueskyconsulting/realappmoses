<?php

use Illuminate\Database\Seeder;
use App\Industry;
use Illuminate\Database\Eloquent\Model;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the industries table with this data
        Model::unguard();
        DB::table('industries')->truncate();
        $industries = array(
            ['industry' => 'Telecommunication'],
            ['industry' => 'Information Technology'],
            ['industry' => 'Insurance'],
            ['industry' => 'Banking'],
        );
        
        // Loop through each Industry above and create the record for them in the database
        foreach ($industries as $key) {
            Industry::create($key);
        }

        Model::reguard();
    }
}
