<?php

use Illuminate\Database\Seeder;
use App\Sales_Pipeline;
use Illuminate\Database\Eloquent\Model;

class PipelineSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        DB::table('sales_pipeline')->truncate();

        $pipelines = array(
            [ 'name' => 'Sales Pipeline'],
            ['name' => 'Custom']
        );

        // Loop through each user above and create the record for them in the database
        foreach ($pipelines as $pipeline) {
            Sales_Pipeline::create($pipeline);
        }
        Model::reguard();
    }

}
