<?php

use Illuminate\Database\Seeder;
use App\InventoryStatus;
use Illuminate\Database\Eloquent\Model;

class InventoryStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the address natures table with this data
        Model::unguard();
        DB::table('inventory_status')->truncate();
        $inventory_status = array(
            ['status' => 'Available'],
            ['status' => 'Sold'],
            ['status' => 'Booked'],
            ['status' => 'Hot']
        );

        // Loop through each address natures above and create the record for them in the database
        foreach ($inventory_status as $key) {
            InventoryStatus::create($key);
        }
        Model::reguard();
    }
}
