<?php

use Illuminate\Database\Seeder;
use App\Priority_type;
use Illuminate\Database\Eloquent\Model;
class PriorityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run() {

        //Fill the phone natures table with this data
        Model::unguard();
        DB::table('priority_types')->truncate();
        $priority_types = array(
            ['type' => 'HIGH'],
             ['type' => 'MEDIUM'],
              ['type' => 'LOW']
        );

        // Loop through each phone natures above and create the record for them in the database
        foreach ($priority_types as $key) {
            Priority_type::create($key);
        }

        Model::reguard();
    }
}
