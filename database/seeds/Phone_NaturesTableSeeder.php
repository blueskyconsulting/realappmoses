<?php

use Illuminate\Database\Seeder;
use App\Phone_Nature;
use Illuminate\Database\Eloquent\Model;

class Phone_NaturesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        //Fill the phone natures table with this data
        Model::unguard();
        DB::table('phone_natures')->truncate();
        $phone_natures = array(
            ['phone_nature' => 'Home'],
            ['phone_nature' => 'Work']
        );

        // Loop through each phone natures above and create the record for them in the database
        foreach ($phone_natures as $key) {
            Phone_Nature::create($key);
        }

        Model::reguard();
    }

}
