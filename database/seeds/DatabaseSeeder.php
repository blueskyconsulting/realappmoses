<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('InventoryStatusTableSeeder');
        $this->call('Address_NaturesTableSeeder');
        $this->call('ContactTypesTableSeeder');
        $this->call('IndustriesTableSeeder');
        $this->call('Phone_NaturesTableSeeder');
        $this->call('SalutationsTableSeeder');
        $this->call('TypeTableSeeder');
        $this->call('SocialNetworksTableSeeder');
        $this->call('Email_NaturesTableSeeder');
        //$this->call('OpportunitiesSeeder');
        $this->call('PipelineSeeder');
        $this->call('pipelineStagesSeeder');
        $this->call('Bid_TypesSeeder');
        $this->call('PriorityTypesSeeder');
        $this->call('Lead_status_seeder');
        $this->call('Lead_sources_seeder');
        $this->call('PlanStatusTableSeeder');
        $this->call('Task_status_seeder');
        $this->call('Task_types_seeder');
        $this->call('PaymentModesTableSeeder');
        $this->call('PaymentReasonsTableSeeder');
        $this->call('PermissionsTableSeeder');
        Model::unguard();

        DB::table('users')->truncate();

        $users = array(
            ['name' => 'Aluda Moses', 'email' => 'aluda@innovate.co.ke', 'password' => Hash::make('pass123')],
            ['name' => 'Martha Mwangi', 'email' => 'martha.mwangi@enkavilla.co.ke', 'password' => Hash::make('pass123')],
            ['name' => 'Adrian Akuvitsa', 'email' => 'adrian.akuvitsa@enkavilla.co.ke', 'password' => Hash::make('pass123')],
            ['name' => 'Solo Ken', 'email' => 'solo.ken@enkavilla.co.ke', 'password' => Hash::make('pass123')],
            ['name' => 'Meshack Muhoho', 'email' => 'muhoho@enkavilla.co.ke', 'password' => Hash::make('pass123')],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user) {
            User::create($user);
        }

        Model::reguard();
    }
}
