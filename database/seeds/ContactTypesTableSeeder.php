<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Contact_Type;

class ContactTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the contact type table with this data
        Model::unguard();
        DB::table('contact_types')->truncate();
        $contact_types = array(
            ['contact_type' => 'Support'],
            ['contact_type' => 'Admin'],
            ['contact_type' => 'User'],
            ['contact_type' => 'Customer'],
            ['contact_type' => 'Lead']
            
        );
        
        // Loop through each Industry above and create the record for them in the database
        foreach ($contact_types as $key) {
            Contact_Type::create($key);
        }

        Model::reguard();
    }
}
