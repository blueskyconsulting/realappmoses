<?php

use Illuminate\Database\Seeder;
use App\Task_type;
use Illuminate\Database\Eloquent\Model;
class Task_types_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Fill the social networks table with this data
        Model::unguard();
        DB::table('task_types')->delete();
        $types = array(
            ['type' => 'Meeting'],
            ['type' => 'Email'],
            ['type' => 'Call'],
            ['type' => 'to do']
        );

        // Loop through each social network above and create the record for them in the database
        foreach ($types as $key) {
            Task_type::create($key);
        }

        Model::reguard();
    }
}
