<?php

use Illuminate\Database\Seeder;
use App\Opportunities;
use Illuminate\Database\Eloquent\Model;

class OpportunitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Model::unguard();

        //DB::table('opportunities')->delete();

        $opportunities = array(
                [   'name' => 'powerNGO Management Software', 
                    'description' => 'Sell powerNGO management Software', 
                    'winning_probability' => 4, 'user_responsible_id'=>1,
                    'amount'=>400000,'bid_type_id'=>1,'pipeline_id'=>1,
                    'close_date'=>"2015-12-11"], 
            );
            
        // Loop through each user above and create the record for them in the database
        foreach ($opportunities as $opportunity)
        {
            Opportunities::create($opportunity);
        }

        Model::reguard();
    }
}
