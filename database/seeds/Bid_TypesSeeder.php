<?php

use Illuminate\Database\Seeder;
use App\BidTypes;
use Illuminate\Database\Eloquent\Model;

class Bid_TypesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();
        DB::table('bid_types')->truncate();

        $bid_types = array(
            ['name' => 'Fixed Bid'],
            ['name' => 'Per Hour'],
            ['name' => 'Per Day'],
            ['name' => 'Per Week'],
            ['name' => 'Per Month'],
            ['name' => 'Per Year']
        );

        // Loop through each user above and create the record for them in the database
        foreach ($bid_types as $bid_type) {
            BidTypes::create($bid_type);
        }

        Model::reguard();
    }

}
