<?php

use Illuminate\Database\Seeder;
use App\Payment_Mode;
use Illuminate\Database\Eloquent\Model;

class PaymentModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('payment_modes')->truncate();
        $payment_modes = array(
            ['payment_mode' => 'Cash'],
            ['payment_mode' => 'Cheque']
        );
        
        foreach ($payment_modes as $key) {
            Payment_Mode::create($key);
        }

        Model::reguard();
    }
}
