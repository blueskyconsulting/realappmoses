<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Task_status;
class Task_status_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Fill the social networks table with this data
        Model::unguard();
        DB::table('tasks_status')->delete();
        $types = array(
            ['status' => 'new'],
            ['status' => 'in progress'],
            ['status' => 'paused'],
            ['status' => 'awating someone'],
            
            ['status' => 'Done']
        );

        // Loop through each social network above and create the record for them in the database
        foreach ($types as $key) {
            Task_status::create($key);
        }

        Model::reguard();
    }
}
