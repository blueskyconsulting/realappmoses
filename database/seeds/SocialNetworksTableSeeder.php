<?php

use Illuminate\Database\Seeder;
use App\SocialNetwork;
use Illuminate\Database\Eloquent\Model;

class SocialNetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Fill the social networks table with this data
        Model::unguard();
        DB::table('social_networks')->truncate();
        $social_networks = array(
            ['social_network' => 'Facebbok'],
            ['social_network' => 'Twitter'],
            ['social_network' => 'LinkedIn'],
        );

        // Loop through each social network above and create the record for them in the database
        foreach ($social_networks as $key) {
            SocialNetwork::create($key);
        }

        Model::reguard();
    }
}
