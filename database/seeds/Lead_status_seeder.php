<?php

use Illuminate\Database\Seeder;
use App\Lead_status;
use Illuminate\Database\Eloquent\Model;
class Lead_status_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('leads_status')->truncate();
        $status = array(
                ['status' => 'Not contacted'],
                ['status' => 'Contacted'],
                ['status' => 'Defferred'],
                ['status' => 'Disqualified'],
             ['status' => 'Qualified']
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($status as $st)
        {
            Lead_status::create($st);
        }
        Model::reguard();
    }
}
