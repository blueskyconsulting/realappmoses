<?php

use Illuminate\Database\Seeder;
use App\Salutation;
use Illuminate\Database\Eloquent\Model;

class SalutationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the salutations table with this data
        Model::unguard();
        DB::table('salutations')->truncate();
        $salutations = array(
            ['salutation' => 'Mr'],
            ['salutation' => 'Mrs'],
            ['salutation' => 'Miss'],
            ['salutation' => 'Dr'],
            ['salutation' => 'Prof'],
            ['salutation' => 'Eng'],
            ['salutation' => 'Rev'],
        );
            
        // Loop through each salutation above and create the record for them in the database
        foreach ($salutations as $key)
        {
            Salutation::create($key);
        }

        Model::reguard();
    }
}
