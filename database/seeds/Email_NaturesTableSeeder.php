<?php

use Illuminate\Database\Seeder;
use App\Email_Nature;
use Illuminate\Database\Eloquent\Model;

class Email_NaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the phone natures table with this data
        Model::unguard();
        DB::table('email_natures')->truncate();
        $email_natures = array(
            ['email_nature' => 'Personal'],
            ['email_nature' => 'Work']
        );

        // Loop through each phone natures above and create the record for them in the database
        foreach ($email_natures as $key) {
            Email_Nature::create($key);
        }

        Model::reguard();
    }
}
