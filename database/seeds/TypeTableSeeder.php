<?php

use Illuminate\Database\Seeder;
use App\Type;
use Illuminate\Database\Eloquent\Model;

class TypeTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //Fill the types table with this data
        Model::unguard();
        DB::table('types')->truncate();
        $types = array(
            ['type' => 'Contact'],
            ['type' => 'Organization'],
            ['type' => 'Lead'],
            ['type' => 'Task'],
            ['type'=>'Product'],
            ['type'=>'Inventory'],
            ['type'=>'Opportunity'],
            ['type'=>'Project'],
            ['type'=>'Receipt'],
            ['type'=>'Invoice'],
            ['type'=>'Document']
        );

        // Loop through each types above and create the record for them in the database
        foreach ($types as $key) {
            Type::create($key);
        }

        Model::reguard();
        //
    }
}
