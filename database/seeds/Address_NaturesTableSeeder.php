<?php

use Illuminate\Database\Seeder;
use App\Address_Nature;
use Illuminate\Database\Eloquent\Model;
class Address_NaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Fill the address natures table with this data
        Model::unguard();
        DB::table('address_natures')->truncate();
        $address_natures = array(
            ['address_nature' => 'Home'],
            ['address_nature' => 'Work']
        );

        // Loop through each address natures above and create the record for them in the database
        foreach ($address_natures as $key) {
            Address_Nature::create($key);
        }
        Model::reguard();
    }
}
