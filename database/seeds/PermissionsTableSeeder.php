<?php

use Illuminate\Database\Seeder;
use App\Permission;
use Illuminate\Database\Eloquent\Model;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('permissions')->truncate();
        $permissions = array(
            ['permission' => 'Private'],
            ['permission' => 'Public'],
            ['permission' => 'Assigned']
        );
        
        foreach ($permissions as $key) {
            Permission::create($key);
        }

        Model::reguard();
    }
}
