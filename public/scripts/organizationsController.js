/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for Organizations
 * @author James, Simon, Moses
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('organizationsController', organizationsController);
    function organizationsController($scope, CRUDservice, $mdDialog, toastService,

            dialogService, $rootScope, filterFilter, pinService, moment, taskService, $stateParams, $state, $http, Upload,bulkEmailService,$timeout, permService, uploadService) {

        var vm = this;
        $scope.user_id = [];
        $scope.displaynumber = 5;
        vm.addphone = [];
        vm.addemail = [];
        vm.addDay = [];
        vm.addindustry = [];
        $scope.note = [];
        $scope.plan = [];
        $scope.search = $stateParams.search;
        if ($scope.search.length) {
            $scope.displaynumber = "";
        }
        angular.forEach($rootScope.permissions, function (value, key) {
            if (value.permission == 'Private') {
                vm.privateid = value.id;
            } else if (value.permission == 'Assigned') {
                vm.assignedid = value.id;
            }
        });
        // pick organizations from the database
        CRUDservice.query({object: 'organizations'})
                .$promise.then(function (data) {
                    vm.organizations = data.organizations;
                    $rootScope.industries = data.industries;
                    $rootScope.users = data.users;
                    vm.type = data.type;
                    vm.notes = data.notes;
                    
                    vm.phones = data.phone_numbers;
                    vm.emails = data.emails;
                    vm.tasks = data.tasks;
                    vm.dates = data.dates;
                    vm.owners = data.owners;
                    vm.address = data.address;
                    vm.events = data.events;
                    vm.conversations = data.conversations;
                    vm.opportunities = data.opportunities;
                    for (var i = 0; i < vm.organizations.length; i++) {
                        vm.addphone[i] = false;
                        vm.addemail[i] = false;
                        vm.addDay[i] = false;
                        vm.addindustry[i] = false;
                    }
                    vm.assigned_id = data.assigned_id;
                    $rootScope.users_choose = data.usser;
                    $scope.documents = data.documents;
                    vm.doc_type=data.doc_type;
                });
        /*A simple function to update the scopes form db each time someone chages organization*/
        function updateScope() {
            vm.organizations = CRUDservice.query({object: 'organizations'})
                    .$promise.then(function (data) {
                        vm.organizations = data.organizations;
                        vm.phones = data.phone_numbers;
                        vm.emails = data.emails;
                        vm.tasks = data.tasks;
                        vm.dates = data.dates;
                        vm.owners = data.owners;
                        vm.address = data.address;
                    });
        }
        vm.getOwners = function (org_id) {
            return filterFilter(vm.owners, {entity_id: org_id});
        };
        vm.getDates = function (org_id) {
            return filterFilter(vm.dates, {entity_id: org_id});
        };
        vm.getEmails = function (org_id) {
            return filterFilter(vm.emails, {entity_id: org_id});
        };
        vm.getPhones = function (org_id) {
            return filterFilter(vm.phones, {entity_id: org_id});
        };
        vm.getTasks = function (org_id) {
            return filterFilter(vm.tasks, {entity_id: org_id});
        };
        vm.getAddress = function (org_id) {
            return filterFilter(vm.address, {entity_id: org_id});
        };
        vm.getEvents = function (org_id) {
            return filterFilter(vm.events, {entity_id: org_id});
        };
        vm.getConversations = function (org_id) {
            return filterFilter(vm.conversations, {entity_id: org_id});
        };
        /*Function to filter oppportunities based on the org id
         * */
        vm.getOpportunities = function (org_id) {
            return filterFilter(vm.opportunities, {entity_id: org_id});
        };
        $scope.getDocuments = function (lead_id) {
            return filterFilter($scope.documents, {entity_id: lead_id});
        };
        /*Function to get number of pinned objects */
        vm.getCount = function (org_id, object) {
            if (object === 'events') {
                var event = filterFilter(vm.events, {entity_id: org_id});
                return event.length;
            }
//            else if(object==='opportunities'){
//                var op=filterFilter(vm.optasks, {id: org_id});
//                return op.length;
//                
//            }
            else if (object === 'tasks') {
                var task = filterFilter(vm.tasks, {entity_id: org_id});
                return task.length;
            } else if (object === 'notes') {
                var notes = filterFilter(vm.notes, {entity_id: org_id});
                return notes.length;
            } else if (object === 'dates') {
                var notes = filterFilter(vm.dates, {entity_id: org_id});
                return notes.length;
            } else if (object === 'opportunities') {
                var opp = filterFilter(vm.opportunities, {entity_id: org_id});
                return opp.length;
            }
        }
        /*Function that fetches notes if it exist for an organization*/
        vm.getNotes = function (org_id) {
            var org_notes = filterFilter(vm.notes, {entity_id: org_id});
            var notecount = org_notes.length;
            var orgs = vm.organizations;
            $scope.editnoteflag = [];
            $scope.editnoteflag[[orgs.length][notecount]];
            for (var y = 0; y < orgs.length; y++) {
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[[y][i]] = true;
                }
            }
            ;
            angular.forEach(org_notes, function (value, key) {
                $scope.note[key] = value.note;
            });
            return filterFilter(vm.notes, {entity_id: org_id});
        };
        /*Function to reset display number for efficient searching*/
        $scope.resetItems = function () {
            $scope.displaynumber = '';
        }
        /* =====================================================================
         * Edit Function For Organizations
         * 
         * =====================================================================
         */
        vm.edit = function (organization_id, ev) {
            // get the organization from db here for seeding the edit form
            // then call the edit form with the preseeded data

            CRUDservice.get({object: 'organizations', id: organization_id}).$promise.then(function (data) {

                $rootScope.organ = data.organization;
                $rootScope.add = data.address;
                vm.addr = data.address;
                $rootScope.indust = data.industry;
                dialogService.show(ev, 'editOrganization', editOrganization, true);

            });

        };
        /* =====================================================================
         * Controller Function for Editing an Organization
         * Binds to the Dialog Show
         * =====================================================================
         */
        function editOrganization($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.editOrganization = function (org_id, $rootScope) {
                var organization = {
                    name: $scope.organ.name,
                    background: $scope.organ.background
                };
                if ($scope.selectedindustry.originalObject.id === undefined) {
                    var industryData = {
                        industry: $scope.selectedindustry.originalObject
                    };
                } else {
                    var industryData = {
                        industry_id: $scope.selectedindustry.originalObject.id
                    };
                }
                ;
                angular.extend(organization, industryData);

                CRUDservice.update({object: 'organizations', id: org_id}, organization).$promise.then(function (data) {
                    var org = data.organization_last_id;

                    if ($scope.add.address === undefined) {
                    } else {
                        var address = {
                            address: $scope.add.address,
                            city: $scope.add.city,
                            postal_code: $scope.add.postal_code,
                            country: $scope.add.country,
                        };

                        CRUDservice.update({object: 'address', id: vm.addr.id}, address).$promise.then(function (data) {

                        });
                    }
                    updateScope();
                    toastService.show('Organization Edited');
                    dialogService.close();
                });

            }

        }

        /* =====================================================================
         * Function to delete an organization
         * @returns success or failure
         * @param the contact_id we are deleting
         * =====================================================================
         */
        vm.deleteOrganization = function (org_id) {
            if (confirm('Do you want to delete this organization?')) {
                var success = CRUDservice.delete({object: 'organizations', id: org_id});
                if (success) {

                    updateScope();
                    dialogService.close();
                    toastService.show('Organization deleted');
                }

            }
        };

//
        /* =====================================================================
         * Function to handle showing the addOrganization Dialog
         * =====================================================================
         */
        vm.addOrganization = function (ev) {

            dialogService.show(ev, 'addOrganization', addOrg, true);
        };
        /* =====================================================================
         * 'Contoller' function to add an Organization
         * =====================================================================
         */
        function addOrg($scope, $rootScope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.close = function () {
                dialogService.close();
            }

            $scope.createOrganization = function () {
                var organization = {
                    name: $scope.name,
                    background: $scope.background,
                    permission_id: $scope.permission_id
                };
                if ($scope.selectedindustry.originalObject.id === undefined) {
                    var industryData = {
                        industry: $scope.selectedindustry.originalObject
                    };
                } else {
                    var industryData = {
                        industry_id: $scope.selectedindustry.originalObject.id
                    };
                }
                ;
                angular.extend(organization, industryData);
                console.log(organization);
                CRUDservice.save({object: 'organizations'}, organization).$promise.then(function (data) {
                    var org = data.organization_last_id;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: org,
                                type_id: vm.type.id
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: org,
                            type_id: vm.type.id
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);
                    }
                    ;
                    if ($scope.address !== undefined) {
                        var address = {
                            address: $scope.address,
                            city: $scope.city,
                            postal_code: $scope.postal_code,
                            country: $scope.country,
                            entity_id: org,
                            type_id: vm.type.id
                        };
                        console.log(address);
                        CRUDservice.save({object: 'address'}, address).$promise.then(function (data) {

                        });
                    }

                    /*
                     * =========================================================
                     * call the permission service to save the permission
                     * First check if the permission is private or public
                     * the call the appriate function of the service
                     * =========================================================
                     */


                    if ($scope.permission_id == vm.privateid) {
                        permService.savePrivate($rootScope.currentUser.id, org, vm.type.id);
                    } else if ($scope.permission_id == vm.assignedid) {
                        permService.saveAssigned($scope.users_chosen, org, vm.type.id);
                    }
                    updateScope();
                    toastService.show('Organization Added');

                    dialogService.close();

                });
            }




        }
        /*Function that makes form for adding an object visible
         * @params type and angular index
         * @returns true for object to be added*/
        vm.add = function (type, index) {

            if (type === 'phone') {
                vm.addphone[index] = true;
            } else if (type === 'email') {
                vm.addemail[index] = true;
            } else if (type === 'date') {
                vm.addDay[index] = true;
            } else if (type === 'industry') {
                vm.addindustry[index] = true;
            }
        }
        /*Function to save object for organization added
         * @params type of object, organization id and angular index
         * @retruns success true when saved*/
        vm.addDetail = function (type, org_id, index) {
            if (type === 'phone') {
                var orgphone = {
                    phone_number: vm.phone,
                    type_id: vm.type.id,
                    entity_id: org_id,
                    phone_nature_id: 2
                }
               CRUDservice.save({object: 'phone_numbers'}, orgphone).$promise.then(function(){
                     updatePhone();
                    toastService.show('Phone number for organization added');

                    vm.addphone[index] = false;
                });

            } else if (type === 'email') {
                var orgemail = {
                    email: vm.email,
                    type_id: vm.type.id,
                    entity_id: org_id

                }
                var success = CRUDservice.save({object: 'emails'}, orgemail);
                if (success) {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.emails = data.emails;
                    });
                    toastService.show('Email for organization added');


                    vm.addemail[index] = false;




                }
            } else if (type === 'date') {
                var start = moment(vm.date).format('YYYY-MM-DD HH:mm:ss');
                var date = {
                    description: vm.description,
                    date: start,
                    type_id: vm.type.id,
                    entity_id: org_id

                }
                var success = CRUDservice.save({object: 'dates'}, date);
                if (success) {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.dates = data.dates;
                    });
                    toastService.show('Date for organization added');


                    vm.addDay[index] = false;



                }
            }
        }
        function updatePhone(){
            CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.phones = data.phones;
                    });
        }
        vm.deletePhone = function (tid) {
            if (confirm('Are you sure you want to delete this phone')) {
                CRUDservice.delete({object: 'phone_numbers', id: tid}).$promise.then(function (data) {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.phones = data.phones;
                    });
                    toastService.show('Phone number deleted.');
                });
            }
        }
        vm.deleteEmail = function (tid) {
            if (confirm('Are you sure you want to delete this email')) {
                CRUDservice.delete({object: 'emails', id: tid}).$promise.then(function (data) {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.emails = data.emails;
                    });
                    toastService.show('Email deleted.');
                });
            }
        }
        vm.deleteDate = function (tid) {
            if (confirm('Are you sure you want to delete this date')) {
                CRUDservice.delete({object: 'dates', id: tid}).$promise.then(function (data) {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.dates = data.dates;
                    });
                    toastService.show('Date deleted.');
                });
            }
        }
        CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
            $rootScope.priority = data.priorities;
            $rootScope.task_status = data.status;
            $rootScope.task_type = data.task_types;
        });
        function updateTask() {
            CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                vm.tasks = data.tasks;

            });
        }
        vm.addTask = function (ev, org_id) {

            $rootScope.pin_org = org_id;
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;
                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;

            });
            dialogService.show(ev, 'addTask', addTasks, true);
        };

        function addTasks($scope, $rootScope, dialogService) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities
                };

                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    var task_type = data.task_type;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: task_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: task_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);

                    }
                    ;
                    var pin = {
                        task_id: task.id,
                        entity_id: $rootScope.pin_org,
                        type_id: vm.type.id
                    };
                    var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.tasks = data.tasks;
                    });

                    toastService.show('Task Added');
                    dialogService.close();
                });


            };
        }
        /*load the add note dialog*/
        $scope.pinNote = function (ev, org_id) {
            $rootScope.noteorg_id = org_id;
            dialogService.show(ev, 'addNote', pinNote, true);
        };

        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                pinService.pinNote(noteData, $rootScope.noteorg_id, vm.type.id);
            }
        }
        ;

        $scope.editNote = function (ev, note_id) {
            $rootScope.editnote_id = note_id;
            CRUDservice.get({object: 'notes', id: note_id}).$promise.then(function (data) {
                $rootScope.note = data.note.note;
            });
            dialogService.show(ev, 'addNote', editNote, true);
        };

        function editNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                CRUDservice.update({object: 'notes', id: $rootScope.editnote_id}, noteData).$promise.then(function () {
                    updateNoteScope();
                    dialogService.close();
                    toastService.show('Note edited');
                });
            };
        }
        ;

        $scope.deleteNote = function (id) {
            CRUDservice.delete({object: 'notes', id: id}).$promise.then(function () {
                updateNoteScope();
                toastService.show('Note deleted');
            });
        };
        function updateNoteScope() {
            CRUDservice.query({object: 'organizations'})
                    .$promise.then(function (data) {
                        vm.notes = data.notes;
                    });
        }
        ;


        vm.deleteTask = function (tid) {
            //$scope.task=CRUDservice.get({object:'tasks',id:tid});
            if (confirm('Do you want to delete this task?')) {
                CRUDservice.delete({object: 'tasks', id: tid}).$promise.then(function () {
                    CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                        vm.tasks = data.tasks;
                    });
                    dialogService.close();
                    toastService.show('task deleted');
                });

            }


        };
        /*Function that loads the add event modal*/
        $scope.addEvent = function (ev, org_id) {
            $rootScope.pin_org = org_id;
            dialogService.show(ev, 'addEvent', add, true);
        }
        /*Controller to save a particular event*/
        function add($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.addEvent = function () {
                var start = moment($scope.data.start).format('YYYY-MM-DD HH:mm:ss');

                var end = moment($scope.data.end).format('YYYY-MM-DD HH:mm:ss');

                var event = {
                    title: $scope.title,
                    start: start,
                    end: end,
                    allDay: false
                }

                CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {

                    toastService.show('event added');
                    var event_id = data.event.id;
                    var pin = {
                        event_id: event_id,
                        entity_id: $rootScope.pin_org,
                        type_id: vm.type.id
                    };

                    CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {

                        CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                            vm.events = data.events;
                        });
                        dialogService.close();
                    });
                });
            }

        }
        CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
            $rootScope.plans = data.payment_plans;
            //console.log('Plans are ' + $rootScope.plans);
        });
        CRUDservice.query({object: 'opp_products'}).$promise.then(function (data) {
            $rootScope.opp_products = data.opp_prods;
            //console.log('Opp Products are' + $rootScope.opp_products);
        });
        // get the inventories here
        CRUDservice.query({object: 'product_inventories'}).$promise.then(function (data) {
            $rootScope.inventories = data.product_inventories;
            console.log('Inventories are : ' + $rootScope.inventories.length);
        });
        CRUDservice.query({object: 'products'}).$promise.then(function (data) {
            $rootScope.product = data.products;
        });
        // Now Populate The Opportunities
        CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
            // Opportunities

            // Pipeline Stages
            $rootScope.stages = data.stages;
            // Bid Types
            $rootScope.bidTypes = data.bid_type;
            // Users
            $scope.users = data.users;
            // Type id
            $rootScope.type = data.type;
            // Pipelines
            $scope.pipelines = data.pipelines;
        });
        /* =====================================================================
         * 
         * addOpportunity Service
         * @param contact id, contact first name andd last name
         * @returns success/failure
         * 
         * =====================================================================
         */
        vm.addTunity = function (ev, org_id, name) {
            $rootScope.show_customers = true;
            $rootScope.show_org = true;
            $rootScope.org_id = org_id;

            $rootScope.name = name;
            // Load The Resources Here

            $mdDialog.show({
                scope: $scope.$new(),
                controller: addTunity,
                templateUrl: '../views/dialogs/addTunity.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        /* =====================================================================
         * 'Contoller' function to add an opportunity
         * =====================================================================
         */
        function addTunity($scope, $state, filterFilter) {
            //return stages in a given pipeline      
            $scope.p_plan = [];
            $scope.addNew = function () {
                //console.log($scope.amount);
                // get opportunitites data from the dialog form
                var start = moment($scope.close_date).format('YYYY-MM-DD HH:mm:ss');

                var opportunities = {
                    name: $scope.oppname,
                    entity_id: $rootScope.org_id,
                    type_id: vm.type.id,
                    description: $scope.description,
                    user_responsible_id: $scope.assigned_to,
                    pipeline_id: $scope.stage,
                    close_date: start
                }
                //console.log('Saving this' + opportunities);
                //console.log('Customer Id is ' + $scope.customer_id);
                var currentStage = $scope.stage;
                //console.log('The stage id is ' + currentStage);
                // save opportunities
                CRUDservice.save({object: 'opportunities'}, opportunities)
                        .$promise.then(function (data) {
                            //get the opportunity id.
                            $scope.opportunity_id = data.opportunity_id;
                            $rootScope.type.id = data.op_type;
                            //log the id 
                            //console.log('Opportunnity Id is: '+$scope.opportunity_id);
                            console.log($scope.p_plan[2]);
                            // save the products to the products table                           
                            angular.forEach($scope.p_plan, function (plan, i) {
                                //console.log('Got it' + plan + ' got it too' + $scope.plan[i]);
                                var productsData = {
                                    opportunity_id: data.opportunity_id,
                                    inventory_id: $scope.p_plan[i],
                                    payment_plan_id: $scope.plan[i],
                                    //quantity: $scope.quantity[i],
                                    value: vm.payment_plan_value($scope.plan[i])
                                };
                                //console.log('Plans are' + $scope.plan[i]);
                                //console.log('Value equals:' + vm.payment_plan_value($scope.plan[i]));
                                CRUDservice.save({object: 'opp_products'}, productsData);
                            });
                            // now let's save the current stage for the app
                            // since we are saving for the first time, previous
                            // stage id is equated to 
                            // notes < entry stage for opportunity >
                            var current_stage = {
                                opportunity_id: $scope.opportunity_id,
                                current_stage_id: currentStage,
                                notes: 'Initial entry stage for this opportunity'
                            };
                            /*
                             * Save the owners
                             */
                            if ($scope.user_selected.length > 0) {
                                angular.forEach($scope.user_selected, function (value, key) {
                                    var ownersData = {
                                        user_id: value.id,
                                        entity_id: data.opportunity_id,
                                        type_id: $rootScope.type.id
                                    };
                                    CRUDservice.save({object: 'owners'}, ownersData);
                                });
                            } else {
                                var ownersData = {
                                    user_id: $rootScope.currentUser.id,
                                    entity_id: data.opportunity_id,
                                    type_id: $rootScope.type.id
                                };
                                CRUDservice.save({object: 'owners'}, ownersData);
                            }
                            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                            //console.log(success);
                            //show success dialog here
                            toastService.show('Opportunity Created Successfully');
                            $mdDialog.hide();
                            //redirect();
                        });
            };

        }
        /* =====================================================================
         * Calculate the total value for a certain payment plan
         * 
         */
        vm.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };
        // Filter plans for add Tunity dialog
        $scope.getPlan = function (id) {
            // console.log('Plans are:' + $rootScope.plans.length)
            return filterFilter($rootScope.plans, {product_id: id});
        };
        $scope.isDone = function (task_id) {
            if (confirm('Mark task as done?')) {
                CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {

                    $rootScope.task_status = data.status;
                    angular.forEach($rootScope.task_status, function (value, key) {

                        if (value.status == 'Done') {
                            vm.done_id = value.id;

                        }
                    });
                    var done = {
                        status: vm.done_id
                    }
                    console.log(done);
                    CRUDservice.update({object: 'tasks', id: task_id}, done).$promise.then(function () {

                        CRUDservice.query({object: 'organizations'}).$promise.then(function (data) {
                            vm.tasks = data.tasks;
                        });
                        toastService.show('Task marked as done');
                    });


                });

            }
        }
        $scope.newEmail = function (ev, org_id) {
            $rootScope.org_id = org_id;
            $rootScope.emails = [];
            $rootScope.emails = filterFilter(vm.emails, {entity_id: org_id});
            if (!$rootScope.emails.length) {
                toastService.show('Please add an email first');
            } else {
                CRUDservice.get({object: 'organizations', id: org_id}).$promise.then(function (data) {

                    $rootScope.organ = data.organization;
                    dialogService.show(ev, 'sendmail', send, true);

                });

            }


        };
        function send($scope,$timeout) {
            $scope.attach_id=[];
            $scope.sendMail = function (files) {

                var mail = {
                    from: $rootScope.currentUser.email,
                    to: $scope.to,
                    message: $scope.message,
                    receiver: $rootScope.organ.name,
                    sender: $rootScope.currentUser.name,
                    subject: $scope.subject,
                    entity_id: $rootScope.org_id,
                    type_id: vm.type.id,
                    attach_ids:$scope.attach_id
                   

                }
               
                 dialogService.close();
                $http.post('api/emails/send', mail).success(function (data) {
                    console.log('email id '+data.email_id);
                    var email_id={email_id:data.email_id};
                    for(var i=0;i<$scope.attach_id.length;i++){
                        CRUDservice.update({object:'email_attachments',id:$scope.attach_id[i]},email_id);
                    }
                    toastService.show('Email sent');
                   
                });

            }
            $scope.uploadFiles = function (files, errFiles) {
                $scope.files = files;
                $scope.errFiles = errFiles;
                
                angular.forEach(files, function (file,key) {
                    file.upload = Upload.upload({
                        url: 'api/attachments',
                        data: {file: file,
                                type_id:vm.type.id}
                    });

                    file.upload.then(function (response) {
                        console.log('attach id '+response.data.attach_id);
                        
                        $timeout(function () {
                            file.result = response.data;
                            $scope.attach_id[key]=response.data.attach_id;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 *
                                evt.loaded / evt.total));
                    });
                });
                console.log($scope.attach_id);
            }
        }
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }
//            console.log('customers '+$rootScope.contacts.length)
//            for(var i=0;i<$rootScope.contacts.length;i++){
//                vm.customers_selected[i]=true;
//            }
            
            angular.forEach(vm.organizations, function (value) {
                value.selected = $scope.selectAll;
            });
           

        };
        $scope.deleteSome = function () {
            if (confirm('Are you sure you want to delete these items?')) {
                angular.forEach(vm.organizations, function (value) {
                    if (value.selected) {

                        CRUDservice.delete({object: 'organizations', id: value.id}).$promise.then(function (data) {
                            updateScope();
                        });

                    }
                });
                toastService.show('Successfully deleted');
            }
        }
        $scope.sendBulk=function(ev){
            
        $scope.chosen_emails=[];
    
       
            angular.forEach(vm.organizations, function (value,key) {
                if (value.selected) {
                    angular.forEach(vm.emails,function(val){
                        if(val.entity_id===value.id){
                            val.name=value.name;
                            $scope.chosen_emails.push(val);
                            

                        }
                        
                    });
                      
                }
            });
            
            console.log($scope.chosen_emails);
            bulkEmailService.send(ev,$scope.chosen_emails);    
        }


        $scope.pinDocument = function (ev, org_id) {
            vm.docorg = org_id;

            dialogService.show(ev, 'pinDocument', pinDocument, true);
        };

        function pinDocument($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.docname = [];
            $scope.uploadDoc = function (files) {
                var docdata={
                    users:$scope.users_chosen,
                    doctype:vm.doc_type
                };
                uploadService.upload(files, vm.type.id, vm.docorg, $scope.docname,$scope.permission_id,docdata);
            };
        }

        $scope.deleteDoc = function (id) {
            uploadService.delete(id);
            updateDocScope();
        };
        function updateDocScope() {
            CRUDservice.get({object: 'organizations'}).$promise.then(function (data) {
                $scope.documents = data.documents;
            });
        }
    }

})();