(function(){
    'use strict';
    angular.module('authApp').controller('emailController',emailController);
    function emailController($scope, $mdDialog, $state, $rootScope, CRUDservice,
            filterFilter, pinService,
            toastService, dialogService, moment,$stateParams,$http,Upload,$timeout){
                var vm=this;
                //$scope.emails=[];
                $scope.entity_id=$stateParams.id;
                console.log('Enntity id '+$stateParams.id);
                console.log('type id '+$stateParams.type);
                $scope.type_id=$stateParams.type;
                CRUDservice.query({object:'emails'}).$promise.then(function(data){
                    $scope.emails=data.emails;
                    var emails=filterFilter($scope.emails,{type_id:$stateParams.type});
                    $scope.email.emails=filterFilter(emails,{entity_id:$stateParams.id});
                console.log($scope.email.emails);
                });
                CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                    $scope.emails=data.contact_emails;
                    $scope.contact = data.contact;
                    console.log($scope.emails);
                });
                $scope.attach_id=[];
                $scope.sendMail=function(){
                    var mail={
                        from:$rootScope.currentUser.email,
                        to:$scope.to,
                        message:$scope.message,
                        receiver:$scope.contact.firstname,
                        sender:$rootScope.currentUser.name,
                        subject:$scope.subject,
                        entity_id:$stateParams.id,
                        type_id:$stateParams.type,
                         attach_ids:$scope.attach_id
                        
                    }
                    $http.post('api/emails/send',mail).success(function(data){
                         var email_id={email_id:data.email_id};
                    for(var i=0;i<$scope.attach_id.length;i++){
                        CRUDservice.update({object:'email_attachments',id:$scope.attach_id[i]},email_id);
                    }
                        $state.reload('users.contact',{id:$stateParams.id});
                        toastService.show('Email sent');
                    });
                    
                }
           
            $scope.uploadFiles = function (files, errFiles) {
                $scope.files = files;
                $scope.errFiles = errFiles;
                
                angular.forEach(files, function (file,key) {
                    file.upload = Upload.upload({
                        url: 'api/attachments',
                        data: {file: file,
                                type_id:$stateParams.type}
                    });

                    file.upload.then(function (response) {
                        console.log('attach id '+response.data.attach_id);
                        
                        $timeout(function () {
                            file.result = response.data;
                            $scope.attach_id[key]=response.data.attach_id;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 *
                                evt.loaded / evt.total));
                    });
                });
                console.log($scope.attach_id);
            }
    }
})();
