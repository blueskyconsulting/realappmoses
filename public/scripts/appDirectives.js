//this directive is used to remove the last email 
//input elements in the space for emails div specified in the view
angular
        .module('authApp')
        .filter('dateToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
        })
        .directive('removeemail', function () {
            return function (scope, element) {
                element.bind('click', function () {
                    scope.emailcount--;
                    document.getElementById('space-for-emails').removeChild(document.getElementById('space-for-emails').lastChild);
                });
            };
        });


//this directive is used to add an email 
//input elements in the space for emails div specified in the view
angular
        .module('authApp')
        .directive("addemail", function ($compile) {
            return function (scope, element, attrs) {
                var count = scope.emailcount;
                element.bind("click", function () {
                    angular.element(document.getElementById('space-for-emails'))
                            .append($compile("<div><div class='row' ><div class='form-group col-sm-6'>\n\
                                                  <input type='email'  class='col-sm-6 form-control'  ng-model='email[" + scope.emailcount + "]' placeholder='Email" + ++count + "'>\n\
                                                  </div>\n\
                                              <div class='from-group col-sm-6'>\n\
                                                    <label ng-repeat='e_nature in email_natures' class='radio-inline'>\n\
                                                        <input type='radio' name='emailradio"+scope.emailcount+"'\n\
                                                            value='{{e_nature.id}}' ng-model='email_nature_id[" + scope.emailcount++ + "]'>{{e_nature.email_nature}}\n\
                                                    </label>\n\
                                              </div>\n\
                                              </div>\n\
                                              <a addemail class='btn btn-toolbar' type='button'>+Add more emails</a>\n\
                                              <a removeemail class='btn btn-toolbar text-danger' type='button'>-Del added email</a>\n\
                                              </div>")(scope));
                });
            };
        });
//this directive is used to remove the last phone number 
//input elements in the space for emails div specified in the view
angular
        .module('authApp').directive('removephoneno', function ($compile) {
    return function (scope, element) {
        element.bind('click', function () {
            document.getElementById('space-for-phonenos').removeChild(document.getElementById('space-for-phonenos').lastChild);
            delete(scope.phone[1]);
        });
    };
});


//this directive is used to add a phone number  
//input elements will appear  in the space for emails <div> specified in the view
angular
        .module('authApp')
        .directive("addphoneno", function ($compile) {
            return function (scope, element, attrs) {
                var count = scope.phonenocount;
                element.bind("click", function () {
                    angular.element(document.getElementById('space-for-phonenos'))
                            .append($compile("<div><div class='row' ><div class='form-group col-sm-6'>\n\
                                                  <input type='text'  class='col-sm-6 form-control' ng-model='phone[" + scope.phonenocount + "]' placeholder='Phone" + ++count + "'>\n\
                                                  </div>\n\
                                                  <div class='form-group col-sm-6'>\n\
                                                        <label ng-repeat='p_nature in phone_natures' class='radio-inline'>\n\
                                                            <input type='radio' name='phoneradio"+scope.phoneno_count+"'\n\
                                                                value='{{p_nature.id}}' ng-model='phone_nature_id[" + scope.phonenocount++ + "]'>{{p_nature.phone_nature}}\n\
                                                        </label>\n\
                                                  </div>\n\
                                              </div>\n\
                                              <a addphoneno class='btn btn-toolbar' type='button'>+Add more numbers</a>\n\
                                              <a removephoneno class='btn btn-toolbar text-danger' type='button'>-Del added number</a>\n\
                                              </div>")(scope));
                });
            };
        });
//this directive is used to remove the last phone number 
//input elements in the space for emails div specified in the view
angular
        .module('authApp').directive('removeuser', function ($compile) {
    return function (scope, element) {
        element.bind('click', function () {
            document.getElementById('space-for-users').removeChild(document.getElementById('space-for-users').lastChild);
        });
    };
});


//this directive is used to add a user dropdown element for assginment  
//input elements will appear  in the space for emails <div> specified in the view
angular
        .module('authApp')
        .directive("adduser", function ($compile) {
            return function (scope, element, attrs) {
                var count = scope.usercount;
                element.bind("click", function () {
                    angular.element(document.getElementById('space-for-users'))
                            .append($compile("<div><div class='form-group col-sm-12'>\n\
                                                        <select class='form-control' ng-model='user_id'>\n\
                                                            <option ng-repeat='user in users' value='{{user.id}}'>{{user.name}}</option>\n\
                                                         </select>\n\
                                                    </div>\n\
                                                    <a adduser class='btn btn-toolbar' type='button'>+Assign more</a>\n\
                                                    <a removeuser class='btn btn-toolbar text-danger' type='button'>-Del added field</a>\n\
                                                </div>")(scope));
                });
            };
        }).filter('isEmpty', [function () {
        return function (object) {
            return angular.equals({}, object);
        }
    }]).directive("tree", function(RecursionHelper) {
    return {
        restrict: "E",
        scope: {family: '='},
        template: 
        '<p>{{ family.name }}{{test }}</p>'+
            '<ul>' + 
                '<li ng-repeat="child in family.children">' + 
                    '<tree family="child"></tree>' +
                '</li>' +
            '</ul>',
        compile: function(element) {
            return RecursionHelper.compile(element, function(scope, iElement, iAttrs, controller, transcludeFn){
                // Define your normal link function here.
                // Alternative: instead of passing a function,
                // you can also pass an object with 
                // a 'pre'- and 'post'-link function.
            });
        }
    };
  });