(function () {
    "use strict";
    angular.module('authApp').controller('productController', productController);
    function productController($scope, $mdDialog, CRUDservice, toastService, dialogService,
            $rootScope, pinService,filterFilter,$stateParams,bulkDelService) {


        $scope.productflag = [];
        $scope.editedproduct = [];
        $scope.cashflag = [];
        $scope.installflag = [];
        $scope.cash = [];
        $scope.install = [];
        $scope.depo = [];
        $scope.installno = [];
        $scope.note = [];
        $scope.displaynumber = 4;
        // For the payment itself
        $scope.plan = [];
        $scope.prod = [];
        // For the checkbox
        $scope.p_plan = [];
        $scope.empty=false;
        var vm = this;
        $scope.search=$stateParams.search;
        CRUDservice.query({object: 'products'}).$promise.then(function (data) {
            $scope.products = data.products;
            $rootScope.projects = data.projects;
            if($rootScope.projects.length==0){
                $scope.empty=true;
            }
            vm.type_id = data.type_id;
            vm.active_id = data.active_id;
            var productcount = $scope.products.length;
            for (var i = 0; i < productcount; i++) {
                $scope.productflag[i] = true;
            }
            ;

            angular.forEach($scope.products, function (value, key) {
                $scope.editedproduct[key] = value.name;
            });
        });

        $scope.showProduct = function (id) {
            vm.sel_product_id = id;
            CRUDservice.get({object: 'products', id: id}).$promise.then(function (data) {
                $scope.product_cash = data.product_cash;
                $scope.product_installments = data.product_installments;
                $scope.product_note = data.product_notes;
                $scope.cashcount = $scope.product_cash.length;
                $scope.product_notes = data.product_notes;
                $scope.product_inventories=data.product_inventories;
                console.log($scope.product_installments);
                for (var i = 0; i < $scope.cashcount; i++) {
                    $scope.cashflag[i] = true;
                }
                ;

                $scope.installcount = $scope.product_installments.length;
                for (var i = 0; i < $scope.installcount; i++) {
                    $scope.installflag[i] = true;
                }
                ;

                angular.forEach($scope.product_cash, function (value, key) {
                    $scope.cash[key] = value.deposit;
                });

                angular.forEach($scope.product_installments, function (value, key) {
                    $scope.depo[key] = value.deposit;
                    $scope.install[key] = value.installment;
                    $scope.installno[key] = value.installment_no;
                });
                var notecount = $scope.product_notes.length;
                $scope.editnoteflag = [];
                $scope.editnoteflag[notecount];
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[i] = true;
                }
                angular.forEach($scope.product_notes, function (value, key) {
                    $scope.note[key] = value.note;
                });
            });
        };


        $scope.addProduct = function (ev) {
            dialogService.show(ev, 'addProduct', addProduct, true);
        };

        function addProduct($scope) {
            $scope.diaginstallments = [];
            $scope.close = function () {
                dialogService.close();
            }

            $scope.createProduct = function () {
                var productData = {
                    name: $scope.products.name,
                    project_id: $scope.project_id,
                    units_no: $scope.products.units,
                    units_from: $scope.products.from,
                    units_to: $scope.products.to
                };

                CRUDservice.save({object: 'products'}, productData).$promise.then(function (data) {
                    $scope.last_product_id = data.last_product_id;
                    if ($scope.diagcash > 0) {
                        var cashData = {
                            deposit: $scope.diagcash,
                            installment: 0,
                            installment_no: 0,
                            product_id: $scope.last_product_id,
                            plan_status_id: vm.active_id
                        };
                        $scope.diaginstallments.push(cashData);
                    }
                    ;
                    if ($scope.diaginstallments.length > 0) {
                        angular.forEach($scope.diaginstallments, function (value, key) {
                            console.log(value);
                            if (value.installment > 0) {
                                angular.extend($scope.diaginstallments[key], {product_id: $scope.last_product_id});
                                console.log($scope.diaginstallments);
                                console.log(value);
                            }
                            CRUDservice.save({object: 'payment_plans'}, value);
                        });
                    }
                    ;
                    dialogService.close();
                    updateProductScope();
                    toastService.show("Product Added");
                });
            };
            $scope.addDiagInstall = function () {
                var newInstall = {
                    deposit: $scope.diagdepo,
                    installment_no: $scope.diaginstallno,
                    installment: $scope.diaginstall,
                    plan_status_id: vm.active_id
                };
                $scope.diaginstallments.push(newInstall);
                $scope.diagdepo = "";
                $scope.diaginstallno = "";
                $scope.diaginstall = "";
            };

            $scope.delDiagInstall = function (index) {
                $scope.diaginstallments.splice(index, 1);
            };
        }
        ;

        $scope.editProduct = function (ev, product_id) {
            vm.editProdcuctId = product_id;
            CRUDservice.get({object: 'products', id: product_id}).$promise.then(function (data) {
//                console.log(data);
                $rootScope.name = data.product.name;
                $rootScope.project_id = data.product.project_id;
                $rootScope.units = Number(data.product.units_no);
                $rootScope.from = Number(data.product.units_from);
                $rootScope.to = Number(data.product.units_to);
                dialogService.show(ev, 'editProduct', editProduct, true);
            });
        };

        function editProduct($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.editProduct = function () {
                var productData = {
                    name: $scope.name,
                    project_id: $scope.project_id,
                    units_no: $scope.units,
                    units_from: $scope.from,
                    units_to: $scope.to
                };
                CRUDservice.update({object: 'products', id: vm.editProdcuctId}, productData).$promise.then(function (data) {
                    updateProductScope();
                    dialogService.close();
                    toastService.show('Product Edited');
                });
            };
        }
        ;

        $scope.deleteProduct = function (product_id) {
            if (confirm('Are you sure you want to delete this Product')) {
                CRUDservice.delete({object: 'products', id: product_id}).$promise.then(function () {
                    updateProductScope();
                    toastService.show('Product Deleted');
                });
            }
            ;
        };

        $scope.togglecash = function (index) {
            $scope.cashflag[index] = !$scope.cashflag[index];
        };

        $scope.toggleinstall = function (index) {
            $scope.installflag[index] = !$scope.installflag[index];
        };

        $scope.toggleProduct = function (index) {
            $scope.productflag[index] = !$scope.productflag[index];
        };

        $scope.savenewcash = function () {
            var newcashData = {
                product_id: vm.sel_product_id,
                deposit: $scope.newcash,
                installment: 0,
                installment_no: 0,
                plan_status_id: vm.active_id,
                newcash: true
            };
            console.log(newcashData);
            CRUDservice.save({object: 'payment_plans'}, newcashData).$promise.then(function () {
                toastService.show('Cash payment plan added');
                updateCashScope();
                $scope.newcash = "";
            });
        };

        $scope.savenewinstall = function () {
            var newinstallData = {
                product_id: vm.sel_product_id,
                deposit: $scope.newdepo,
                installment: $scope.newinstall,
                installment_no: $scope.newinstallno,
                plan_status_id: vm.active_id
            };
            CRUDservice.save({object: 'payment_plans'}, newinstallData).$promise.then(function () {
                updateInstallmentScope();
                toastService.show('Installment payment plan added');
                $scope.newdepo = "";
                $scope.newinstall = "";
                $scope.newinstallno = "";
            });
        };

        $scope.editCash = function (index, id) {
            var cashData = {
                deposit: $scope.cash[index],
                installment: 0,
                installment_no: 0
            };
            CRUDservice.update({object: 'payment_plans', id: id}, cashData).$promise.then(function () {
                toastService.show('Cash payment plan updated');
                updateCashScope();
            });
        };

        $scope.editInstall = function (index, id) {
            var installData = {
                deposit: $scope.depo[index],
                installment: $scope.install[index],
                installment_no: $scope.installno[index]
            };
            CRUDservice.update({object: 'payment_plans', id: id}, installData).$promise.then(function () {
                toastService.show('Installment payment plan updated');
                updateInstallmentScope();
            });
        };

        $scope.editProductName = function (index, id) {
            var productData = {
                name: $scope.editedproduct[index]
            };
            CRUDservice.update({object: 'products', id: id}, productData).$promise.then(function () {
                toastService.show('Product name updated');
                updateProductScope();
                $scope.productflag[index] = true;
            });
        };
        $scope.deleteCash = function (id) {
            CRUDservice.delete({object: 'payment_plans', id: id}).$promise.then(function (value) {
                toastService.show('Cash payment plan deleted');
                updateCashScope();
                console.log(value);
            });
        };

        $scope.deleteInstall = function (id) {
            CRUDservice.delete({object: 'payment_plans', id: id}).$promise.then(function (value) {
                toastService.show('Installment payment plan deleted');
                updateInstallmentScope();
            });
        };

        function updateCashScope() {
            CRUDservice.get({object: 'products', id:vm.sel_product_id}).$promise.then(function (data) {
                $scope.product_cash = data.product_cash;
                $scope.cashcount = $scope.product_cash.length;
                for (var i = 0; i < $scope.cashcount; i++) {
                    $scope.cashflag[i] = true;
                }
                ;
            });
        }
        ;

        function updateInstallmentScope() {
            CRUDservice.get({object: 'products', id: vm.sel_product_id}).$promise.then(function (data) {
                $scope.product_installments = data.product_installments;
                $scope.installcount = $scope.product_installments.length;
                for (var i = 0; i < $scope.installcount; i++) {
                    $scope.installflag[i] = true;
                }
                ;
            });
        }
        ;

        function updateProductScope() {
            CRUDservice.query({object: 'products'}).$promise.then(function (data) {
                $scope.products = data.products;

                var productcount = $scope.products.length;
                for (var i = 0; i < productcount; i++) {
                    $scope.productflag[i] = true;
                }
                ;

                angular.forEach($scope.products, function (value, key) {
                    $scope.editedproduct[key] = value.name;
                });
            });
        }
        ;

        $scope.pinNote = function (ev, product_id) {
            vm.productnote = product_id;
            dialogService.show(ev, 'addNote', pinNote, true);
        };

        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                pinService.pinNote(noteData, vm.productnote, vm.type_id);
                updateNoteScope();
            };
        }
        ;

        $scope.editNote = function (index, id) {
            var noteData = {
                note: $scope.note[index]
            };
            CRUDservice.update({object: 'notes', id: id}, noteData).$promise.then(function () {
                updateNoteScope();
                toastService.show('Note edited');
                $scope.editnoteflag[index] = !$scope.editnoteflag[index];
            });
        };

        $scope.toggleNoteFlag = function (index) {
            $scope.editnoteflag[index] = !$scope.editnoteflag[index];
        };

        $scope.deleteNote = function (id) {
            CRUDservice.delete({object: 'notes', id: id}).$promise.then(function () {
                updateNoteScope();
                toastService.show('Note deleted');
            });
        };
        function updateNoteScope() {
            CRUDservice.get({object: 'products', id: vm.sel_product_id}).$promise.then(function (data) {
                $scope.product_notes = data.product_notes;
                var notecount = $scope.product_notes.length;
                $scope.editnoteflag = [];
                $scope.editnoteflag[notecount];
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[i] = true;
                }
                angular.forEach($scope.product_notes, function (value, key) {
                    $scope.note[key] = value.note;
                });
            });
        }
        ;

        $scope.resetNoteItems = function () {
            $scope.notedisplaynumber = "";
        };

        $scope.resetItems = function () {
            $scope.displaynumber = "";
        };

        $rootScope.getPlan = function (id) {
            return filterFilter($rootScope.plans, {product_id: id});
        };
        
        function getInventory(id){
            return filterFilter($rootScope.t_inventories, {product_id: id});
        }
        
        $scope.pinOpportunity = function (ev, product_id) {
            
            vm.product_opp = product_id;
            CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
                $rootScope.plans = data.payment_plans;
                console.log($rootScope.plans);
            });
            // get the inventories here
            CRUDservice.query({object: 'product_inventories'}).$promise.then(function (data) {
                $rootScope.t_inventories = data.product_inventories;
                console.log($rootScope.t_inventories);
                $rootScope.t_inventories=getInventory(vm.product_opp);
                console.log($rootScope.t_inventories);
//                console.log('Inventories are : ' + $rootScope.inventories.length);
            });
            // Now Populate The Opportunities
            CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
                // Opportunities
                $rootScope.opportunities = filterFilter(data.opportunities, {deleted_at: null});
                // Pipeline Stages
                $rootScope.stages = data.stages;
                // Bid Types
                $rootScope.bidTypes = data.bid_type;
                // Pipelines
                $scope.pipelines = data.pipelines;
            });
            /*
             * ===================================================================
             * Get Contacts data from the index function (laravel)
             * It also returns various parameter such as phone nature
             * email natures, salutations and address natures 
             * ===================================================================
             */
            CRUDservice.query({object: 'contacts'}).$promise.then(function (data) {
                $rootScope.customers = data.contacts;
            });
            dialogService.show(ev, 'newProdOpp', pinOpportunity, true);
        };

        function pinOpportunity($scope) {
            $scope.plan = [];
            $scope.prod = [];
            // For the checkbox
            $scope.p_plan = [];
            $scope.addNew = function () {
                console.log('Unafanya');
                //console.log($scope.amount);
                // get opportunitites data from the dialog form
                var opportunities = {
                    name: $scope.oppname,
                    customer_id: $scope.customer,
                    description: $scope.description,
                    user_responsible_id: $scope.assigned_to,
                    pipeline_id: $scope.stage,
                    close_date: $scope.closeDate
                };
                //console.log('Saving this' + opportunities);
                console.log('Customer Id is ' + $scope.customer);
                var currentStage = $scope.stage;
                //console.log('The stage id is ' + currentStage);
                // save opportunities
                CRUDservice.save({object: 'opportunities'}, opportunities)
                        .$promise.then(function (data) {
                            //get the opportunity id.
                            $scope.opportunity_id = data.opportunity_id;
                            //log the id 
                            //console.log($scope.opportunity_id);
                            //console.log($scope.p_plan[2]);
                            // save the products to the products table 
                            console.log($scope.p_plan);
                            angular.forEach($scope.p_plan, function (plan, i) {
                                console.log('Got it' + plan + ' got it too' + $scope.plan[i]);
                                var productsData = {
                                    opportunity_id: data.opportunity_id,
                                    inventory_id: $scope.p_plan[i],
                                    payment_plan_id: $scope.plan[i],
                                    //quantity: $scope.quantity[i],
                                    value: vm.payment_plan_value($scope.plan[i])
                                };
                                console.log('Plans are these ones' + $scope.p_plan[i]);
                                CRUDservice.save({object: 'opp_products'}, productsData);
                            });
                            // now let's save the current stage for the app
                            // since we are saving for the first time, previous
                            // stage id is equated to 
                            // notes < entry stage for opportunity >
                            var current_stage = {
                                opportunity_id: $scope.opportunity_id,
                                current_stage_id: currentStage,
                                notes: 'Initial entry stage for this opportunity'

                            };

                            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                            //console.log(success);
                            //show success dialog here
                            toastService.show('Opportunity Created Successfully');
                            $mdDialog.hide();
                        });
            };
        }
        ;
        
        vm.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach($scope.products, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete($scope.products,'products');
            updateProductScope();
            
            toastService.show('Successfully deleted');
            
        };
    }
    ;

})();
