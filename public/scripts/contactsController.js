/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for Contacts App
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('contactController', contactsController);
    function contactsController($q, $scope, $mdDialog, CRUDservice, toastService, dialogService,
            $rootScope, pinService, filterFilter, moment, $stateParams, permService, uploadService, bulkEmailService) {

        /*
         * Initialisations of variable used. Note array type models have to
         * be initialised
         */

        $rootScope.phone = [];
        $rootScope.email = [];
        $rootScope.phone_nature_id = [];
        $rootScope.email_nature_id = [];
        $scope.user_id = [];
        $scope.displaynumber = 5;
        $scope.isChecked = false;
        var vm = this; //this scope
        $scope.select = [];
        $scope.select = false;
        $rootScope.show_customers = false;
        $scope.p_plan = [];
        $scope.plan = [];
        $rootScope.pin = true;
        $rootScope.cust = false;

        vm.customers_selected = [];

        $scope.search = $stateParams.search;
        if ($scope.search.length) {
            $scope.displaynumber = "";
        }

        angular.forEach($rootScope.permissions, function (value, key) {
            if (value.permission == 'Private') {
                vm.privateid = value.id;
            } else if (value.permission == 'Assigned') {
                vm.assignedid = value.id;
            }
        });
        /*
         * ===================================================================
         * Get Contacts data from the index function (laravel)
         * It also returns various parameter such as phone nature
         * email natures, salutations and address natures 
         * ===================================================================
         */
        CRUDservice.query({object: 'contacts'}).$promise.then(function (data) {
            $rootScope.phone_natures = data.phone_natures;
            $rootScope.contacts = data.contacts;
            $rootScope.email_natures = data.email_natures;
            $rootScope.salutations = data.salutations;
            $rootScope.organizations = data.organizations;
            $rootScope.users = data.users;
            $rootScope.users_choose = data.usser;
            $rootScope.permissions = data.permissions;
            vm.assigned_id = data.assigned_id;
            vm.type_id = data.type_id.id;
            vm.contact_type_id = data.contact_type_id.id;

            vm.emails = data.emails;


            vm.doc_type = data.doc_type;

        });
        console.log('Users are:' + $rootScope.users);
        // customers are our contacts
        $rootScope.customers = $rootScope.contacts;
        // active plans, plan types etc        
        CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
            $rootScope.plans = data.payment_plans;
            console.log('Plans are ' + $rootScope.plans);
        });
        // get the inventories here
        CRUDservice.query({object: 'product_inventories'}).$promise.then(function (data) {
            $rootScope.inventories = data.product_inventories;
            console.log('Inventories are : ' + $rootScope.inventories.length);
        });
        CRUDservice.query({object: 'products'}).$promise.then(function (data) {
            $rootScope.product = data.products;
        });
        // Now Populate The Opportunities
        CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
            // Opportunities
            $rootScope.opportunities = filterFilter(data.opportunities, {deleted_at: null});
            // Pipeline Stages
            $rootScope.stages = data.stages;
            // Bid Types
            $rootScope.bidTypes = data.bid_type;
            // Users
            $scope.users = data.users;
            // Type id
            $rootScope.type = data.type;
            // Pipelines
            $scope.pipelines = data.pipelines;
        });
        /* =====================================================================
         * Edit Function For Contacts
         * 
         * Get a specific prestored data from the Database based on contact_id from
         * the database 
         * 
         * Initialise in rootscope to allow the dialog service to access the data
         * =====================================================================
         */
        vm.edit = function (ev, contact_id) {
            CRUDservice.get({object: 'contacts', id: contact_id}).$promise.then(function (data) {
                $rootScope.contact = data.contact;
                $rootScope.contact_organization = data.contact_organization;
                $rootScope.type_id = data.type_id.id;
                $rootScope.contact_salutation = data.contact_salutation;
                $rootScope.salutations = data.salutations;
                $rootScope.organizations = data.organizations;
                console.log(data.contact_address)
                $rootScope.address = Number(data.contact_address.address);
                vm.edited_address_id = data.contact_address.id;
                $rootScope.postal_code = Number(data.contact_address.postal_code);
                $rootScope.city = data.contact_address.city;
                $rootScope.country = data.contact_address.country;
                //call the dialog service
                dialogService.show(ev, 'editContact', editContact, true);
            });
        };
        /* ===------------------------------------------------------------------------------------------------------------------------------------------------==================================================================
         * Controller Function for Editing a contact
         * Binds to the Dialog Show
         * =====================================================================
         */
        function editContact($scope) {
            //Initalise a contactData variable to be passed to CRUDservice.update
            var contactData;
            $scope.close = function () {
                dialogService.close();
            };
            $scope.editContact = function (contact_id) {
                /*
                 * =============================================================
                 * Note when editing a contact we have to use the autocomplete
                 * feature for organization and salutation. 
                 * =============================================================
                 */
                contactData = {
                    firstname: $scope.contact.firstname,
                    lastname: $scope.contact.lastname
                };
                /*
                 * =============================================================
                 * $scope.selectedsal.originalObject is how we access the id 
                 * for the salected salutation object. If undefined we init
                 * salutation data to the text in the input field which will be 
                 * added to the database. if defined we take the specific id so 
                 * as to pass to the contact data.
                 * ($scope.selectedsal.originalObject.id)
                 * This is repeated for organization data with the diffence of 
                 * using $scope.selectedorg.originalObject
                 * =============================================================
                 */
                if ($scope.selectedsal.originalObject.id === undefined) {
                    var salutationData = {
                        salutation: $scope.selectedsal.originalObject
                    };
                } else {
                    var salutationData = {
                        salutation_id: $scope.selectedsal.originalObject.id
                    };
                }
                ;
                if ($scope.selectedorg !== undefined) {
                    if ($scope.selectedorg.originalObject.id === undefined) {
                        var orgData = {
                            name: $scope.selectedorg.originalObject
                        };
                    } else {
                        var orgData = {
                            organization_id: $scope.selectedorg.originalObject.id
                        };
                    }
                } else {
                    var orgData = {
                        organization_id: null
                    };
                }
                ;
                var address = {
                    address: $scope.address,
                    city: $scope.city,
                    postal_code: $scope.postal_code,
                    country: $scope.country,
                    entity_id: $scope.contact_last_id,
                    type_id: vm.type_id
                };
                /*
                 * =============================================================
                 * Using angular extend, we add both the salutationData and 
                 * orgData initialised differently based on their existense in 
                 * the database. 
                 * =============================================================
                 */
                angular.extend(contactData, salutationData, orgData);
                CRUDservice.update({object: 'contacts', id: contact_id}, contactData).$promise.then(function () {
                    updateScope();
                    CRUDservice.update({object: 'address', id: vm.edited_address_id}, address);
                    dialogService.close();
                    toastService.show('Contact Updated');
                });
            };
        }
        /* =====================================================================
         * Function to delete a contact
         * @returns success or failure
         * @param the contact_id we are deleting
         * =====================================================================
         */
        vm.delete = function (contact_id) {
            if (confirm('Are you sure you want to delete this?')) {
                // TODO:  Do something here if the answer is "Ok".
                CRUDservice.delete({object: 'contacts', id: contact_id}).$promise.then(function () {
                    updateScope();
                    toastService.show('Contact moved to Trash');
                });
            }
            ;
        };
        /* =====================================================================
         * Function to handle showing the addContact Dialog
         * To pass data to the dialog use the dialogOpen=true for the data
         * Also add the line scope: $scope.$new()
         * =====================================================================
         */

        vm.addContact = function (ev) {
            dialogService.show(ev, 'addContact', addContact, true);
        };
        /* =====================================================================
         * 'Controller' function to add a contact 
         * =====================================================================
         */

        function addContact($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.close = function () {
                dialogService.close();
            }
            //Initalise a contactData variable to be passed to CRUDservice.update
            var contactData;
            $scope.saveContact = function (croppedDataUrl) {


                /*
                 * =============================================================
                 * Note when saving a contact we have to use the autocomplete
                 * feature for organization and salutation. 
                 * =============================================================
                 */
                contactData = {
                    firstname: $scope.firstname,
                    lastname: $scope.lastname,
                    contact_type_id: vm.contact_type_id,
                    permission_id: $scope.permission_id,
                };
                /*
                 * =============================================================
                 * $scope.selectedsal.originalObject is how we access the id 
                 * for the salected salutation object. If undefined we init
                 * salutation data to the text in the input field which will be 
                 * added to the database. if defined we take the specific id so 
                 * as to pass to the contact data.
                 * ($scope.selectedsal.originalObject.id)
                 * This is repeated for organization data with the diffence of 
                 * using $scope.selectedorg.originalObject
                 * =============================================================
                 */
                if ($scope.selectedsal.originalObject.id === undefined) {
                    var salutationData = {
                        salutation: $scope.selectedsal.originalObject
                    };
                } else {
                    var salutationData = {
                        salutation_id: $scope.selectedsal.originalObject.id
                    };
                }
                ;
                if ($scope.selectedorg.originalObject.id === undefined) {
                    var orgData = {
                        name: $scope.selectedorg.originalObject,
                        permission_id:$scope.permission_id
                    };
                } else {
                    var orgData = {
                        organization_id: $scope.selectedorg.originalObject.id,
                        permission_id:$scope.permission_id
                    };
                }
                /*
                 * =============================================================
                 * Using angular extend, we add both the salutationData and 
                 * orgData initialised differently based on their existense in 
                 * the database. 
                 * =============================================================
                 */
                angular.extend(contactData, salutationData, orgData);
                CRUDservice.save({object: 'contacts'}, contactData).$promise.then(function (data) {
                    /*
                     * Saving a contact returns the previously inserted ID of the contact
                     */
                    $scope.contact_last_id = data.contact_last_id;
                    console.log($scope.contact_last_id);
                    /*
                     * Take the number of provided emails from the form
                     * loop through each email and save it
                     */
                    console.log($scope.email);
                    for (var i = 0; i < $scope.email.length; i++) {
                        var emailData = {
                            email: $scope.email[i],
                            email_nature_id: $scope.email_nature_id[i],
                            entity_id: $scope.contact_last_id,
                            type_id: vm.type_id
                        };
                        console.log(emailData);
                        CRUDservice.save({object: 'emails'}, emailData);
                    }
                    /*
                     * Take the number of provided phones from the form
                     * loop through each email and save it
                     */
                    console.log($scope.phone);
                    for (var i = 0; i < $scope.phone.length; i++) {
                        var phoneData = {
                            phone_number: $scope.phone[i],
                            phone_nature_id: $scope.phone_nature_id[i],
                            entity_id: $scope.contact_last_id,
                            type_id: vm.type_id
                        };
                        console.log(phoneData);
                        CRUDservice.save({object: 'phone_numbers'}, phoneData);
                    }
                    /*
                     * Take the number of provided users from the form
                     * loop through each email and save it
                     */
                    if ($scope.user_selected.length > 0) {
                        angular.forEach($scope.user_selected, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: $scope.contact_last_id,
                                type_id: vm.type_id
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: $scope.contact_last_id,
                            type_id: vm.type_id
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);
                    }
                    ;

                    var address = {
                        address: $scope.address,
                        city: $scope.city,
                        postal_code: $scope.postal_code,
                        country: $scope.country,
                        entity_id: $scope.contact_last_id,
                        type_id: vm.type_id
                    };
                    CRUDservice.save({object: 'address'}, address);


                    /*
                     * =========================================================
                     * call the permission service to save the permission
                     * First check if the permission is private or public
                     * the call the appriate function of the service
                     * =========================================================
                     */


                    if ($scope.permission_id == vm.privateid) {
                        permService.savePrivate($rootScope.currentUser.id, $scope.contact_last_id, vm.type_id);
                    } else if ($scope.permission_id == vm.assignedid) {
                        permService.saveAssigned($scope.users_chosen, $scope.contact_last_id, vm.type_id);
                    }

                    if (croppedDataUrl) {
                        uploadService.uploadContactPic(croppedDataUrl, vm.type_id, $scope.contact_last_id);
                    }
                    ;
                    //run update scope so that view is updated with changes
                    updateScope();
                    //close the dialog
                    dialogService.close();
                    //return messsage
                    toastService.show('Contact Added');
                });
            };
        }

        /*
         * =====================================================================
         * function updateScope
         * Updates scope so that data is updated in the view
         * =====================================================================
         */
        function updateScope() {
            CRUDservice.query({object: 'contacts'}).$promise.then(function (data) {
                $rootScope.contacts = data.contacts;
                $rootScope.salutations = data.salutations;
                $rootScope.organizations = data.organizations;
            });
        }
        /*
         * =====================================================================
         * Get the selected item from the item list.
         * @param none
         * @returns different objects for the contatc
         * =====================================================================
         */
        vm.selected = function () {
            $scope.select = true;
            console.log('Clicked...');
        };

        $scope.pinNote = function (ev, contact_id) {
            vm.notecontact = contact_id;
            dialogService.show(ev, 'addNote', pinNote, true);
        };

        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                pinService.pinNote(noteData, vm.notecontact, vm.type_id);

            };
        }
        ;

        $scope.resetItems = function () {
            $scope.displaynumber = "";
        };
        /* =====================================================================
         * 
         * addOpportunity Service
         * @param contact id, contact first name andd last name
         * @returns success/failure
         * 
         * =====================================================================
         */
        vm.addTunity = function (ev, contact_id, firstname, lastname) {
            $rootScope.customer_id = contact_id;
            $scope.firstname = firstname;
            $scope.lastname = lastname;
            // Load The Resources Here
            console.log('Contact ID:' + contact_id);
            $mdDialog.show({
                scope: $scope.$new(),
                controller: addTunity,
                templateUrl: '../views/dialogs/addTunity.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        /* =====================================================================
         * 'Contoller' function to add an opportunity
         * =====================================================================
         */
        function addTunity($scope, $state, filterFilter) {
            //return stages in a given pipeline            
            $scope.addNew = function () {
                //console.log($scope.amount);
                // get opportunitites data from the dialog form
                var start = moment($scope.close_date).format('YYYY-MM-DD HH:mm:ss');

                var opportunities = {
                    name: $scope.oppname,
                    entity_id: $rootScope.customer_id,
                    type_id: vm.type_id,
                    description: $scope.description,
                    user_responsible_id: $scope.assigned_to,
                    pipeline_id: $scope.stage,
                    close_date: start
                }
                //console.log('Saving this' + opportunities);
                console.log('Customer Id is ' + $scope.customer_id);
                var currentStage = $scope.stage;
                //console.log('The stage id is ' + currentStage);
                // save opportunities
                CRUDservice.save({object: 'opportunities'}, opportunities)
                        .$promise.then(function (data) {
                            //get the opportunity id.
                            $scope.opportunity_id = data.opportunity_id;
                            $rootScope.type.id = data.op_type;
                            //log the id 
                            //console.log('Opportunnity Id is: '+$scope.opportunity_id);
                            console.log($scope.p_plan[2]);
                            // save the products to the products table                           
                            angular.forEach($scope.p_plan, function (plan, i) {
                                console.log('Got it' + plan + ' got it too' + $scope.plan[i]);
                                var productsData = {
                                    opportunity_id: data.opportunity_id,
                                    inventory_id: $scope.p_plan[i],
                                    payment_plan_id: $scope.plan[i],
                                    //quantity: $scope.quantity[i],
                                    value: vm.payment_plan_value($scope.plan[i])
                                };
                                console.log('Plans are' + $scope.plan[i]);
                                console.log('Value equals:' + vm.payment_plan_value($scope.plan[i]));
                                CRUDservice.save({object: 'opp_products'}, productsData);
                            });
                            // now let's save the current stage for the app
                            // since we are saving for the first time, previous
                            // stage id is equated to 
                            // notes < entry stage for opportunity >
                            var current_stage = {
                                opportunity_id: $scope.opportunity_id,
                                current_stage_id: currentStage,
                                notes: 'Initial entry stage for this opportunity'
                            };
                            /*
                             * Save the owners
                             */
                            if ($scope.user_selected.length > 0) {
                                angular.forEach($scope.user_selected, function (value, key) {
                                    var ownersData = {
                                        user_id: value.id,
                                        entity_id: data.opportunity_id,
                                        type_id: $rootScope.type.id
                                    };
                                    CRUDservice.save({object: 'owners'}, ownersData);
                                });
                            } else {
                                var ownersData = {
                                    user_id: $rootScope.currentUser.id,
                                    entity_id: data.opportunity_id,
                                    type_id: $rootScope.type.id
                                };
                                CRUDservice.save({object: 'owners'}, ownersData);
                            }
                            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                            //console.log(success);
                            //show success dialog here
                            toastService.show('Opportunity Created Successfully');
                            $mdDialog.hide();
                            //redirect();
                        });
            };

        }
        /* =====================================================================
         * Calculate the total value for a certain payment plan
         * 
         */
        $scope.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };
        // Filter plans for add Tunity dialog
        $scope.getPlan = function (id) {
            // console.log('Plans are:' + $rootScope.plans.length)
            return filterFilter($rootScope.plans, {product_id: id});
        };
        /*Function that launches dialog to add task
         * @params event and contact id
         * @returns dialog event*/
        vm.addTask = function (ev, contact_id) {
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;
                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;
            });
            $rootScope.pin_contact = contact_id;

            dialogService.show(ev, 'addTask', addTasks, true);
        };
        /*Controler that add tasks*/
        function addTasks($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            /**/
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === '') {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities
                };

                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    var task_type = data.task_type;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: task_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: task_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);

                    }
                    ;
                    var pin = {
                        task_id: task.id,
                        entity_id: $rootScope.pin_contact,
                        type_id: vm.type_id
                    };
                    var success = CRUDservice.save({object: 'pinnedtasks'}, pin);

                    toastService.show('Task Added');
                    dialogService.close();
                });


            };
        }
        vm.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };

        /*Function to select the customers*/

        angular.forEach($rootScope.contacts, function (value) {
            value.selected = false;
        });
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }
//            console.log('customers '+$rootScope.contacts.length)
//            for(var i=0;i<$rootScope.contacts.length;i++){
//                vm.customers_selected[i]=true;
//            }
            angular.forEach($rootScope.contacts, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        /*Function to delete multiple items*/
        $scope.deleteSome = function () {
            if (confirm('Are you sure you want to delete these items?')) {
                angular.forEach($rootScope.contacts, function (value) {
                    if (value.selected) {

                        CRUDservice.delete({object: 'contacts', id: value.id}).$promise.then(function (data) {
                            updateScope();
                        });

                    }
                });
                toastService.show('Successfully deleted');
            }
        }
        /*Function to send bulk email*/
        $scope.sendBulk = function (ev) {

            $scope.chosen_emails = [];
            $scope.chosen_contacts = [];

            angular.forEach($rootScope.contacts, function (value, key) {
                if (value.selected) {
                    angular.forEach(vm.emails, function (val) {
                        if (val.entity_id === value.id) {
                            val.name = value.firstname;
                            $scope.chosen_emails.push(val);
                            $scope.chosen_contacts.push(value.firstname);

                        }

                    });

                }
            });
            

            console.log($scope.chosen_emails);
            bulkEmailService.send(ev, $scope.chosen_emails);
        }

            $scope.pinDocument = function (ev, contact_id) {
                vm.doccontact = contact_id;
                dialogService.show(ev, 'pinDocument', pinDocument, true);
            };

            function pinDocument($scope) {
                $scope.$watch('permission_id', function () {
                    $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
                });
                $scope.docname = [];
                $scope.uploadDoc = function (files) {
                    var docdata={
                        users:$scope.users_chosen,
                        doctype:vm.doc_type
                    };
                    uploadService.upload(files, vm.type_id, vm.doccontact, $scope.docname, $scope.permission_id,docdata);
                };

            }
        
    }
})();
