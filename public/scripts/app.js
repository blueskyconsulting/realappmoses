(function () {

    'use strict';
    angular
            .module('authApp', ['ui.router', 'satellizer', 'ngMaterial',
                'ngMdIcons', 'ngResource', 'ngAnimate', 'angular-loading-bar',
                'angularUtils.directives.dirPagination', 'isteven-multi-select',
                'angucomplete-alt', 'ng.group', 'truncate', 'ngDragDrop',
                'angularjs-gravatardirective', 'RecursionHelper', '720kb.datepicker', 'ui.calendar'

                        , 'ui.timepicker', 'angularMoment', 'ui.bootstrap.datetimepicker','angular-toArrayFilter','ngFileUpload','ngImgCrop'])

            .config(function ($stateProvider, $urlRouterProvider, $authProvider,
                    $httpProvider, $provide, $rootScopeProvider) {
                //$rootScopeProvider.digestTtl(15);
                // paginationTemplateProvider.setPath('../bower_components/angularUtils-pagination/dirPagination.tpl.html');
                function redirectWhenLoggedOut($q, $injector) {
                    return {
                        responseError: function (rejection) {
                            // Need to use $injector.get to bring in $state or else we get
                            // a circular dependency error
                            var $state = $injector.get('$state');
                            // Instead of checking for a status code of 400 which might be used
                            // for other reasons in Laravel, we check for the specific rejection
                            // reasons to tell us if we need to redirect to the login state
                            var rejectionReasons = ['user_not_found', 'token_expired', 'token_absent', 'token_invalid','token_not_provided'];
                            // Loop through each rejection reason and redirect to the login
                            // state if one is encountered
                            angular.forEach(rejectionReasons, function (value, key) {
                                if (rejection.data.error === value) {

                                    // If we get a rejection corresponding to one of the reasons
                                    // in our array, we know we need to authenticate the user so 
                                    // we can remove the current user from local storage
                                    localStorage.removeItem('user');
                                    // Send the user to the auth state so they can login
                                    $state.go('auth');
                                }
                            });
                            return $q.reject(rejection);
                        }
                    }
                }

                // Setup for the $httpInterceptor
                $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
                // Push the new factory onto the $http interceptor array
                $httpProvider.interceptors.push('redirectWhenLoggedOut');
                // now lets make an interceptor to append the token ( if user is
                // already authenticated) to every http request. 
                // solves issue of xhr - Token Not Provided
                /*$httpProvider.interceptors.push(['$q','$location',function($q,$location){
                 return{
                 'request':function(config){
                 config.headers=config.header ||{};
                 alert(''+localStorage.token);
                 if(localStorage.token){
                 config.header.Authorization='Bearer' + localStorage.token;
                 
                 }
                 return config;
                 },
                 'responseError':function(response){
                 if(response.status ===401 || response.status === 403){
                 $location.path('/auth');
                 }
                 return $q.reject(response);
                 }
                 }
                 }]);*/
                $authProvider.loginUrl = '/api/authenticate';
                // Before we get our states, let's get the user and determine if
                // They are admin or not.
                // Grab the user from local storage and parse it to an object
                var is_admin = 'admin'; //variable to hold the user level
                /*if($rootScope.authenticated===false){
                 is_admin='users';
                 }else{
                 var user = JSON.parse(localStorage.getItem('user'));
                 is_admin=(user.level === "1"?'admin':'users');
                 } */

                $stateProvider
                        .state('auth', {
                            url: '/auth',
                            templateUrl: '../views/authView.html',
                            controller: 'AuthController as auth'
                        })
                        .state('users', {
                            url: '/dashboard',
                            
                            templateUrl: '../views/admin/dashboard.html',
                            views: {
                                '': {templateUrl: '../views/' + is_admin + '/dashboard.html'},
                                'topmenu@users': {
                                    templateUrl: '../views/' + is_admin + '/top_menu.html',
                                    controller: 'UserController as user'

                                },
                                'sidebarMenu@users': {
                                    templateUrl: '../views/' + is_admin + '/menu.html',
                                    controller: 'SidebarController as menu'
                                },
                                'content@users': {
                                    templateUrl: '../views/' + is_admin + '/dashinfo.html'
                                }

                            }
                        }).state('users.home', {
                    url: '/home',
                    templateUrl: '../views/' + is_admin + '/dashinfo.html',
                    controller: 'contactController as contact'


                })
                        .state('users.contacts', {
                            url: '/contacts/:search',
                            templateUrl: '../views/' + is_admin + '/contactsView.html',
                            controller: 'contactController as contact'

                        })
                        .state('users.contact', {
                            
                            url: '/contact/:id' ,
                            templateUrl: '../views/' + is_admin + '/contactDetails.html',
                            controller: 'contactDetailController as cdetail'
                        })
                        .state('users.contact.emails', {
                            url:'',
                            templateUrl: '../views/' + is_admin + '/mails.html'
                            
                        })
                        .state('users.contact.email', {
                            url: '/send/:type' ,
                            templateUrl: '../views/' + is_admin + '/sendmail.html',
                            controller: 'emailController as mail'
                        })
                        .state('users.leads', {
                            url: '/prospects',
                            templateUrl: '../views/' + is_admin + '/leadsView.html',
                            controller: 'leadsController as leads'

                        })
                        
                        .state('users.organizations', {
                            url: '/organizations/:search',
                            templateUrl: '../views/' + is_admin + '/organizationsView.html',
                            controller: 'organizationsController as org'
                        })
                            .state('users.organizations.email', {
                            url: '/:id/:type' ,
                            templateUrl: '../views/' + is_admin + '/sendmail.html',
                            controller: 'emailController as mail'
                        })
                        .state('users.tasks', {
                            url: '/tasks/:search',
                            templateUrl: '../views/' + is_admin + '/tasksView.html',
                            controller: 'tasksController as tasks'

                        })
                        .state('users.opportunities', {
                            url: '/opportunities/:search',
                            templateUrl: '../views/' + is_admin + '/opportunitiesView.html',
                            controller: 'opportunitiesController as tunities'
                        })
                        .state('users.opportunity', {
                            url: '/opportunities/:id',
                            templateUrl: '../views/' + is_admin + '/opportunityView.html',
                            controller: 'oppDetailsController as tunities'
                        })
                        .state('users.projects', {
                            url: '/projects',
                            templateUrl: '../views/' + is_admin + '/projectView.html',
                            controller: 'projectController as projects'
                        })
                        .state('users.products', {
                            url: '/products/:search',
                            templateUrl: '../views/' + is_admin + '/productsView.html',
                            controller: 'productController as products'})
                        .state('users.product_inventories', {
                            url: '/product_inventories',
                            templateUrl: '../views/' + is_admin + '/productInventoriesView.html',
                            controller: 'productInventoryController as p_inventory'})
                        .state('users.notes', {
                            url: '/notes',
                            templateUrl: '../views/' + is_admin + '/notesView.html',
                            controller: 'noteController as note'})
                        .state('users.payment_plans', {
                            url: '/payment_plans',
                            templateUrl: '../views/' + is_admin + '/paymentPlans.html',
                            controller: 'plansController as plans'})
                        .state('users.transactions', {
                            url: '/transactions',
                            templateUrl: '../views/' + is_admin + '/transactions.html',
                            controller: 'transactionController as trans'
                        }).state('users.calender', {
                            url: '/calendar',
                            templateUrl: '../views/' + is_admin + '/calender.html',
                            controller: 'calenderController as cal'})

                        .state('users.emails', {
                            url: '/emails',
                            templateUrl: '../views/' + is_admin + '/emails.html',
                            controller: 'emailMessagesController as email'})

                        .state('users.documents', {
                            url: '/documents',
                            templateUrl: '../views/' + is_admin + '/documentsView.html',
                            controller: 'documentsController as doc'});

                    $urlRouterProvider.otherwise( function($injector, $location) {
                        var $state = $injector.get("$state");
                         $state.go("auth");
        });
            })
            .run(function ($rootScope, $state,dialogService,CRUDservice) {

                // $stateChangeStart is fired whenever the state changes. We can use some parameters
                // such as toState to hook into details about the state as it is changing
                $rootScope.$on('$stateChangeStart', function (event, toState) {

                    // Grab the user from local storage and parse it to an object
                    var user = JSON.parse(localStorage.getItem('user'));
                    // If there is any user data in local storage then the user is quite
                    // likely authenticated. If their token is expired, or if they are
                    // otherwise not actually authenticated, they will be redirected to
                    // the auth state because of the rejected request anyway
                    if (user) {

                        // The user's authenticated state gets flipped to
                        // true so we can now show parts of the UI that rely
                        // on the user being logged in
                        $rootScope.authenticated = true;
                        // Putting the user's data on $rootScope allows
                        // us to access it anywhere across the app. Here
                        // we are grabbing what is in local storage
                        $rootScope.currentUser = user;
                        // If the user is logged in and we hit the auth route we don't need
                        // to stay there and can send the user to the main state
                        if (toState.name === "auth") {

                            // Preventing the default behavior allows us to use $state.go
                            // to change states
                            event.preventDefault();
                            // go to the "main" state which in our case is users
                            $state.go('users.home');
                        }

                    }
                });
                
                $rootScope.close=function(){
                    dialogService.close();
                };
                
                CRUDservice.query({object:'permissions'}).$promise.then(function(data){
                    $rootScope.permissions=data.permissions;
                    $rootScope.permission_id=data.public_id;
                    console.log($rootScope.permission_id);
                });
                
            });
})();
