angular.module('authApp')
            .controller('leadDetailController',leadDetailController);
    function leadDetailController($scope,CRUDservice,$stateParams,toastService){
        $scope.phone=[];
        $scope.phone_nature_id=[];
        $scope.email=[];
        $scope.email_nature_id=[];
        CRUDservice.get({object:'leads',id:$stateParams.id}).$promise.then(function(data){
            $scope.contact=data.contact;
            $scope.contact_phones=data.contact_phones;
            $scope.contact_emails=data.contact_emails;
            $scope.contact_organization=data.contact_organization;
            $scope.phone_natures=data.phone_natures;
            $scope.email_natures=data.email_natures;
            $scope.type_id=data.type_id.id;
            $scope.tasks=data.tasks;
            $scope.dates=data.dates;
            var phonecount=$scope.contact_phones.length;
            $scope.editphoneflag=[];
            $scope.phone_selected=[];
            $scope.editphoneflag[phonecount];
            $scope.phone_selected[phonecount];
            for(var i=0;i<phonecount;i++){
                $scope.editphoneflag[i]=true;
                $scope.phone_selected[i]=false;
            }
            angular.forEach($scope.contact_phones,function(value,key){
                $scope.phone[key]=value.phone_number;
                $scope.phone_selected[key]=value.phone_nature_id;
            });
            
            var emailcount=$scope.contact_emails.length;
            $scope.editemailflag=[];
            $scope.editemailflag[emailcount];
            for(var i=0;i<emailcount;i++){
                $scope.editemailflag[i]=true;
            }
            angular.forEach($scope.contact_emails,function(value,key){
                $scope.email[key]=value.email;
            });
        });
        $scope.toggleOrgFlag=function(){
            $scope.orgflag=!$scope.orgflag;
        };
        
        $scope.editOrganization=function(orgid){
            var orgdata={
                name:$scope.contact_organization.name
            };
            CRUDservice.update({object:'organizations',id:orgid},orgdata).$promise.then(function(data){
                console.log(data);
            });
            $scope.orgflag=true;
        };
        
        $scope.editPhone=function(phoneid,phoneindex){
            var phoneData={
                phone_number:$scope.phone[phoneindex],
                phone_nature_id:$scope.phone_nature_id[phoneindex],
                entity_id:$stateParams.id,
                type_id:$scope.type_id
            };
            CRUDservice.update({object:'phone_numbers',id:phoneid},phoneData).$promise.then(function(data){
                console.log(data);
            });
            $scope.editphoneflag[phoneindex]=true;
        };
        
        $scope.editEmail=function(emailid,emailindex){
            var emailData={
                email:$scope.email[emailindex],
                email_nature_id:$scope.email_nature_id[emailindex],
                entity_id:$stateParams.id,
                type_id:$scope.type_id
            };
            CRUDservice.update({object:'emails',id:emailid},emailData).$promise.then(function(data){
                console.log(data);
            });
            $scope.editemailflag[emailindex]=true;
        };
        
        $scope.toggleAddPhoneFlag=function(){
            $scope.addphoneflag=!$scope.addphoneflag;
        };
        $scope.add=function(){
            $scope.addDay=true;
        }
        $scope.addDetail=function(){
             var date={
                    description:$scope.description,
                    date:$scope.date,
                    type_id:$scope.type_id,
                    entity_id:$stateParams.id
                    
                }
                 var success=CRUDservice.save({object:'dates'},date);
                if(success){
                    
                    toastService.show('Date for contact added');
                    $scope.addDay=false;
                    
                    
                } 
        }
        $scope.savePhone=function(){
            var newPhone={
                phone_number:$scope.newphone,
                phone_nature_id:$scope.new_phone_nature_id,
                entity_id:$stateParams.id,
                type_id:$scope.type_id
            };
            CRUDservice.save({object: 'phone_numbers'}, newPhone);
            $scope.addphoneflag=!$scope.addphoneflag;
        };
        
        $scope.toggleAddEmailFlag=function(){
            $scope.addemailflag=!$scope.addemailflag;
        };
        
        $scope.saveEmail=function(){
            var newEmail={
                email:$scope.newemail,
                email_nature_id:$scope.new_email_nature_id,
                entity_id:$stateParams.id,
                type_id:$scope.type_id
            };
            console.log(newEmail);
            CRUDservice.save({object: 'emails'}, newEmail);
            $scope.addemailflag=!$scope.addemailflag;
        };
        
        $scope.toggleEditPhoneFlag=function(counter){
            $scope.editphoneflag[counter]=!$scope.editphoneflag[counter];
        };
        $scope.toggleEditEmailFlag=function(counter){
            $scope.editemailflag[counter]=!$scope.editemailflag[counter];
        };
    };
