/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for prospect
 * @author James, Simon, Moses
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('leadsController', leadsController);
    function leadsController($scope, $mdDialog, $state, $rootScope, CRUDservice,
            filterFilter, pinService,
            toastService, dialogService, moment, $stateParams, $http, bulkEmailService, Upload, $timeout, uploadService, permService) {

        var vm = this;
        $scope.search = "";
        $scope.arraySize;
        $scope.flexSize;

        $scope.phone = [];
        $scope.email = [];

        $scope.phone_nature_id = [];
        $scope.email_nature_id = [];
        $scope.user_id = [];
        $scope.note = [];
        $scope.displaynumber = 5;
        $scope.addphone = [];
        $scope.addemail = [];
        $scope.addDay = [];
        $scope.addindustry = [];
        $rootScope.pin = true;
        $rootScope.cust = false;
        $scope.select = [];
        $scope.select = false;
        $rootScope.show_customers = false;
        $scope.p_plan = [];
        $scope.plan = [];

        $scope.search = $stateParams.search;
        if ($scope.search === undefined) {
            $scope.displaynumber = "";
        }
        /*Function to reset display number when searching a lead*/
        $scope.resetItems = function () {
            $scope.displayNumber = '';
        };


        angular.forEach($rootScope.permissions, function (value, key) {
            if (value.permission == 'Private') {
                vm.privateid = value.id;
            } else if (value.permission == 'Assigned') {
                vm.assignedid = value.id;
            }
        });


        CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
            // Leads
            $scope.leads = data.leads;
            $scope.notes = data.notes;

            $scope.editPhone = [];
            $rootScope.status = data.status;

            $rootScope.salutations = data.salutations;
            $rootScope.organizations = data.organizations;
            $rootScope.sources = data.sources;
            vm.lead_update = data.update_lead;
            vm.type = data.type;
            $scope.owners = data.owners;

            $scope.conversations = data.conversations;

            $scope.contact_type = data.contact_type;
            $scope.address = data.address;
            $scope.phones = data.phone_numbers;
            $scope.emails = data.emails;
            $scope.tasks = data.tasks;
            $scope.dates = data.dates;
            vm.ctype_id = data.contact_type.id;
            vm.contact_type = data.type.id;
            $scope.events = data.events;

            vm.assigned_id = data.assigned_id;
            // Bid Types
            $rootScope.phone_natures = data.phone_natures;

            $rootScope.email_natures = data.email_natures;

            $rootScope.users = data.users;

            for (var i = 0; i < $scope.leads.length; i++) {
                $scope.addphone[i] = false;
                $scope.addemail[i] = false;
                $scope.addDay[i] = false;
                $scope.addindustry[i] = false;
                $scope.editPhone[i] = false;
            }

            $rootScope.users_choose = data.usser;
            $scope.documents = data.documents;
            vm.doc_type = data.doc_type;
            $scope.getCount = function (lead_id, object) {
            if (object === 'events') {
                var event = filterFilter($scope.events, {entity_id: lead_id});
                return event.length;
            }
//            else if(object==='opportunities'){
//                var op=filterFilter(vm.optasks, {id: lead_id});
//                return op.length;
//                
//            }
            else if (object === 'tasks') {
                var task = filterFilter($scope.tasks, {entity_id: lead_id});
                return task.length;
            } else if (object === 'notes') {
                var notes = filterFilter(vm.notes, {entity_id: lead_id});
                return notes.length;
            } else if (object === 'dates') {
                var dates = filterFilter($scope.dates, {entity_id: lead_id});
                return dates.length;
            }
        }
        });

        /*FUnction to update scope on any CRUD*/

        function updateScope() {
            CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                $scope.leads = data.leads;
                $scope.phones = data.phone_numbers;
                $scope.emails = data.emails;
                $scope.tasks = data.tasks;
                $scope.dates = data.dates;
                $scope.address = data.address;
                $scope.owners = data.owners;

            });
        }
        /*Function to update the status of a lead
         * @params id of a lead
         * @returns success after update of lead*/
        $scope.updateStatus = function (lead_id) {
            var lead = {
                status_id: $scope.upstatus
            };
            console.log(lead_id);
            CRUDservice.update({object: 'leads', id: lead_id}, lead).$promise.then(function () {
                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
                    $scope.leads = data.leads;
                });
            });
        }
        $scope.addLead = function (ev) {

            dialogService.show(ev, 'addLead', addLeads, true);
        };
        /* =====================================================================
         * Function to process the add lead form
         * =====================================================================
         */

        function addLeads($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            //Initalise a contactData variable to be passed to CRUDservice.update
            var contactData;
            

            
            $scope.close = function () {
                dialogService.close();
            }
            $scope.addNew = function (croppedDataUrl) {

                contactData = {
                    firstname: $scope.firstname,
                    lastname: $scope.lastname,
                    contact_type_id: vm.ctype_id,
                    permission_id: $scope.permission_id
                };

                /*
                 * =============================================================
                 * $scope.selectedsal.originalObject is how we access the id 
                 * for the salected salutation object. If undefined we init
                 * salutation data to the text in the input field which will be 
                 * added to the database. if defined we take the specific id so 
                 * as to pass to the contact data.
                 * ($scope.selectedsal.originalObject.id)
                 * This is repeated for organization data with the diffence of 
                 * using $scope.selectedorg.originalObject
                 * =============================================================
                 */
                if ($scope.selectedsal.originalObject.id === undefined) {
                    var salutationData = {
                        salutation: $scope.selectedsal.originalObject
                    };
                } else {
                    var salutationData = {
                        salutation_id: $scope.selectedsal.originalObject.id
                    };
                }
                ;
                if ($scope.selectedorg === undefined) {
                    var OrgData = {};
                } else {
                    if ($scope.selectedorg.originalObject.id === undefined) {
                        var orgData = {
                            name: $scope.selectedorg.originalObject
                        };
                    } else {
                        var orgData = {
                            organization_id: $scope.selectedorg.originalObject.id
                        };
                    }
                }
                /*
                 * =============================================================
                 * Using angular extend, we add both the salutationData and 
                 * orgData initialised differently based on their existense in 
                 * the database. 
                 * =============================================================
                 */
                angular.extend(contactData, salutationData, orgData);
                CRUDservice.save({object: 'contacts'}, contactData).$promise.then(function (data) {
                    /*
                     * Saving a contact returns the previously inserted ID of the contact
                     */
                    $scope.contact_last_id = data.contact_last_id;
                    vm.contact_type = data.type_id;
                    /*
                     * Take the number of provided emails from the form
                     * loop through each email and save it
                     */
                    console.log($scope.email);
                    for (var i = 0; i < $scope.email.length; i++) {
                        var emailData = {
                            email: $scope.email[i],
                            email_nature_id: $scope.email_nature_id[i],
                            entity_id: $scope.contact_last_id,
                            type_id: vm.contact_type
                        };
                        console.log(emailData);
                        CRUDservice.save({object: 'emails'}, emailData).$promise.then(function () {

                        });
                    }
                    /*
                     * Take the number of provided phones from the form
                     * loop through each email and save it
                     */
                    console.log($scope.phone);
                    for (var i = 0; i < $scope.phone.length; i++) {
                        var phoneData = {
                            phone_number: $scope.phone[i],
                            phone_nature_id: $scope.phone_nature_id[i],
                            entity_id: $scope.contact_last_id,
                            type_id: vm.contact_type
                        };
                        console.log(phoneData);
                        CRUDservice.save({object: 'phone_numbers'}, phoneData).$promise.then(function () {

                        });
                    }
//                    angular.forEach($scope.phone,function(value,key){
//                                                var phoneData = {
//                            phone_number: value.phone,
//                            phone_nature_id: $scope.phone_nature_id[i],
//                            entity_id: $scope.contact_last_id,
//                            type_id: vm.contact_type
//                        };
//                        
//                    });
                    /*
                     * Take the number of provided users from the form
                     * loop through each email and save it
                     */
                    /*
                     * Take the number of provided users from the form
                     * loop through each email and save it
                     */
                    if ($scope.user_selected.length > 0) {
                        angular.forEach($scope.user_selected, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: $scope.contact_last_id,
                                type_id: vm.contact_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: $scope.contact_last_id,
                            type_id: vm.contact_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);
                    }
                    ;
                    if ($scope.source.originalObject.id === undefined) {
                        var sourceData = {
                            source: $scope.source.originalObject
                        }
                    } else {
                        var sourceData = {
                            lead_source_id: $scope.source.originalObject.id
                        }

                    }


                    var leads = {
                        contact_id: $scope.contact_last_id,
                        status_id: $scope.lead_status,
                        description: $scope.desc

                    };

                    angular.extend(leads, sourceData);
                    CRUDservice.save({object: 'leads'}, leads).$promise.then(function (data) {
                        var lead = data.lead;
                        if ($scope.address === undefined) {
                        } else {
                            var address = {
                                address: $scope.address,
                                city: $scope.city,
                                postal_code: $scope.postal_code,
                                country: $scope.country,
                                entity_id: $scope.contact_last_id,
                                type_id: vm.contact_type
                            };

                            console.log(address);

                            CRUDservice.save({object: 'address'}, address);
                        }

                    });

                    /*
                     * =========================================================
                     * call the permission service to save the permission
                     * First check if the permission is private or public
                     * the call the appriate function of the service
                     * =========================================================
                     */


                    if ($scope.permission_id == vm.privateid) {
                        permService.savePrivate($rootScope.currentUser.id, $scope.contact_last_id, vm.type.id);
                    } else if ($scope.permission_id == vm.assignedid) {
                        permService.saveAssigned($scope.users_chosen, $scope.contact_last_id, vm.type.id);
                    }
                    if (croppedDataUrl) {
                        uploadService.uploadContactPic(croppedDataUrl, vm.type.id, $scope.contact_last_id);
                    }
                    ;
                    //run update scope so that view is updated with changes
                    updateScope();
                    updateAddress();
                    //close the dialog
                    dialogService.close();
                    //return messsage
                    toastService.show('Prospect Added');
                });
            };
        }

        /*Function to update address*/
        function updateAddress() {
            CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
                // Opportunities

                $scope.address = data.address;



            });
        }


        $scope.getNotes = function (lead_id) {
            var notecount;
            var lead_notes = filterFilter($scope.notes, {entity_id: lead_id});
            if(lead_notes === undefined){
                notecount=0;
            }
            else{
                
                notecount = lead_notes.length;
            }
            
            var leads = $scope.leads;
            $scope.editnoteflag = [];
            $scope.editnoteflag[[leads.length][notecount]];
            for (var y = 0; y < leads.length; y++) {
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[[y][i]] = true;
                }
            }
            ;
            angular.forEach(lead_notes, function (value, key) {
                $scope.note[key] = value.note;
            });
            return filterFilter($scope.notes, {entity_id: lead_id});
        };

        $scope.pinNote = function (ev, lead_id) {
            $rootScope.notelead_id = lead_id;
            dialogService.show(ev, 'addNote', pinNote, true);
            ;
        };

        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };

                pinService.pinNote(noteData, $rootScope.notelead_id, vm.type.id);
                updateNoteScope();
            }
        }
        ;



        $scope.editNote = function (ev, note_id) {
            $rootScope.editnote_id = note_id;
            CRUDservice.get({object: 'notes', id: note_id}).$promise.then(function (data) {
                $rootScope.note = data.note.note;
            });
            dialogService.show(ev, 'addNote', editNote, true);
        };

        function editNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                CRUDservice.update({object: 'notes', id: $rootScope.editnote_id}, noteData).$promise.then(function () {
                    updateNoteScope();
                    dialogService.close();
                    toastService.show('Note edited');
                });
            };
        }
        ;

        $scope.deleteNote = function (id) {
            if (confirm('Are you sure you want to delete this note')) {
                CRUDservice.delete({object: 'notes', id: id}).$promise.then(function () {
                    updateNoteScope();
                    toastService.show('Note deleted');
                });
            }
            ;
        };
        function updateNoteScope() {
            CRUDservice.query({object: 'leads'})
                    .$promise.then(function (data) {
                        vm.notes = data.notes;
                    });
        }
        ;

        $scope.getOwners = function (org_id) {

            return filterFilter($scope.owners, {entity_id: org_id});
        };
        $scope.getDates = function (org_id) {
            return filterFilter($scope.dates, {entity_id: org_id});
        };
        $scope.getEmails = function (org_id) {
            return filterFilter($scope.emails, {entity_id: org_id});
        };
        $scope.getPhones = function (org_id) {
            return filterFilter($scope.phones, {entity_id: org_id});
        };

        $scope.getTasks = function (org_id) {
            return filterFilter($scope.tasks, {entity_id: org_id});
        };
        $scope.getAddress = function (org_id) {

            return filterFilter($scope.address, {entity_id: org_id});
        };
        $scope.getEvents = function (org_id) {

            return filterFilter($scope.events, {entity_id: org_id});
        };

        $scope.getConversations = function (lead_id) {

            return filterFilter($scope.conversations, {entity_id: lead_id});
        };

        $scope.getDocuments = function (lead_id) {
            return filterFilter($scope.documents, {entity_id: lead_id});
        }

        /*Function to count number of objects per prospect
         * @params lead id and object
         * @returns number of objects per lead*/
        


        $scope.deleteLead = function (lead_id) {
            if (confirm('Are you sure you want to delete this?')) {
                CRUDservice.delete({object: 'contacts', id: lead_id}).$promise.then(function (data) {

                    updateScope();
                    dialogService.close();
                    toastService.show('Prospect deleted');

                });
            }


            //  dialogService.show(ev,'confirmDelete',deleteConfirm,true); 
        };
        /*Function to load dialog for editing a lead. */
        $scope.editLead = function (lead_id, ev) {
            CRUDservice.get({object: 'leads', id: lead_id}).$promise.then(function (data) {
                $rootScope.contact = data.contact;
                $rootScope.contact_organization = data.contact_organization;
                $rootScope.type_id = data.type_id.id;
                $rootScope.contact_salutation = data.contact_salutation;


                $rootScope.sources = data.sources;
                $rootScope.source = data.source;

                $rootScope.office = data.address;
//                Number($rootScope.office.address);
//                Number($rootScope.office.postal_code);
                dialogService.show(ev, 'editLead', editLeads, true);

            });
        }
        /*Controller that handles edititng of a lead*/
        function editLeads($scope, toastService, dialogService) {
            var contactData;
            $scope.close = function () {
                dialogService.close();
            }
            $scope.edit = function (cid, lid) {
                contactData = {
                    firstname: $scope.contact.firstname,
                    lastname: $scope.contact.lastname,
                    contact_type_id: vm.ctype_id
                };

                /*
                 * =============================================================
                 * $scope.selectedsal.originalObject is how we access the id 
                 * for the salected salutation object. If undefined we init
                 * salutation data to the text in the input field which will be 
                 * added to the database. if defined we take the specific id so 
                 * as to pass to the contact data.
                 * ($scope.selectedsal.originalObject.id)
                 * This is repeated for organization data with the diffence of 
                 * using $scope.selectedorg.originalObject
                 * =============================================================
                 */
                if ($scope.selectedsal.originalObject.id === undefined) {
                    var salutationData = {
                        salutation: $scope.selectedsal.originalObject
                    };
                } else {
                    var salutationData = {
                        salutation_id: $scope.selectedsal.originalObject.id
                    };
                }
                ;

                if ($scope.selectedorg !== undefined) {
                    if ($scope.selectedorg.originalObject.id === undefined) {
                        var orgData = {
                            name: $scope.selectedorg.originalObject
                        };
                    } else {
                        var orgData = {
                            organization_id: $scope.selectedorg.originalObject.id
                        };
                    }
                } else {
                    var orgData = {
                        organization_id: null
                    };
                }
                /*
                 * =============================================================
                 * Using angular extend, we add both the salutationData and 
                 * orgData initialised differently based on their existense in 
                 * the database. 
                 * =============================================================
                 */
                angular.extend(contactData, salutationData, orgData);
                CRUDservice.update({object: 'contacts', id: cid}, contactData).$promise.then(function (data) {
                    /*
                     * Saving a contact returns the previously inserted ID of the contact
                     */
                    $scope.contact_last_id = data.contact_last_id;

                    if ($scope.source.originalObject.id === undefined) {
                        var sourceData = {
                            source: $scope.source.originalObject
                        }
                    } else {
                        var sourceData = {
                            lead_source_id: $scope.source.originalObject.id
                        }

                    }





                    var leads = {
                        contact_id: $scope.contact_last_id,
                        status_id: $scope.contact.status_id,
                        description: $scope.contact.description

                    };

                    angular.extend(leads, sourceData);
                    CRUDservice.update({object: 'leads', id: lid}, leads).$promise.then(function (data) {
                        var lead = data.lead;
                        var address = {
                            city: $scope.office.city,
                            address: $scope.office.address,
                            country: $scope.office.country,
                            postal_code: $scope.office.postal_code,
                            entity_id: lead.id,
                            type_id: vm.type.id
                        };

                        CRUDservice.save({object: 'address'}, address);


                    });


                    //run update scope so that view is updated with changes
                    updateScope();
                    //close the dialog
                    dialogService.close();
                    //return messsage
                    toastService.show('Lead Edited');
                });

            };
        }
        ;
        $scope.add = function (type, index) {


            if (type === 'phone') {
                $scope.addphone[index] = true;
            } else if (type === 'email') {
                $scope.addemail[index] = true;
            } else if (type === 'date') {
                $scope.addDay[index] = true;
            } else if (type === 'industry') {
                $scope.addindustry[index] = true;
            }
        }


        $scope.addDetail = function (type, org_id, index) {

            if (type === 'phone') {
                var orgphone = {
                    phone_number: $scope.myphone,
                    type_id: vm.type.id,
                    entity_id: org_id,
                    phone_nature_id: 2
                }
                console.log(orgphone);
                var success = CRUDservice.save({object: 'phone_numbers'}, orgphone);
                if (success) {
                    CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                        $scope.phones = data.phone_numbers;

                    });
                    toastService.show('Phone number for prospect added');

                    $scope.addphone[index] = false;
                }
            } else if (type === 'email') {
                var orgemail = {
                    email: $scope.myemail,
                    type_id: vm.type.id,
                    entity_id: org_id

                }
                var success = CRUDservice.save({object: 'emails'}, orgemail);

                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                    $scope.emails = data.emails;

                });
                toastService.show('Email for organization added');


                $scope.addemail[index] = false;





            } else if (type === 'date') {
                var start = moment($scope.mydate).format('YYYY-MM-DD HH:mm:ss');
                var date = {
                    description: $scope.mydescription,
                    date: start,
                    type_id: vm.type.id,
                    entity_id: org_id

                }
                var success = CRUDservice.save({object: 'dates'}, date);
                if (success) {
                    CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                        $scope.dates = data.dates;

                    });
                    toastService.show('Date for organization added');


                    $scope.addDay[index] = false;



                }
            }
        }
        /*Function that loads the edit phone form*/
        $scope.editphone = function (phone_id, index) {
            console.log('here');
            CRUDservice.get({object: 'phone_numbers', id: phone_id}).$promise.then(function (data) {
                $scope.phone = data.phone_number;
                $scope.editPhone[index] = true;
            })

        }
        /*Function that edits a phone*/
        $scope.editTel = function (phone_id, index) {
            var orgphone = {
                phone_number: $scope.phone.phone_number,
                type_id: vm.type.id,
                entity_id: phone_id,
                phone_nature_id: 2
            }
            console.log(orgphone);
            var success = CRUDservice.save({object: 'phone_numbers'}, orgphone);
            if (success) {
                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                    $scope.phones = data.phone_numbers;

                });
                toastService.show('Phone number for prospect added');

                $scope.editPhone[index] = false;
            }
        }
        $scope.deletePhone = function (tid) {
            CRUDservice.delete({object: 'phone_numbers', id: tid}).$promise.then(function (data) {
                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                    $scope.phones = data.phone_numbers;

                });
                toastService.show('Phone number deleted.');
            });
        }
        $scope.deleteEmail = function (tid) {
            CRUDservice.delete({object: 'emails', id: tid}).$promise.then(function (data) {
                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                    $scope.emails = data.emails;

                });
                toastService.show('Email deleted.');
            });
        }
        $scope.deleteDate = function (tid) {
            CRUDservice.delete({object: 'dates', id: tid}).$promise.then(function (data) {
                CRUDservice.query({object: 'leads'}).$promise.then(function (data) {

                    $scope.dates = data.dates;

                });
                toastService.show('Date deleted.');
            });
        }
        /*Function to update task*/
        function updateTask() {
            CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
                $scope.tasks = data.tasks;
            });
        }
        $scope.addTask = function (ev, lead_id) {
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;
                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;

            });

            $rootScope.pin_lead = lead_id;
            dialogService.show(ev, 'addTask', addTasks, true);
        };

        function addTasks($scope, $rootScope, dialogService) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities
                };
                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    var task_type = data.task_type;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: task_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: task_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);

                    }


                    var pin = {
                        task_id: task.id,
                        entity_id: $rootScope.pin_lead,
                        type_id: vm.type.id
                    };
                    var success = CRUDservice.save({object: 'pinnedtasks'}, pin).$promise.then(function () {
                        updateTask();
                        toastService.show('Task Added');
                        dialogService.close();
                    });


                    //vm.tasks=data.tasks;

                });


            };
        }

        $scope.deleteTask = function (tid) {
            if (confirm('Do you want to delete this task?')) {
                CRUDservice.delete({object: 'tasks', id: tid}).$promise.then(function () {
                    updateTask();
                    dialogService.close();
                    toastService.show('task deleted');
                });
            }





        };
        $scope.addEvent = function (ev, lead_id) {
            $rootScope.pin_org = lead_id;
            dialogService.show(ev, 'addEvent', add, true);
        }
        function add($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.addEvent = function () {
                var start = moment($scope.data.start).format('YYYY-MM-DD HH:mm:ss');

                var end = moment($scope.data.end).format('YYYY-MM-DD HH:mm:ss');

                var event = {
                    title: $scope.title,
                    start: start,
                    end: end,
                    allDay: false
                }

                CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {

                    toastService.show('event added');
                    var event_id = data.event.id;
                    var pin = {
                        event_id: event_id,
                        entity_id: $rootScope.pin_org,
                        type_id: vm.type.id
                    };
                    console.log(pin);
                    CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {

                        updateEvents();
                        dialogService.close();
                    });
                });
            }

        }
        function updateEvents() {
            CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
                $scope.events = data.events;

            });
        }


        // customers are our contacts
        $rootScope.customers = $rootScope.contacts;
        // active plans, plan types etc        

        // get the inventories here
        CRUDservice.query({object: 'product_inventories'}).$promise.then(function (data) {
            $rootScope.inventories = data.product_inventories;
            console.log('Inventories are : ' + $rootScope.inventories.length);
        });
        CRUDservice.query({object: 'products'}).$promise.then(function (data) {
            $rootScope.product = data.products;
        });
        // Now Populate The Opportunities
        CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
            // Opportunities
            $rootScope.opportunities = filterFilter(data.opportunities, {deleted_at: null});
            // Pipeline Stages
            $rootScope.stages = data.stages;
            // Bid Types
            $rootScope.bidTypes = data.bid_type;
            // Users
            $scope.users = data.users;
            // Type id
            $rootScope.type = data.type;
            // Pipelines
            $scope.pipelines = data.pipelines;
        });
        $scope.getPlan = function (id) {
            // console.log('Plans are:' + $rootScope.plans.length)
            return filterFilter($rootScope.plans, {product_id: id});
        };

        $scope.addTunity = function (ev, contact_id, firstname, lastname) {

            $rootScope.customer_id = contact_id;
            $scope.firstname = firstname;
            $scope.lastname = lastname;
            CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
                $rootScope.plans = data.payment_plans;
                console.log($rootScope.plans);
            });
            // Load The Resources Here
            console.log('Contact ID:' + contact_id);
            $mdDialog.show({
                scope: $scope.$new(),
                controller: addTunity,
                templateUrl: '../views/dialogs/addTunity.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        /* =====================================================================
         * 'Contoller' function to add an opportunity
         * =====================================================================
         */
        function addTunity($scope, $state, filterFilter) {
            //return stages in a given pipeline            
            $scope.addNew = function () {
                //console.log($scope.amount);
                // get opportunitites data from the dialog form
                var start = moment($scope.close_date).format('YYYY-MM-DD HH:mm:ss');

                var opportunities = {
                    name: $scope.oppname,
                    entity_id: $rootScope.customer_id,
                    type_id: vm.type.id,
                    description: $scope.description,
                    user_responsible_id: $scope.assigned_to,
                    pipeline_id: $scope.stage,
                    close_date: start
                }
                //console.log('Saving this' + opportunities);
                console.log('Customer Id is ' + $scope.customer_id);
                var currentStage = $scope.stage;
                //console.log('The stage id is ' + currentStage);
                // save opportunities
                CRUDservice.save({object: 'opportunities'}, opportunities)
                        .$promise.then(function (data) {
                            //get the opportunity id.
                            $scope.opportunity_id = data.opportunity_id;
                            $rootScope.type.id = data.op_type;
                            //log the id 
                            //console.log('Opportunnity Id is: '+$scope.opportunity_id);
                            console.log($scope.p_plan[2]);
                            // save the products to the products table                           
                            angular.forEach($scope.p_plan, function (plan, i) {
                                console.log('Got it' + plan + ' got it too' + $scope.plan[i]);
                                var productsData = {
                                    opportunity_id: data.opportunity_id,
                                    inventory_id: $scope.p_plan[i],
                                    payment_plan_id: $scope.plan[i],
                                    //quantity: $scope.quantity[i],
                                    value: vm.payment_plan_value($scope.plan[i])
                                };
                                console.log('Plans are' + $scope.plan[i]);
                                console.log('Value equals:' + vm.payment_plan_value($scope.plan[i]));
                                CRUDservice.save({object: 'opp_products'}, productsData);
                            });
                            // now let's save the current stage for the app
                            // since we are saving for the first time, previous
                            // stage id is equated to 
                            // notes < entry stage for opportunity >
                            var current_stage = {
                                opportunity_id: $scope.opportunity_id,
                                current_stage_id: currentStage,
                                notes: 'Initial entry stage for this opportunity'
                            };
                            /*
                             * Save the owners
                             */
                            if ($scope.user_selected.length > 0) {
                                angular.forEach($scope.user_selected, function (value, key) {
                                    var ownersData = {
                                        user_id: value.id,
                                        entity_id: data.opportunity_id,
                                        type_id: $rootScope.type.id
                                    };
                                    CRUDservice.save({object: 'owners'}, ownersData);
                                });
                            } else {
                                var ownersData = {
                                    user_id: $rootScope.currentUser.id,
                                    entity_id: data.opportunity_id,
                                    type_id: $rootScope.type.id
                                };
                                CRUDservice.save({object: 'owners'}, ownersData);
                            }
                            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                            var lead = {
                                contact_type_id: vm.lead_update.id
                            }
                            console.log('contact type' + vm.lead_update.id);
                            CRUDservice.update({object: 'contacts', id: $scope.customer_id}, lead);
                            updateScope();
                            //console.log(success);
                            //show success dialog here
                            toastService.show('Opportunity Created. Prospect now customer ');
                            $mdDialog.hide();
                            //redirect();
                        });
            };

        }
        vm.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };
        /*Funvtion to mark task as done*/
        $scope.isDone = function (task_id) {
            if (confirm('Mark task as done?')) {
                CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {

                    $rootScope.task_status = data.status;
                    angular.forEach($rootScope.task_status, function (value, key) {

                        if (value.status == 'Done') {
                            vm.done_id = value.id;

                        }
                    });
                    var done = {
                        status: vm.done_id
                    }
                    console.log(done);
                    CRUDservice.update({object: 'tasks', id: task_id}, done).$promise.then(function () {

                        updateTask();
                        toastService.show('Task marked as done');
                    });



                });

            }
        }

        $scope.newEmail = function (ev, org_id) {
            $rootScope.emails = [];
            $rootScope.lead_id = org_id;
            $rootScope.emails = filterFilter($scope.emails, {entity_id: org_id});
            if ($rootScope.emails.length) {
                CRUDservice.get({object: 'leads', id: org_id}).$promise.then(function (data) {
                    $rootScope.contact = data.contact;


                })
                dialogService.show(ev, 'sendmail', send, true);

            } else {
                toastService.show("please add an email for prospect first");
            }

        };
        function send($scope) {
            $scope.attach_id = [];
            $scope.sendMail = function (files) {

                $scope.files = angular.toJson(files);
                var mail = {
                    from: $rootScope.currentUser.email,
                    to: $scope.to,
                    message: $scope.message,
                    receiver: $rootScope.organ.name,
                    sender: $rootScope.currentUser.name,
                    subject: $scope.subject,
                    entity_id: $rootScope.org_id,
                    type_id: vm.type.id,
                    attach_ids: $scope.attach_id


                }
                console.log(mail);
                $http.post('api/emails/send', mail).success(function (data) {
                    var email_id = {email_id: data.email_id};
                    for (var i = 0; i < $scope.attach_id.length; i++) {
                        CRUDservice.update({object: 'email_attachments', id: $scope.attach_id[i]}, email_id);
                    }
                    toastService.show('Email sent');
                    dialogService.close();
                });

            }
            $scope.uploadFiles = function (files, errFiles) {
                $scope.files = files;
                $scope.errFiles = errFiles;

                angular.forEach(files, function (file, key) {
                    file.upload = Upload.upload({
                        url: 'api/attachments',
                        data: {file: file,
                            type_id: vm.type.id}
                    });

                    file.upload.then(function (response) {
                        console.log('attach id ' + response.data.attach_id);

                        $timeout(function () {
                            file.result = response.data;
                            $scope.attach_id[key] = response.data.attach_id;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 *
                                evt.loaded / evt.total));
                    });
                });
                console.log($scope.attach_id);
            }
        }
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }
//            console.log('customers '+$rootScope.contacts.length)
//            for(var i=0;i<$rootScope.contacts.length;i++){
//                vm.customers_selected[i]=true;
//            }
            angular.forEach($scope.leads, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            if (confirm('Are you sure you want to delete these items?')) {
                angular.forEach($scope.leads, function (value) {
                    if (value.selected) {

                        CRUDservice.delete({object: 'contacts', id: value.id}).$promise.then(function (data) {
                            updateScope();
                        });

                    }
                });
                toastService.show('Successfully deleted');

            }


        }

        $scope.sendBulk = function (ev) {

            $scope.chosen_emails = [];


            angular.forEach($scope.leads, function (value) {
                if (value.selected) {
                    angular.forEach($scope.emails, function (val) {
                        if (val.entity_id === value.id) {
                            val.name = value.firstname;
                            $scope.chosen_emails.push(val);


                        }

                    });

                }
            });
            console.log($scope.chosen_emails);
            bulkEmailService.send(ev, $scope.chosen_emails);


        }



        $scope.pinDocument = function (ev, lead_id) {
            vm.doclead = lead_id;
            dialogService.show(ev, 'pinDocument', pinDocument, true);
        };

        function pinDocument($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.docname = [];
            $scope.uploadDoc = function (files) {
                var docdata={
                        users:$scope.users_chosen,
                        doctype:vm.doc_type
                    };
                uploadService.upload(files, vm.type.id, vm.doclead, $scope.docname, $scope.permission_id,docdata);
            };
        }
    }


})();

