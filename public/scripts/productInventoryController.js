(function(){
    angular.module('authApp').controller('productInventoryController',productInventoryController);
    
    function productInventoryController($scope, $mdDialog, CRUDservice,pinService,toastService,dialogService,$rootScope
            ,bulkDelService){
        $rootScope.notedisplaynumber=4;
        var vm=this;
        $scope.empty=false;
        CRUDservice.get({object:'product_inventories'}).$promise.then(function(data){
            $scope.product_inventories=data.product_inventories;
            $rootScope.products=data.products;
            $rootScope.inventory_status=data.inventory_status;
            if($rootScope.products.length==0){
                $scope.empty=true;
            }
            vm.type_id=data.type_id;
            console.log($rootScope.currentUser.id);
        });
        
        $scope.addInventory=function(ev){
            dialogService.show(ev,'addInventory',addInventory,true);
        };
        
        function addInventory($scope,dialogService){
            $scope.close=function(){
                 dialogService.close();
             }
            $scope.createInventory=function(){
                var inventoryData={
                    property_no:$scope.propertyno,
                    product_id:$scope.product_id,
                    inventory_status_id:$scope.status_id
                };
                CRUDservice.save({object:'product_inventories'},inventoryData).$promise.then(function(){
                    updateInventoryScope();
                    dialogService.close();
                    toastService.show('Invetory Added');
                });
            };
        };
        
        
        $scope.editInventory=function(ev,inventory_id){
            $rootScope.editInventoryId=inventory_id;
            CRUDservice.get({object:'product_inventories',id:inventory_id}).$promise.then(function(data){
               $rootScope.propertyno=data.product_inventory.property_no;
               $rootScope.product_id=data.product_inventory.product_id;
               $rootScope.status_id=data.product_inventory.inventory_status_id;
               dialogService.show(ev,'addInventory',editInventory,true);
            });
        };
        
        function editInventory($scope){
            $scope.close=function(){
                 dialogService.close();
             }
            $scope.createInventory=function(){
                var inventoryData={
                    property_no:$scope.propertyno,
                    product_id:$scope.product_id,
                    inventory_status_id:$scope.status_id
                };
                CRUDservice.update({object:'product_inventories',id:$rootScope.editInventoryId},inventoryData).$promise.then(function(){
                    updateInventoryScope();
                    dialogService.close();
                    toastService.show('Invetory Edited');
                });
            };
        };
        
        $scope.pinNote=function(ev,product_id){
            vm.inventorynote=product_id;
            dialogService.show(ev,'addNote',pinNote,true);
        };
        
        function pinNote($scope){
            $scope.attachNote=function(){
                var noteData={
                    note:$scope.note
                };
                pinService.pinNote(noteData,vm.inventorynote,vm.type_id);
            };
        };
        
        
        $scope.deleteInventory=function(inventory_id){
            if (confirm('Are you sure you want to delete this?')) {
                // TODO:  Do something here if the answer is "Ok".
                CRUDservice.delete({object:'product_inventories', id: inventory_id}).$promise.then(function(){
                    updateInventoryScope();
                    toastService.show('Inventory moved to Trash');
                });
            };
        };
        
        $scope.viewMore=function(ev,inventory_id){
            $rootScope.note=[];
            vm.selected_inventory=inventory_id;
            CRUDservice.get({object:'product_inventories',id:inventory_id}).$promise.then(function(data){
                $rootScope.inventory_notes=data.inventory_notes;
                var notecount = $rootScope.inventory_notes.length;
                $rootScope.editnoteflag = [];
                $rootScope.editnoteflag[notecount];
                for (var i = 0; i < notecount; i++) {
                    $rootScope.editnoteflag[i] = true;
                }
                angular.forEach($rootScope.inventory_notes, function (value, key) {
                    $rootScope.note[key] = value.note;
                });
                dialogService.show(ev,'inventory',inventoryHandler,true);
            });
        };
        
        function inventoryHandler($scope){
            $scope.toggleNoteFlag = function (index) {
                    $scope.editnoteflag[index] = !$scope.editnoteflag[index];
            };
            $scope.editNote=function(index,id){
                var noteData={
                    note:$scope.note[index]
                };
                CRUDservice.update({object:'notes',id:id},noteData).$promise.then(function(){
                    updateNoteScope();
                    toastService.show('Note edited');
                    $scope.editnoteflag[index] = !$scope.editnoteflag[index];
                });
            };
            
            $scope.deleteNote=function(id){
            CRUDservice.delete({object:'notes',id:id}).$promise.then(function(){
                updateNoteScope();
                toastService.show('Note deleted');
            });
            function updateNoteScope(){
                CRUDservice.get({object:'product_inventories',id:vm.selected_inventory}).$promise.then(function(data){
                    $rootScope.inventory_notes=data.inventory_notes;
                    var notecount = $scope.contact_notes.length;
                    $scope.editnoteflag = [];
                    $scope.editemailflag[notecount];
                    for (var i = 0; i < notecount; i++) {
                        $scope.editnoteflag[i] = true;
                    }
                    angular.forEach($scope.contact_notes, function (value, key) {
                        $scope.note[key] = value.note;
                    });
                });
            };
        };
        };
        
        
        
        
        function updateInventoryScope(){
            CRUDservice.get({object:'product_inventories'}).$promise.then(function(data){
                $scope.product_inventories=data.product_inventories;
            });
        };
        
        $scope.resetNoteItems=function(){
            $scope.notedisplaynumber="";
        };
        
        $scope.resetItems=function(){
            $scope.displaynumber="";
        };
         $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach($scope.product_inventories, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete($scope.product_inventories,'product_inventories');
            updateInventoryScope();
            
            toastService.show('Successfully deleted');
            
        }
    };
})();

