/* 
 * Contacts Controller
 * BlueSky CRM
 * Controllers File
 * @author James, Simon, Moses
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('SidebarController', ['isLoggedIn', '$state', '$location', SidebarController]);
    // =========================================================================
    // Sidebar Controller
    // Controller to Handle the Sidebar Menu For The application
    // @return the goTo state
    // @params - toState - the next state
    //           event - the click event on the menu view
    // =========================================================================
    function SidebarController($location, $state, toastService) {
        var vm = this;
        //navigator function
        vm.navigateTo = function (to, event) {
            //this will change the state
            if (to === 'contacts') {
                $state.go('contacts');
            } else if (to === 'home') {
                $state.go('users.home');
            } else if (to === 'tasks') {
                $state.go('tasks');
            } else if (to === 'leads') {
                $state.go('leads');
            } else if (to === 'organizations') {
                $state.go('organizations');
            } else if (to === 'opportunities') {
                $state.go('opportunities');
            }

        };

    }
    angular.module('authApp')
            .controller('contactDetailController', contactDetailController);
    function contactDetailController($scope, CRUDservice, $stateParams,

                    toastService,filterFilter,dialogService,pinService,$rootScope,moment,$state,uploadService) {

        var vm=this;
        $scope.phone = [];
        $scope.phone_nature_id = [];
        $scope.email = [];
        $scope.email_nature_id = [];
        $scope.note = [];
        $scope.displaynumber = 5;
        CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
            console.log(data.success);
            $scope.contact = data.contact;
            $rootScope.users = data.users;
            $scope.contact_phones = data.contact_phones;
            $scope.contact_emails = data.contact_emails;
            $scope.contact_organization = data.contact_organization;
            $scope.phone_natures = data.phone_natures;
            $scope.email_natures = data.email_natures;
            $scope.addr=data.contact_address;
            $scope.type_id = data.type_id.id;
            vm.type = data.type_id.id;
            $scope.tasks = data.tasks;
            $scope.dates = data.dates;
            $scope.contact_notes = data.contact_notes;
            $scope.opportunities = data.opportunities;
            $scope.opp_prods = data.opp_products;
            $scope.events = data.events;
            $scope.conversations=data.conversations;
            console.log($scope.conversations);
            var phonecount = $scope.contact_phones.length;
            $scope.editphoneflag = [];
            $scope.phone_selected = [];
            $scope.editphoneflag[phonecount];
            for (var i = 0; i < phonecount; i++) {
                $scope.editphoneflag[i] = true;
                $scope.phone_selected[i] = false;
            }
            angular.forEach($scope.contact_phones, function (value, key) {
                $scope.phone[key] = value.phone_number;
                $scope.phone_nature_id[key] = value.phone_nature_id;
            });

            var emailcount = $scope.contact_emails.length;
            $scope.editemailflag = [];
            $scope.editemailflag[emailcount];
            for (var i = 0; i < emailcount; i++) {
                $scope.editemailflag[i] = true;
            }
            angular.forEach($scope.contact_emails, function (value, key) {
                $scope.email[key] = value.email;
                $scope.email_nature_id[key] = value.email_nature_id;
            });

            var notecount = $scope.contact_notes.length;
            $scope.editnoteflag = [];
            $scope.editnoteflag[notecount];
            for (var i = 0; i < notecount; i++) {
                $scope.editnoteflag[i] = true;
            }
            angular.forEach($scope.contact_notes, function (value, key) {
                $scope.note[key] = value.note;
            });
            $scope.contact_documents=data.contact_documents;
        });

        $scope.toggleOrgFlag = function () {
            $scope.orgflag = !$scope.orgflag;
        };

        $scope.editOrganization = function (orgid) {
            var orgdata = {
                name: $scope.contact_organization.name
            };
            CRUDservice.update({object: 'organizations', id: orgid}, orgdata).$promise.then(function (data) {
                console.log(data);
            });
            $scope.orgflag = true;
        };

        $scope.editPhone = function (phoneid, phoneindex) {
            var phoneData = {
                phone_number: $scope.phone[phoneindex],
                phone_nature_id: $scope.phone_nature_id[phoneindex],
                entity_id: $stateParams.id,
                type_id: $scope.type_id
            };
            CRUDservice.update({object: 'phone_numbers', id: phoneid}, phoneData).$promise.then(function (data) {
                updatePhoneScope();
                toastService.show('Phone number edited');
                $scope.editphoneflag[phoneindex] = true;
            });
        };

        $scope.editEmail = function (emailid, emailindex) {
            var emailData = {
                email: $scope.email[emailindex],
                email_nature_id: $scope.email_nature_id[emailindex],
                entity_id: $stateParams.id,
                type_id: $scope.type_id
            };
            CRUDservice.update({object: 'emails', id: emailid}, emailData).$promise.then(function (data) {
                updateEmailScope();
                toastService.show('Email edited');
                $scope.editemailflag[emailindex] = true;
            });
        };

        $scope.toggleAddPhoneFlag = function () {
            $scope.addphoneflag = !$scope.addphoneflag;
        };


        $scope.add = function () {
            $scope.addDay = true;
        }
        /*Function to update date scope*/
        function updateDate() {
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                $scope.dates = data.dates;
            });
        }
        /*Edit day*/
        $scope.editD = function (date_id) {
            CRUDservice.get({object: 'dates', id: date_id}).$promise.then(function (data) {
                $scope.date = data.date;
            });
            $scope.eDay = true;
        }
        /*Function to post edit*/
        $scope.editDay = function (date_id) {
            var start = moment($scope.date.date).format('YYYY-MM-DD HH:mm:ss');
            var date = {
                description: $scope.date.description,
                date: start,
                type_id: $scope.type_id,
                entity_id: $stateParams.id

            }
            var success = CRUDservice.update({object: 'dates', id: date_id}, date);
            if (success) {
                updateDate();
                toastService.show('Date for contact edited');
                $scope.eDay = false;
            }
        }
        /*Function to add a day*/
        $scope.addDetail = function () {
            var start = moment($scope.date).format('YYYY-MM-DD HH:mm:ss');
            var date = {
                description: $scope.description,
                date: start,
                type_id: $scope.type_id,
                entity_id: $stateParams.id

            }
            var success = CRUDservice.save({object: 'dates'}, date);
            if (success) {
                updateDate();
                toastService.show('Date for contact added');
                $scope.addDay = false;
            }
        }
        $scope.savePhone = function () {
            var newPhone = {
                phone_number: $scope.newphone,
                phone_nature_id: $scope.new_phone_nature_id,
                entity_id: $stateParams.id,
                type_id: $scope.type_id
            };
            CRUDservice.save({object: 'phone_numbers'}, newPhone).$promise.then(function () {
                updatePhoneScope();
                $scope.addphoneflag = !$scope.addphoneflag;
                toastService.show('Phone Number added');
            });
        };

        $scope.toggleAddEmailFlag = function () {
            $scope.addemailflag = !$scope.addemailflag;
        };

        $scope.saveEmail = function () {
            var newEmail = {
                email: $scope.newemail,
                email_nature_id: $scope.new_email_nature_id,
                entity_id: $stateParams.id,
                type_id: $scope.type_id
            };
            console.log(newEmail);
            CRUDservice.save({object: 'emails'}, newEmail).$promise.then(function () {
                updateEmailScope();
                $scope.addemailflag = !$scope.addemailflag;
                toastService.show('Email added');
            });
        };

        $scope.pinNote = function (ev) {
            vm.notecontact = $stateParams.id;
            dialogService.show(ev, 'addNote', pinNote, true);
        };

        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                pinService.pinNote(noteData, vm.notecontact, vm.type);
                updateNoteScope();

            };
        }
        ;

        $scope.editNote = function (index, id) {
            var noteData = {
                note: $scope.note[index]
            };
            CRUDservice.update({object: 'notes', id: id}, noteData).$promise.then(function () {
                updateNoteScope();
                toastService.show('Note edited');
                $scope.editnoteflag[index] = !$scope.editnoteflag[index];
            });
        };

        $scope.toggleNoteFlag = function (index) {
            $scope.editnoteflag[index] = !$scope.editnoteflag[index];
        };

        $scope.deleteNote = function (id) {
            if (confirm('Are you sure you want to delete this note')) {
                CRUDservice.delete({object: 'notes', id: id}).$promise.then(function () {
                    updateNoteScope();
                    toastService.show('Note deleted');
                });
            }
            ;
        };
        function updateNoteScope() {
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                $scope.contact_notes = data.contact_notes;
                var notecount = $scope.contact_notes.length;
                $scope.editnoteflag = [];
                $scope.editnoteflag[notecount];
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[i] = true;
                }
                angular.forEach($scope.contact_notes, function (value, key) {
                    $scope.note[key] = value.note;
                });
            });
        }
        ;

        $scope.toggleEditPhoneFlag = function (counter) {
            $scope.editphoneflag[counter] = !$scope.editphoneflag[counter];
        };
        $scope.toggleEditEmailFlag = function (counter) {
            $scope.editemailflag[counter] = !$scope.editemailflag[counter];
        };

        function updatePhoneScope() {
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                $scope.contact = data.contact;
                $scope.contact_phones = data.contact_phones;
                var phonecount = $scope.contact_phones.length;
                $scope.editphoneflag = [];
                $scope.editphoneflag[phonecount];
                for (var i = 0; i < phonecount; i++) {
                    $scope.editphoneflag[i] = true;
                }
                angular.forEach($scope.contact_phones, function (value, key) {
                    $scope.phone[key] = value.phone_number;
                    $scope.phone_nature_id[key] = value.phone_nature_id;
                });
            });
        }
        ;

        function updateEmailScope() {
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                $scope.contact = data.contact;
                $scope.contact_emails = data.contact_emails;
                var emailcount = $scope.contact_emails.length;
                $scope.editemailflag = [];
                $scope.editemailflag[emailcount];
                for (var i = 0; i < emailcount; i++) {
                    $scope.editemailflag[i] = true;
                }
                angular.forEach($scope.contact_emails, function (value, key) {
                    $scope.email[key] = value.email;
                    $scope.email_nature_id[key] = value.email_nature_id;
                });
            });
        }
        ;
        /*Function to reset items*/
        $scope.resetItems = function () {
            $scope.displaynumber = "";
        };
        /*Fetch tasks*/
        CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;

                $rootScope.users = data.users;

                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;





            });
        $scope.addTask = function (ev) {
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;

                $rootScope.users = data.users;

                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;





            });



            dialogService.show(ev, 'addTask', addTasks, true);
        };
        function updateTask(){
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                        $scope.tasks = data.tasks;
                    });
              
        }
        
        function addTasks($scope, $rootScope, dialogService) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }

                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities
                };
                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    var task_type = data.task_type;
                    console.log(task);
                    var pin = {
                        task_id: task.id,
                        entity_id: $stateParams.id,
                        type_id: vm.type
                    };
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: task_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: task_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);

                    }
                    var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                    CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                        $scope.tasks = data.tasks;
                    });
                    updateTask();
                    toastService.show('Task Added');
                    dialogService.close();
                });


            };

        }
        /*Function to add load an event dialog*/
        $scope.addEvent = function (ev) {

            dialogService.show(ev, 'addEvent', add, true);
        }
        /*Function to add an event*/
        function add($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.addEvent = function () {
                var start = moment($scope.data.start).format('YYYY-MM-DD HH:mm:ss');

                var end = moment($scope.data.end).format('YYYY-MM-DD HH:mm:ss');

                var event = {
                    title: $scope.title,
                    start: start,
                    end: end,
                    allDay: false
                }

                CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {

                    toastService.show('event added');
                    var event_id = data.event.id;
                    var pin = {
                        event_id: event_id,
                        entity_id: $stateParams.id,
                        type_id: vm.type
                    };
                    console.log(pin);
                    CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {
                       updateEvent();

                    });
                });
            }

        }
        function updateEvent(){
             CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                            $scope.events = data.events;
                            toastService.show('Event added');
                            dialogService.close();
                        });
        }
        $scope.isDone=function(task_id){
            if(confirm('Mark task as done?')){
               angular.forEach($rootScope.task_status,function(value,key){
                 
                   if(value.status==='Done'){
                       vm.done_id=value.id;
                
                   }
               });
                      var done={
                    status:vm.done_id
                }
                
                CRUDservice.update({object:'tasks',id:task_id},done).$promise.then(function(){
                    
                    CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                        $scope.tasks = data.tasks;
                    });
                    updateTask();
                    toastService.show('Task marked as done');
                });
            }

        };
        /*Function to load email view*/
        $scope.newEmail=function(){
            if($scope.contact_emails.length){
               $state.go('users.contact.email',{id:$stateParams.id,type:vm.type}); 
            }else{
                toastService.show('Please add an email first');
            }
            
        };

        
        
        $scope.deleteDoc=function(id){
            uploadService.delete(id);
            updateDocScope();
        };
        function updateDocScope() {
            CRUDservice.get({object: 'contacts', id: $stateParams.id}).$promise.then(function (data) {
                $scope.contact_documents=data.contact_documents;
            });
        }

    }
    ;

    /* =========================================================================
     * Payment Plans Controller
     * =========================================================================
     */
    angular.module('authApp')
            .controller('plansController', plansController);
    function plansController($scope, CRUDservice, $stateParams, filterFilter,
            toastService, $http, $rootScope, dialogService,bulkDelService) {
        var vm = this; // overall scope 
        vm.customers;
        vm.products;
        vm.plans;
        vm.deleted;
        $scope.displaynumber = 5;
        $scope.empty=false;
        //check whether the plan is deleted or not. toggles the bg-color
        // Load the plans resources including the products. This CRUDservice call
        // will return various data from the laravel end-point such as products,
        // active plans, plan types etc        
        CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
            vm.plans = data.payment_plans;
            vm.active_id = data.active_id;
            $rootScope.products = data.products;
            if($rootScope.products.length==0){
                $scope.empty=true;
            }
            $rootScope.plan_status = data.plan_status;
        });
        /* =====================================================================
         * get the type of the payment plan
         * whether cash or deposits
         * @params installment, installment_no
         * @returns whether cash of deposit
         * =====================================================================
         */
        vm.getType = function (installments, installments_no) {
            if (parseInt(installments) === 0 && parseInt(installments_no) === 0) {
                return true;
            } else {
                return false;
            }
        };
        /* =====================================================================
         * get The Status of a payment plan
         * @param the plan_status 
         * @returns the status i.e whether active, passive, deferred etc
         * PS:
         * More work needs to be done to determine different colors for different
         * Plan status. 
         * =====================================================================
         */
        vm.planStatus = function (plan_status) {
            if (plan_status === 'Not Active') {
                return false;
            } else if (plan_status === 'Active') {
                return true;
            }
        };
        /* =====================================================================
         * Edit a Plan
         * @param the plan ID
         * @return edited plan 
         * ===================================================================== 
         */
        vm.editPlan = function (plan_id, ev) {
            vm.edited_plan_id = plan_id;
            CRUDservice.get({object: 'payment_plans', id: plan_id}).$promise.then(function (data) {
                $rootScope.edited_depo = Number(data.deposit);
                $rootScope.edited_install = Number(data.installment);
                $rootScope.edited_installno = Number(data.installment_no);
                $rootScope.edited_product_id = data.product_id;
                $rootScope.edited_status_id = data.plan_status_id;
                dialogService.show(ev, 'editPlan', editPlan, true);
            });
        };

        function editPlan($scope) {
            $scope.editPlan = function () {
                var editData = {
                    deposit: $scope.edited_depo,
                    installment: $scope.edited_install,
                    installment_no: $scope.edited_installno,
                    product_id: $scope.edited_product_id,
                    plan_status_id: $scope.edited_status_id
                };
                CRUDservice.update({object: 'payment_plans',id:vm.edited_plan_id}, editData).$promise.then(function (){
                    updateScope();
                    dialogService.close();
                    toastService.show('plan status updated');
                });
            };
        }
        ;

        vm.addPlan = function (ev) {
            dialogService.show(ev, 'addPlan', addPlan, true);
        };

        function addPlan($scope) {
            $scope.createPlan = function () {
                console.log($scope.cashflag);
                console.log($scope.installflag);
                if ($scope.cashflag === true) {
                    var cashData = {
                        deposit: $scope.cash,
                        installment: 0,
                        installment_no: 0,
                        product_id: $scope.product_id,
                        plan_status_id: vm.active_id,
                        newcash: true
                    };
                    console.log(cashData);
                    CRUDservice.save({object: 'payment_plans'}, cashData).$promise.then(function () {
                        updateScope();
                        dialogService.close();
                        toastService.show('Plan added');
                    });
                } else if ($scope.installflag === true) {
                    var installData = {
                        deposit: $scope.depo,
                        installment: $scope.depo,
                        installment_no: $scope.install,
                        product_id: $scope.product_id,
                        plan_status_id: vm.active_id
                    };
                    console.log(installData);
                    CRUDservice.save({object: 'payment_plans'}, installData).$promise.then(function () {
                        updateScope();
                        dialogService.close();
                        toastService.show('Plan added');
                    });
                }
                ;
            };
        }
        ;
        /* =====================================================================
         * Delete a plan
         * @param the plan ID
         * @return edited plan 
         * ===================================================================== 
         */
        vm.deletePlan = function (plan_id) {
            if (confirm('Are you sure you want to delete this plan')) {
                CRUDservice.delete({object: 'payment_plans', id: plan_id}).$promise.then(function () {
                    updateScope();
                    toastService.show('Plan moved to Trash');
                });
            }
            ;
        };
        /* Check whether a plan has been trashed or not
         * 
         */
        vm.deletedPlan = function (plan_id) {
            return true;
        };

        $scope.resetItems = function () {
            $scope.displaynumber = "";
        };

        function updateScope() {
            CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
                vm.plans = data.payment_plans;
            });
        }
        ;
         $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach(vm.plans, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete(vm.plans,'payment_plans');
            updateScope();
            
            toastService.show('Successfully deleted');
            
        }
    }
})();