(function(){
    angular.module('authApp').controller('noteController',noteController);
    
    function noteController(CRUDservice,$scope,dialogService,$rootScope,
                            toastService,pinService,bulkDelService){
        $scope.displaynumber=4;
        $scope.note=[];
        CRUDservice.query({object:'notes'}).$promise.then(function(data){
            $scope.notes=data.notes;
            $scope.types=data.types;
            var notecount = $scope.notes.length;
            $scope.editnoteflag = [];
            $scope.editnoteflag[notecount];
            for (var i = 0; i < notecount; i++) {
                $scope.editnoteflag[i] = true;
            }
            angular.forEach($scope.notes, function (value, key) {
                $scope.note[key] = value.note;
            });
            console.log($scope.note);
        });
        
        $scope.addNote=function(ev,entity,type_id){
            $rootScope.type_id=type_id;
            if(entity===''){
                dialogService.show(ev,'addNote',addNote,true);
            }
            else if(entity==='Contact'){
                CRUDservice.query({object:'contacts'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.contacts;
                    dialogService.show(ev,'newContactNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Organization'){
                CRUDservice.query({object:'organizations'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.organizations;
                    dialogService.show(ev,'newOrgNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Lead'){
                CRUDservice.query({object:'leads'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.leads;
                    dialogService.show(ev,'newLeadNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Task'){
                CRUDservice.query({object:'tasks'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.tasks;
                    dialogService.show(ev,'newTaskNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Product'){
                CRUDservice.query({object:'products'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.products;
                    dialogService.show(ev,'newProductNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Inventory'){
                CRUDservice.query({object:'product_inventories'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.product_inventories;
                    dialogService.show(ev,'newInventoryNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Opportunity'){
                CRUDservice.query({object:'opportunities'}).$promise.then(function(data){
                    console.log(data);
                    $rootScope.entity_selected=data.opportunities;
                    dialogService.show(ev,'newOpportunityNote',addNote,true);
                    console.log($rootScope.entity_selected);
                });
            }
            else if(entity==='Project'){
                CRUDservice.query({object:'projects'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.projects;
                    dialogService.show(ev,'newProductNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Invoice'){
                CRUDservice.query({object:'invoices'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.invoices;
                    dialogService.show(ev,'newInvoiceNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Receipt'){
                CRUDservice.query({object:'receipts'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.receipts;
                    dialogService.show(ev,'newReceiptNote',addNote,true);
                    console.log($scope.entity_selected);
                });
            }
        };
        
        $scope.editNote=function(index,id){
            var noteData={
                note:$scope.note[index]
            };
            CRUDservice.update({object:'notes',id:id},noteData).$promise.then(function(){
                updateNoteScope();
                toastService.show('Note edited');
                $scope.editnoteflag[index] = !$scope.editnoteflag[index];
            });
        };
        
        $scope.toggleNoteFlag = function (index) {
            $scope.editnoteflag[index] = !$scope.editnoteflag[index];
        };
        
        $scope.deleteNote=function(id){
            if(confirm('Are you sure you want to delete this note')){
                CRUDservice.delete({object:'notes',id:id}).$promise.then(function(){
                    updateNoteScope();
                    toastService.show('Note deleted');
                });
            };
        };
        function updateNoteScope(){
            CRUDservice.get({object: 'notes'}).$promise.then(function(data){
                $scope.notes = data.notes;
                var notecount = $scope.notes.length;
                $scope.editnoteflag = [];
                $scope.editnoteflag[notecount];
                for (var i = 0; i < notecount; i++) {
                    $scope.editnoteflag[i] = true;
                }
                angular.forEach($scope.notes, function (value, key) {
                    $scope.note[key] = value.note;
                });
            });
        };
        
        function addNote($scope){
            $scope.attachNote=function(){
                var noteData={
                    note:$scope.note
                };
                pinService.pinNote(noteData,$scope.selected_id,$rootScope.type_id);
                updateNoteScope();
            };
        };
        
        $scope.resetItems=function(){
            $scope.displaynumber="";
        };
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach($scope.notes, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete($scope.notes,'notes');
            updateNoteScope();
            
            toastService.show('Successfully deleted');
            
        };
    };
})();