/*
 * BlueSky Real Estate App
 * Controller Class For Transactions
 * @authors <moses><simon><james>
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('transactionController', transactionController);
    function transactionController($q, $scope, $mdDialog, CRUDservice, toastService, dialogService,

            $rootScope, pinService,$http,moment,bulkDelService,permService) {


            $scope.emptyinvoice=false;
            $scope.emptyreceipt=false;
            var vm=this;
            $scope.displaynumber=10;
            angular.forEach($rootScope.permissions, function (value, key) {
                if (value.permission == 'Private') {
                    vm.privateid = value.id;
                } else if (value.permission == 'Assigned') {
                    vm.assignedid = value.id;
                }
            });
            CRUDservice.query({object:'transactions'}).$promise.then(function(data){
                console.log(data);
                $scope.invoices=data.invoices;
                $scope.orginvoices=data.orginvoices;
                console.log($scope.invoices);
                console.log($scope.orginvoices);
                if($scope.invoices.length==0 && $scope.orginvoices.length==0){
                    $scope.emptyreceipt=true;
                }
                $rootScope.dialog_invoices=data.dialog_invoices;
                $scope.receipts=data.receipts;
                $scope.orgreceipts=data.orgreceipts;
                console.log($scope.invoices);
                console.log($scope.orginvoices);
                vm.invoicetype_id=data.invoice_type_id;
                vm.receipttype_id=data.receipt_type_id;
                vm.cheque_payment=data.cheque_payment;
                $rootScope.won_oppor=data.won_oppor;
                if($rootScope.won_oppor.length==0){
                    $scope.emptyinvoice=true;
                    $scope.emptyreceipt=true;
                }
                $rootScope.payment_modes=data.payment_modes;
                $rootScope.payment_reasons=data.payment_reasons;
                $rootScope.invoice_opp=data.invoice_opp;
                $rootScope.users_choose = data.users;
                vm.assigned_id=data.assigned_id;
            });
            
            $scope.addInvoice=function(ev){
                dialogService.show(ev,'addInvoice',addInvoice,true);
            };
            function addInvoice($scope){
                $scope.$watch('permission_id', function () {
                    $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
                });
                $scope.saveInvoice=function(){
                    var invoiceData={
                        opportunity_id:$scope.opportunity_id,
                        invoice_no:$scope.invoice_no,
                        due_date:moment($scope.due_date).format('YYYY-MM-DD'),
                        permission_id:$scope.permission_id
                    };
                    CRUDservice.save({object:'invoices'},invoiceData).$promise.then(function(data){
                        if ($scope.permission_id == vm.privateid) {
                            permService.savePrivate($rootScope.currentUser.id,data.last_invoice, vm.invoicetype_id);
                        } else if ($scope.permission_id == vm.assignedid) {
                            permService.saveAssigned($scope.users_chosen, data.last_invoice, vm.invoicetype_id);
                        }
                        updateScope();
                        dialogService.close();
                        toastService.show('Invoice Added');
                    });
                };
            };
            
            $scope.editInvoice=function(ev,invoice_id){
                vm.invoice_id=invoice_id;
                CRUDservice.get({object:'invoices',id:vm.invoice_id}).$promise.then(function(data){
                    $rootScope.opportunity_id=data.invoice.opportunity_id;
                    $rootScope.invoice_no=data.invoice.invoice_no;
                    $rootScope.due_date=new Date(data.invoice.due_date);
                    dialogService.show(ev,'addInvoice',editInvoice,true);
                });
            };
            
            function editInvoice($scope){
                $scope.saveInvoice=function(){
                    var invoiceData={
                        opportunity_id:$scope.opportunity_id,
                        invoice_no:$scope.invoice_no,
                        due_date:moment($scope.due_date).format('YYYY-MM-DD')
                    };
                    CRUDservice.update({object:'invoices',id:vm.invoice_id},invoiceData).$promise.then(function(){
                        updateScope();
                        dialogService.close();
                        toastService.show('Invoice Edited');
                    });
                };
            };
            
            $scope.deleteInvoice = function (id, ev) {
            //console.log('Opportunity Id' + id);
                CRUDservice.get({object: 'invoices', id: id}).$promise.then(function (data) {
                    $rootScope.task = data.invoice;
                    dialogService.show(ev, 'confirmDelete', confirmInvoiceDelete, true);
                });
            };
            /*Function called in confirm delete dialog. */
            function confirmInvoiceDelete($scope, $mdDialog) {
                /*function that deletes an opportunities
                 @params id of opportunity to delete
                 @returns true to delete the item*/
                $scope.deleteConfirm = function (invoice_id) {
                    CRUDservice.delete({object: 'invoices', id: invoice_id}).$promise.then(function(){
                        updateScope();
                        dialogService.close();
                        toastService.show('Invoice deleted');
                    });

                };
                $scope.close = function () {
                    dialogService.close();
                }
            }
            
            
            $scope.addReceipt=function(ev){
                dialogService.show(ev,'addReceipt',addReceipt,true);
            };
            function addReceipt($scope){
                $scope.$watch('payment_mode_id', function() {
                    $rootScope.isEnabled = $scope.payment_mode_id == vm.cheque_payment ? true : false;
                });
                $scope.$watch('permission_id', function () {
                    $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
                });
                $scope.saveReceipt=function(){
                    var receiptData={
                        invoice_id:$scope.invoice_id,
                        receipt_no:$scope.receipt_no,
                        payment_reason_id:$scope.payment_reason_id,
                        payment_mode_id:$scope.payment_mode_id,
                        cheque_no:$scope.cheque_no,
                        receipt_date:moment($scope.receipt_date).format('YYYY-MM-DD'),
                        amount:$scope.amount,
                        next_payment_date:moment($scope.next_date).format('YYYY-MM-DD'),
                        permission_id:$scope.permission_id
                    };
                    CRUDservice.save({object:'receipts'},receiptData).$promise.then(function(data){
                        if ($scope.permission_id == vm.privateid) {
                            permService.savePrivate($rootScope.currentUser.id,data.last_receipt, vm.receipttype_id);
                        } else if ($scope.permission_id == vm.assignedid) {
                            permService.saveAssigned($scope.users_chosen, data.last_receipt, vm.receipttype_id);
                        }
                        updateScope();
                        dialogService.close();
                        toastService.show('Receipt Added');
                    });
                };
            };
            
            $scope.editReceipt=function(ev,receipt_id){
                vm.receipt_id=receipt_id;
                CRUDservice.get({object:'receipts',id:vm.receipt_id}).$promise.then(function(data){
                    $rootScope.invoice_id=data.receipt.invoice_id;
                    $rootScope.receipt_no=data.receipt.receipt_no;
                    $rootScope.cheque_no=Number(data.receipt.cheque_no);
                    $rootScope.next_date=new Date(data.receipt.next_payment_date);
                    $rootScope.amount=Number(data.receipt.amount);
                    $rootScope.payment_reason_id=data.receipt.payment_reason_id;
                    $rootScope.payment_mode_id=data.receipt.payment_mode_id;
                    $rootScope.receipt_date=new Date(data.receipt.receipt_date);
                    dialogService.show(ev,'addReceipt',editReceipt,true);
                });
            };
            
            function editReceipt($scope){
                $scope.$watch('payment_mode_id', function() {
                    $rootScope.isEnabled = $scope.payment_mode_id == vm.cheque_payment ? true : false;
                });
                $scope.saveReceipt=function(){
                    var receiptData={
                        invoice_id:$scope.invoice_id,
                        receipt_no:$scope.receipt_no,
                        payment_reason_id:$scope.payment_reason_id,
                        payment_mode_id:$scope.payment_mode_id,
                        cheque_no:$scope.cheque_no,
                        receipt_date:moment($scope.receipt_date).format('YYYY-MM-DD'),
                        amount:$scope.amount,
                        next_payment_date:moment($scope.next_date).format('YYYY-MM-DD'),
                        permission_id:$scope.permission_id
                    };
                    CRUDservice.update({object:'receipts',id:vm.receipt_id},receiptData).$promise.then(function(){
                        updateScope();
                        dialogService.close();
                        toastService.show('Receipt Edited');  
                    });
                };
            };
            
            $scope.deleteReceipt = function (id, ev) {
            //console.log('Opportunity Id' + id);
                CRUDservice.get({object: 'receipts', id: id}).$promise.then(function (data) {
                    $rootScope.task = data.receipt;
                    dialogService.show(ev, 'confirmDelete', confirmReceiptDelete, true);
                });
            };
            /*Function called in confirm delete dialog. */
            function confirmReceiptDelete($scope, $mdDialog) {
                /*function that deletes an opportunities
                 @params id of opportunity to delete
                 @returns true to delete the item*/
                $scope.deleteConfirm = function (receipt_id) {
                    CRUDservice.delete({object: 'receipts', id: receipt_id}).$promise.then(function(){
                        updateScope();
                        dialogService.close();
                        toastService.show('Receipt deleted');
                    });

                };
                $scope.close = function () {
                    dialogService.close();
                }
            }
            
            $scope.pinInvoiceNote=function(ev,invoice_id){
                vm.invoicenote=invoice_id;
                dialogService.show(ev,'addNote',pinInvoiceNote,true);
            };

            function pinInvoiceNote($scope){
                $scope.attachNote=function(){
                    var noteData={
                        note:$scope.note
                    };
                    pinService.pinNote(noteData,vm.invoicenote,vm.invoicetype_id);
                };
            };
            
            $scope.pinReceiptNote=function(ev,receipt_id){
                vm.receiptnote=receipt_id;
                dialogService.show(ev,'addNote',pinReceiptNote,true);
            };

            function pinReceiptNote($scope){
                $scope.attachNote=function(){
                    var noteData={
                        note:$scope.note
                    };
                    pinService.pinNote(noteData,vm.receiptnote,vm.receipttype_id);
                };
            };
            
            $scope.generateReceipt=function(receipt_id){
                $http.get('/api/pdfs/'+receipt_id, {responseType: 'arraybuffer',
                                                    params:{type:'receipt'}})
                    .success(function (data) {
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                 });
            };
            
            $scope.generateInvoice=function(invoice_id){
                $http.get('/api/pdfs/'+invoice_id, {responseType: 'arraybuffer',
                                                    params:{type:'invoice'}})
                    .success(function (data) {
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                 });
            };
            
            
            $scope.generateOrgReceipt=function(receipt_id){
                $http.get('/api/pdfs/'+receipt_id, {responseType: 'arraybuffer',
                                                    params:{type:'orgreceipt'}})
                    .success(function (data) {
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                 });
            };
            
            $scope.generateOrgInvoice=function(invoice_id){
                $http.get('/api/pdfs/'+invoice_id, {responseType: 'arraybuffer',
                                                    params:{type:'orginvoice'}})
                    .success(function (data) {
                        var file = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                 });
            };
            
            function updateScope(){
                CRUDservice.query({object:'transactions'}).$promise.then(function(data){
                    $scope.invoices=data.invoices;
                    $scope.receipts=data.receipts;
                    $scope.orginvoices=data.orginvoices;
                    if($scope.invoices.length>0 && $scope.orginvoices.length>0){
                        $scope.emptyreceipt=false;
                    }
                    $scope.orgreceipts=data.orgreceipts;
                });
            };
            
            $scope.resetItems = function () {
                $scope.displaynumber = "";
            };
            $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach($scope.products, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete($scope.products,'products');
            updateProductScope();
            
            toastService.show('Successfully deleted');
            
        };
    }
})();