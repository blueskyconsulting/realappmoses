(function(){
    "use strict";
    angular.module('authApp')
            .controller('documentsController', documentsController);
    
    function documentsController(CRUDservice,$scope,$rootScope,dialogService,
    toastService,permService){
        $scope.displaynumber=5;
        var vm=this;
        $scope.nameflag=[];
        $scope.editedname=[];
        CRUDservice.get({object:'documents'}).$promise.then(function(data){
            $scope.documents=data.documents;
            $scope.types=data.types;
            vm.assigned_id=data.assigned_id;
            $rootScope.users_choose = data.users;
            angular.forEach($scope.documents,function(value,key){
                $scope.nameflag[key]=true;
                $scope.editedname[key]=value.document_name;
            });
        });
        
        $scope.addDocument=function(ev,entity,type_id){
            vm.type_id=type_id;
            if(entity===''){
                dialogService.show(ev,'pinDocument',addDocument,true);
            }
            else if(entity==='Contact'){
                CRUDservice.query({object:'contacts'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.contacts;
                    dialogService.show(ev,'newContactDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Organization'){
                CRUDservice.query({object:'organizations'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.organizations;
                    dialogService.show(ev,'newOrgDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Lead'){
                CRUDservice.query({object:'leads'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.leads;
                    dialogService.show(ev,'newContactDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Task'){
                CRUDservice.query({object:'tasks'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.tasks;
                    dialogService.show(ev,'newOrgDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Product'){
                CRUDservice.query({object:'products'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.products;
                    dialogService.show(ev,'newOrgDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Inventory'){
                CRUDservice.query({object:'product_inventories'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.product_inventories;
                    dialogService.show(ev,'newInventoryDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Opportunity'){
                CRUDservice.query({object:'opportunities'}).$promise.then(function(data){
                    console.log(data);
                    $rootScope.entity_selected=data.opportunities;
                    dialogService.show(ev,'newOrgDocument',addDocument,true);
                    console.log($rootScope.entity_selected);
                });
            }
            else if(entity==='Project'){
                CRUDservice.query({object:'projects'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.projects;
                    dialogService.show(ev,'newOrgDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Invoice'){
                CRUDservice.query({object:'invoices'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.invoices;
                    dialogService.show(ev,'newInvoiceDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
            else if(entity==='Receipt'){
                CRUDservice.query({object:'receipts'}).$promise.then(function(data){
                    $rootScope.entity_selected=data.receipts;
                    dialogService.show(ev,'newReceiptDocument',addDocument,true);
                    console.log($scope.entity_selected);
                });
            }
        };
        
        function addDocument($scope,uploadService){
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            angular.forEach(function () {
                if ($scope.permission_id == vm.privateid) {
                    permService.savePrivate($rootScope.currentUser.id, vm.doccontact, vm.type_id);
                } else if ($scope.permission_id == vm.assignedid) {
                    permService.saveAssigned($scope.users_chosen, vm.doccontact, vm.type_id);
                }
            });
            $scope.docname=[];
            $scope.attachDocument=function(files){
                uploadService.upload(files,vm.type_id,$scope.selected_id,$scope.docname,$scope.permission_id);
                updateDocScope();
                toastService.show('Document Added');
            };
        };
        
        $scope.toggleName=function(index){
            $scope.nameflag[index]=!$scope.nameflag[index];
        };
        
        $scope.editName=function(index,id){
            var docname={
                document_name:$scope.editedname[index]
            };
            CRUDservice.update({object:'documents',id:id},docname).$promise.then(function(){
                $scope.nameflag[index]=!$scope.nameflag[index];
                updateDocScope();
                toastService.show('Name edited');
            });
        };
        
        $scope.deleteDoc=function(id){
          CRUDservice.delete({object:'documents',id:id}).$promise.then(function(){
              updateDocScope();
              toastService.show('Document deleted');
          });  
        };
        
        $scope.resetItems = function () {
            $scope.displaynumber = "";
        };
        
        function updateDocScope(){
            CRUDservice.get({object:'documents'}).$promise.then(function(data){
                $scope.documents=data.documents;
            });
        };
    };
})();


