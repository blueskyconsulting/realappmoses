/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for Tasks
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('projectController', projectController);
    function projectController($scope, $state, $rootScope, CRUDservice, toastService,
            dialogService, filterFilter,pinService,bulkDelService) {
        var vm = this;
        $scope.note=[];
        $scope.notedisplaynumber=4;
        /*Fetch all projects*/
        CRUDservice.query({object:'projects'}).$promise.then(function(data){
            vm.projects=data.projects;
            vm.type=data.type;
            vm.products=data.products;
            vm.notes=data.notes;
            vm.cash=data.cash;
            vm.installments=data.installments;
            console.log(vm.installments);
            $rootScope.users=data.users;
           
        });
        /*Function to get products per project*/
        vm.getProducts=function(pro_id){
            return filterFilter(vm.products,{project_id:pro_id});
        }
        vm.getCash=function(product_id){
            return filterFilter(vm.cash,{product_id:product_id});
        }
        vm.getInstallments=function(p_id){
          
            return filterFilter(vm.installments,{product_id:p_id});
        }
        /*Update scope on any change*/
        function updateScope(){
            CRUDservice.query({object:'projects'}).$promise.then(function(data){
            vm.projects=data.projects;
        });
        }
        
        vm.addNew=function(ev){
            dialogService.show(ev,'addProject',addProject,true);
        }
        function addProject($scope,dialogService){
            $scope.close=function(){
                 dialogService.close();
             }
          $scope.createProject=function(){
              var project = {
                  name:$scope.projects.name,
                  description:$scope.projects.description,
                  units_no:$scope.projects.units_no,
                  project_type:$scope.projects.project_type,
                  tenant_type:$scope.projects.tenant_type
                  
              }
              console.log(project);
              CRUDservice.save({object:'projects'},project).$promise.then(function(data){
                  updateScope();
                  toastService.show('Project added');
                  dialogService.close();
              });
          }  
        }
        
        vm.getNotes=function(project_id){
           
            var project_notes=filterFilter(vm.notes,{entity_id:project_id});
            angular.forEach(project_notes, function (value, key) {
                $scope.note[key] = value.note;
                console.log(value);
            });
            return filterFilter(vm.notes,{entity_id:project_id});
        };
        
        $scope.pinNote=function(ev,proj_id){
            vm.noteproj_id=proj_id;
            dialogService.show(ev,'addNote',pinNote,true);;
        };
        
        function pinNote($scope){
            $scope.attachNote=function(){
                var noteData={
                    note:$scope.note
                };
                
                pinService.pinNote(noteData,vm.noteproj_id,vm.type);
                updateNoteScope();
            };
        };
        
        $scope.editNote = function (ev,note_id) {
            vm.editnote_id=note_id;
            CRUDservice.get({object:'notes',id:note_id}).$promise.then(function(data){
                $rootScope.note=data.note.note;
            });
            dialogService.show(ev,'addNote',editNote,true);
        };
        
        function editNote($scope){
            $scope.attachNote=function(){
                var noteData={
                    note:$scope.note
                };
                CRUDservice.update({object:'notes',id:vm.editnote_id},noteData).$promise.then(function(){
                    updateNoteScope();
                    dialogService.close();
                    toastService.show('Note edited');
                });
            };  
        };
        
        $scope.deleteNote=function(id){
            CRUDservice.delete({object:'notes',id:id}).$promise.then(function(){
                updateNoteScope();
                toastService.show('Note deleted');
            });
        };
        function updateNoteScope(){
            CRUDservice.query({object: 'projects'})
                .$promise.then(function (data) {
                    vm.notes=data.notes;
            });
        };
        
        $scope.resetNoteItems=function(){
            $scope.notedisplaynumber="";
        };
        vm.edit=function(ev,project_id){
            CRUDservice.get({object:'projects',id:project_id}).$promise.then(function(data){
                $rootScope.project=data.project;
                $rootScope.project.units_no=Number(data.project.units_no);
            });
            dialogService.show(ev,'editProject',editProjects,true);
        }
        function editProjects($scope){
            $scope.close=function(){
                 dialogService.close();
             }
            $scope.editProject=function(project_id){
               
                var project = {
                  name:$scope.project.name,
                  description:$scope.project.description,
                  units_no:$scope.project.units_no,
                  project_type:$scope.project.project_type,
                  tenant_type:$scope.project.tenant_type
                  
              }
              
              CRUDservice.update({object:'projects',id:project_id},project).$promise.then(function(data){
                  toastService.show('Project edited');
                  dialogService.close();
                  updateScope();
              });
            }
            
        }
        vm.delete=function(project_id){
            if(confirm('Are you sure you want to delete this?')){
               CRUDservice.delete({object:'projects',id:project_id}).$promise.then(function(){
                updateScope();
                toastService.show('Project deleted');
                  dialogService.close();
            }); 
            }
            
        }
        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach(vm.projects, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete(vm.projects,'projects');
            updateScope();
            toastService.show('Successfully deleted');
            
        }
    }

})();
