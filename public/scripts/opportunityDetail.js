/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for a particular opportunity
 * @author James, Simon, Moses
 */
(function () {
    "use strict";
    angular.module('authApp') 
            .controller('oppDetailController',oppDetailController);
    function orgDetailController($scope,CRUDservice,$mdDialog,toastService,
                                        dialogService,$rootScope,$stateParams) 
    {
        var vm=this;
        CRUDservice.get({object:'opportunities',id:$stateParams.id}).$promise
                .then(function(data){
                    vm.opportunity=data.opportunity; 
        });
        
    }
}
        );
