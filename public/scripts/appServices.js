/* =============================================================================
 *  A Services File
 *  This File Contains Application wide services
 *  BlueSky CRM
 * =============================================================================
 */
(function () {
    "use strict";
    angular.module('authApp')
            /* =================================================================
             * Service To Check if a user is logged in
             * @returns a redirection to either the auth state or continue to
             * the internal state of the throwing state
             * =================================================================
             */
            .service('isLoggedIn', function ($state, $rootScope) {
                //Look at $rootScope variable
                if ($rootScope.authenticated !== true) {
                    //alert('What we do?'); 
                    return $state.go('auth');
                } else {
                    // put some data here
                }
            })
            /* =================================================================
             * Service For CRUD Based Operations
             * @name: CRUD Service
             * @params takes an object containing { object e.g. contacts ,object
             *         id }
             * @return depends on type of request e.g. success, failure, object 
             *         data
             * =================================================================
             */
            .factory('CRUDservice', function ($resource) {
                return $resource('api/:object/:id',
                        {
                            object: '@object',
                            id: '@id'
                        }, {
                    query: {isArray: false},
                    get: {isArray: false},
                    update: {method: 'PUT'},
                    create: {method: 'POST'},
                    delete: {method: 'DELETE'},
                });
            })
            /* =================================================================
             * Service For Showing Dialogs
             * Name: Dialogr
             * @function dialog
             * @params ev -the event
             *         template - the dialog template file
             *         controller - the dialog controller
             *         closeOutside - true or false to close the dialog on a
             *         click event outside the dom
             * =================================================================
             */

            /*======================================================================
             * Service for showing a simple toast.
             * @params Text to be displayed
             * =====================================================================*/
            .service('toastService', function ($mdToast) {
                this.show = function (text, $event) {
                    $mdToast.show($mdToast.simple().content(text));
                }

            }).service('dialogService', function ($mdDialog, $rootScope) {
        this.show = function (ev, template, controller, closeOutside) {
            $mdDialog.show({
                scope: $rootScope.$new(),
                controller: controller,
                templateUrl: '../views/dialogs/' + template + '.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: closeOutside
            });
        };
        this.close = function () {
            $mdDialog.hide();
        }
    })

            .service('pinService', function (CRUDservice, dialogService, toastService, $rootScope) {
                this.pinTask = function (data, specific_id, entity_id) {
                    angular.extend(data, {entity_id: specific_id});
                    angular.extend(data, {type_id: entity_id});
                    CRUDservice.save({object: 'pinned_tasks'}, data);
                };
                this.pinNote = function (data, specific_id, entity_id) {
                    angular.extend(data, {entity_id: specific_id});
                    angular.extend(data, {type_id: entity_id});
                    angular.extend(data, {user_id: $rootScope.currentUser.id});
                    CRUDservice.save({object: 'notes'}, data).$promise.then(function () {
                        dialogService.close();
                        toastService.show('Note added');
                    });
                };
            }).service('taskService', function (CRUDservice, dialogService, toastService, $rootScope, moment) {

        this.add = function (ev, entity_id, type_id) {

            var promise = CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;
                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;
                $rootScope.entity_id = entity_id;
                $rootScope.type_id = type_id;
                $rootScope.users = data.users;
                dialogService.show(ev, 'addTask', addTasks, true);
                var addTasks = function ($rootScope, dialogService, moment, CRUDservice, toastService, $scope) {

                    $scope.close = function () {
                        dialogService.close();
                    }
                    $scope.createTask = function () {
                        var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                        if ($scope.reminder === undefined) {
                            var reminder = null;
                        } else {
                            var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                        }
                        var task = {
                            name: $scope.name,
                            start_date: start,
                            status: $scope.status,
                            task_type: $scope.t_type,
                            reminder: reminder,
                            priority_type_id: $scope.priorities
                        };
                        console.log(task);
                        CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                            var task = data.task;
                            var task_type = data.task_type;
                            if ($scope.assign_to.length > 0) {
                                angular.forEach($scope.assign_to, function (value, key) {
                                    var ownersData = {
                                        user_id: value.id,
                                        entity_id: task.id,
                                        type_id: task_type
                                    };
                                    CRUDservice.save({object: 'owners'}, ownersData);
                                });
                            } else {
                                var ownersData = {
                                    user_id: $rootScope.currentUser.id,
                                    entity_id: task.id,
                                    type_id: task_type
                                };
                                CRUDservice.save({object: 'owners'}, ownersData);

                            }
                            ;
                            //                    ;
                            var pin = {
                                task_id: task.id,
                                entity_id: $rootScope.entity_id,
                                type_id: $rootScope.type_id
                            };
                            var success = CRUDservice.save({object: 'pinnedtasks'}, pin);


                            toastService.show('Task Added');
                            dialogService.close();


                        });


                    };
                }
                return true;

            });
            addTasks();


            return promise;
        }


    }).service('bulkDelService', function (CRUDservice, toastService) {

        this.delete = function (objects, entity) {
            if (confirm('Do you want to delete selected items?')) {
                angular.forEach(objects, function (value) {
                    if (value.selected) {

                        CRUDservice.delete({object: entity, id: value.id});

                    }
                });
                toastService.show('Successfully deleted');
            }
        };
    }).service('bulkEmailService', function ($http, dialogService, CRUDservice, $rootScope, toastService, Upload, $timeout, filterFilter) {
        this.send = function (ev, emails, contacts) {
            $rootScope.emails = emails;
            $rootScope.contacts = contacts;

            dialogService.show(ev, 'sendBulk', sendBulkEmail, true);
        };
        function sendBulkEmail($scope, $http) {

            $scope.sendBulk = function () {

                toastService.show('Emails being sent');
                dialogService.close();

                angular.forEach($rootScope.emails, function (value, key) {

                    var mail = {
                        from: $rootScope.currentUser.email,
                        to: value.email,
                        message: $scope.message,
                        receiver: value.name,
                        sender: $rootScope.currentUser.name,
                        subject: $scope.subject,
                        entity_id: value.entity_id,
                        type_id: value.type_id,
                        attach_ids: $scope.attach_id[key]

                    };
                    $http.post('api/emails/send', mail).success(function (data) {

                        var email_id = {email_id: data.email_id};
                        for (var i = 0; i < $scope.attach_id[key].length; i++) {
                            CRUDservice.update({object: 'email_attachments', id: $scope.attach_id[key][i]}, email_id);
                        }
                        toastService.show('Email sent');

                    });

                });


            };
            $scope.uploadFiles = function (files, errFiles) {
                $scope.attach_id = [];
                $scope.at = [];
                $scope.files = files;
                $scope.errFiles = errFiles;
                angular.forEach($rootScope.emails, function (value, keym) {
                    angular.forEach(files, function (file, key) {
                        file.upload = Upload.upload({
                            url: 'api/attachments',
                            data: {file: file,
                                type_id: 1}
                        });

                        file.upload.then(function (response) {
                            console.log('attach id ' + response.data.attach_id);

                            $timeout(function () {
                                $scope.at[key] = response.data.attach_id;
                            });
                        }, function (response) {
                            if (response.status > 0)
                                $scope.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 *
                                    evt.loaded / evt.total));
                        });
                    });
                    $scope.attach_id[keym] = $scope.at;
                });
                console.log($scope.attach_id);
            }

        }
    })



            .service('permService', function ($rootScope, CRUDservice) {
                this.savePrivate = function (user_id, entity_id, type_id) {
                    var permData = {
                        user_id: user_id,
                        entity_id: entity_id,
                        type_id: type_id
                    };
                    CRUDservice.save({object: 'private_entities'}, permData);
                };
                this.saveAssigned = function (users, entity_id, type_id) {
                    if (users.length > 0) {
                        angular.forEach(users, function (value, key) {
                            var permData = {
                                user_id: value.id,
                                entity_id: entity_id,
                                type_id: type_id
                            };
                            CRUDservice.save({object: 'assigned_entities'}, permData);
                        });
                    }
                };
            })

            .service('uploadService', function (Upload, $rootScope, $timeout, CRUDservice, permService) {
                this.upload = function (files, type_id, entity_id, docname, permission_id, docdata) {
                    var vm=this;
                    angular.forEach($rootScope.permissions, function (value, key) {
                        if (value.permission == 'Private') {
                            vm.privateid = value.id;
                        } else if (value.permission == 'Assigned') {
                            vm.assignedid = value.id;
                        }
                    });
                    angular.forEach(files, function (file, key) {
                        console.log(entity_id);
                        file.upload = Upload.upload({
                            url: '/api/documents',
                            data: {file: file, info:
                                        {
                                            type_id: type_id,
                                            entity_id: entity_id,
                                            document_name: docname[key],
                                            permission_id: permission_id
                                        }
                            }
                        });
                        file.upload.then(function (response) {
                            $timeout(function () {
                                file.result = response.data;
                                if (docdata) {
                                    if (permission_id == vm.privateid) {
                                        permService.savePrivate($rootScope.currentUser.id, response.data.lastdoc, docdata.doctype);
                                    } else if (permission_id == vm.assignedid) {
                                        permService.saveAssigned(docdata.users, response.data.lastdoc, docdata.doctype);
                                    }
                                };
                            });
                        }, function (response) {
                            if (response.status > 0)
                                $rootScope.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            // Math.min is to fix IE which reports 200% sometimes
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    });
                };

                this.uploadContactPic = function (dataUrl, type_id, entity_id) {
                    Upload.upload({
                        url: '/api/profile_pictures',
                        data: {
                            pic: Upload.dataUrltoBlob(dataUrl),
                            type_id: type_id,
                            entity_id: entity_id
                        }
                    }).then(function (response) {
                        $timeout(function () {
                            $rootScope.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $rootScope.errorMsg = response.status
                                    + ': ' + response.data;
                    }, function (evt) {
                        $rootScope.progress = parseInt(100.0 * evt.loaded / evt.total);
                    });
                };

                this.delete = function (id) {
                    CRUDservice.delete({object: 'documents', id: id});
                };
            });


})();

