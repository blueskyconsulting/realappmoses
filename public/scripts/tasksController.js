/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for Tasks
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('tasksController', tasksController);
    function tasksController($scope, $rootScope, CRUDservice, toastService,
            dialogService, pinService, filterFilter, $stateParams, moment, bulkDelService, permService,
            uploadService) {

        // Lets check that the user is logged i
        var vm = this;
        $scope.currentPage = 1;
        $scope.displayNumber = 5;
        vm.view = [];
        $scope.note = [];
        vm.done = [];
        $scope.search = $stateParams.search;
        if ($scope.search.length) {
            $scope.displayNumber = "";
        }

        angular.forEach($rootScope.permissions, function (value, key) {
            if (value.permission == 'Private') {
                vm.privateid = value.id;
            } else if (value.permission == 'Assigned') {
                vm.assignedid = value.id;
            }
        });
        /*First load task view with all tasks*/
        CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
            $rootScope.priority = data.priorities;
            vm.owners = data.owners;
            vm.orgtasks = data.orgtasks;
            vm.contact_tasks = data.contact_tasks;
            vm.lead_tasks = data.lead_tasks;
            vm.optasks = data.optasks;
            vm.tasks = data.tasks;
            $rootScope.users = data.users;
            vm.type = data.type;
            vm.notes = data.notes;

            $rootScope.task_status = data.status;
            vm.done = data.status;

            $rootScope.task_type = data.task_types;
            for (var i = 0; i < vm.tasks.length; i++) {
                vm.view[i] = false;
            }

            vm.assigned_id = data.assigned_id;
            $rootScope.users_choose = data.ussers;
            $scope.documents = data.documents;
            vm.doc_type = data.doc_type;
        });
        /*This function resets displays number when we searching items */
        $scope.resetItems = function () {
            $scope.displayNumber = '';
        };
        vm.getOwners = function (task_id) {
            return filterFilter(vm.owners, {entity_id: task_id});
        };
        vm.getCustomers = function (task_id) {
            return filterFilter(vm.contact_tasks, {id: task_id});
        };
        vm.getLeads = function (task_id) {
            return filterFilter(vm.lead_tasks, {id: task_id});
        };
        vm.getOrg = function (task_id) {
            return filterFilter(vm.orgtasks, {id: task_id});
        };
        vm.getOp = function (task_id) {
            return filterFilter(vm.optasks, {id: task_id});
        };
        /*A function that will return number of pinned items for a task*/
        vm.getCount = function (task_id, object) {
            if (object === 'organization') {
                var org = filterFilter(vm.orgtasks, {id: task_id});
                return org.length;
            } else if (object === 'opportunities') {
                var op = filterFilter(vm.optasks, {id: task_id});
                return op.length;

            } else if (object === 'prospects') {
                var lead = filterFilter(vm.lead_tasks, {id: task_id});
                return lead.length;
            } else if (object === 'contacts') {
                var contacts = filterFilter(vm.contact_tasks, {id: task_id});
                return contacts.length;
            }
        }
        /*function to filter notes for each task*/
        vm.getNotes = function (task_id) {
            var task_notes = filterFilter(vm.notes, {entity_id: task_id});
            angular.forEach(task_notes, function (value, key) {
                $scope.note[key] = value.note;
                console.log(value);
            });
            return filterFilter(vm.notes, {entity_id: task_id});
        };
        $scope.getDocuments = function (lead_id) {
            return filterFilter($scope.documents, {entity_id: lead_id});
        };
        function updateScope() {
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                vm.tasks = data.tasks;
                vm.orgtasks = data.orgtasks;
                vm.contact_tasks = data.tasks;
                vm.lead_tasks = data.lead_tasks;
                vm.optasks = data.optasks;
                vm.owners = data.owners;
            });
        }

        /*Functions that loads dialog for adding task*/
        vm.addTask = function (ev) {

            dialogService.show(ev, 'addTask', addTasks, true);
        };
        /*Dialog controller for adding function*/
        function addTasks($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            /*Function to close a dialog on click and cancel*/
            $scope.close = function () {
                dialogService.close();
            }
            /*Function that handles addition of task*/
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }

                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities,
                    permission_id: $scope.permission_id
                };

                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: vm.type.id
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: vm.type.id
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);
                    }
                    ;

                    /*
                     * =========================================================
                     * call the permission service to save the permission
                     * First check if the permission is private or public
                     * the call the appriate function of the service
                     * =========================================================
                     */


                    if ($scope.permission_id == vm.privateid) {
                        permService.savePrivate($rootScope.currentUser.id, task.id, vm.type.id);
                    } else if ($scope.permission_id == vm.assignedid) {
                        permService.saveAssigned($scope.users_chosen, task.id, vm.type.id);
                    }
                    updateScope();
                    //vm.tasks=data.tasks;
                    toastService.show('Task Added');
                    dialogService.close();
                });


            };
        }
        /*Function to delete a task. Loads a dialog that acts for confirmation*/
        vm.deleteTask = function (tid, ev) {
            //$scope.task=CRUDservice.get({object:'tasks',id:tid});
            CRUDservice.get({object: 'tasks', id: tid}).$promise.then(function (data) {

                $rootScope.task = data.task;


            });
            dialogService.show(ev, 'confirmDelete', confirmDelete, true);

        };
        /*Function called in confirm delete dialog. */
        function confirmDelete($scope, $mdDialog) {
            $scope.close = function () {
                dialogService.close();
            }
            /*function that deletes task.
             @params id of task to delete
             @returns true to delete the item*/
            $scope.deleteConfirm = function (tid) {
                var success = CRUDservice.delete({object: 'tasks', id: tid});
                if (success) {
                    updateScope();
                    dialogService.close();
                    toastService.show('task deleted');
                }
            }
            $scope.close = function () {
                dialogService.close();
            }
        }
        /*Function that calls md dialog to edit a particular task
         @params ID of task to edit*/
        vm.editTask = function (tid, ev) {

            CRUDservice.get({object: 'tasks', id: tid}).$promise.then(function (data) {

                $rootScope.task = data.task;



            });
            dialogService.show(ev, 'editTask', editDialog, true);

        };
        /*Edit dialog controller*/
        function editDialog($scope, $mdDialog) {
            $scope.close = function () {
                dialogService.close();
            }
            /*Function to edit a task. 
             @params ID of a task,
             @returns success if edit is successful */
            $scope.edit = function (tid) {
                var start = moment($scope.task.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                var task = {
                    name: $scope.task.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.task.task_type,
                    reminder: reminder,
                    priority_type_id: $scope.task.priority_type_id,
                    done: 0
                };

                CRUDservice.update({object: 'tasks', id: tid}, task).$promise.then(function (data) {


                    updateScope();
                    toastService.show('Task edited');
                    dialogService.close();
                });





            }
        }
        ;
        /*Function thats called when pinned is clicked, when clicked, makes the pin form visible,
         * loaded with data
         * @params object, task id and index
         * @returns visible pin form with objects loaded e.g organizations or contacts*/
        vm.pin = function (object, tid, index) {
            for (var i = 0; i < vm.tasks.length; i++) {

                vm.view[i] = false;

            }
            vm.view[index] = true;
            if (object === 'organization') {



                $scope.pintask_id = tid;
                vm.object = 'organization';
                vm.pinobjects = CRUDservice.query({object: 'organizations'})
                        .$promise.then(function (data) {
                            vm.organizations = data.organizations;
                            vm.orgtype = data.type.id;
                        });
            } else if (object === 'customer') {


                $scope.pintask_id = tid;
                vm.object = 'customer';
                vm.pinobjects = CRUDservice.query({object: 'contacts'})
                        .$promise.then(function (data) {
                            vm.contacts = data.contacts;
                            vm.contact_type = data.type_id.id;
                        });
            } else if (object === 'lead') {


                $scope.pintask_id = tid;
                vm.object = 'prospect';
                vm.pinobjects = CRUDservice.query({object: 'leads'})
                        .$promise.then(function (data) {
                            vm.leads = data.leads;
                            vm.contact_type = data.type_id.id;
                        });
            } else if (object === 'opportunity') {


                $scope.pintask_id = tid;
                vm.object = 'opportunity';
                vm.pinobjects = CRUDservice.query({object: 'opportunities'})
                        .$promise.then(function (data) {
                            vm.opportunities = data.opportunities;
                            vm.op_type = data.type.id;
                        });
            }
        };
        /*Function to pin a task to an entity
         * @params object to tell us what is to be pinned
         * @return pin success*/
        vm.pinned = function (object) {
            if (object === 'organization') {
                console.log(vm.org_id);
                console.log(vm.orgtype);
                var pin = {
                    task_id: $scope.pintask_id,
                    entity_id: vm.object_id,
                    type_id: vm.orgtype
                };
                var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                if (success) {
                    for (var i = 0; i < vm.tasks.length; i++) {

                        vm.view[i] = false;

                    }

                    CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                        vm.orgtasks = data.orgtasks;
                    });
                    toastService.show('Task pinned');
                }
            } else if (object === 'customer') {
                var pin = {
                    task_id: $scope.pintask_id,
                    entity_id: vm.object_id,
                    type_id: vm.contact_type
                };
                var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                if (success) {
                    for (var i = 0; i < vm.tasks.length; i++) {

                        vm.view[i] = false;

                    }
                    CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                        vm.contact_tasks = data.contact_tasks;
                    });

                    toastService.show('Task pinned');
                }
            } else if (object === 'prospect') {
                var pin = {
                    task_id: $scope.pintask_id,
                    entity_id: vm.object_id,
                    type_id: vm.contact_type
                };
                var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                if (success) {
                    for (var i = 0; i < vm.tasks.length; i++) {

                        vm.view[i] = false;

                    }

                    CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                        vm.lead_tasks = data.lead_tasks;
                    });
                    toastService.show('Task pinned');
                }
            } else if (object === 'opportunity') {
                var pin = {
                    task_id: $scope.pintask_id,
                    entity_id: vm.object_id,
                    type_id: vm.op_type
                };
                var success = CRUDservice.save({object: 'pinnedtasks'}, pin);
                if (success) {
                    for (var i = 0; i < vm.tasks.length; i++) {

                        vm.view[i] = false;

                    }
                    CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                        vm.optasks = data.optasks;
                    });
                    toastService.show('Task pinned');
                }
            }
        }
        /*Function to pin a note to a task*/
        $scope.pinNote = function (ev, task_id) {
            vm.notetask_id = task_id;
            dialogService.show(ev, 'addNote', pinNote, true);
            ;
        };
        /*Controller to add a note*/
        function pinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };

                pinService.pinNote(noteData, vm.notetask_id, vm.type.id);
                updateNoteScope();
            };
        }
        ;

        $scope.editNote = function (ev, note_id) {
            vm.editnote_id = note_id;
            CRUDservice.get({object: 'notes', id: note_id}).$promise.then(function (data) {
                $rootScope.note = data.note.note;
            });
            dialogService.show(ev, 'addNote', editNote, true);
        };

        function editNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                CRUDservice.update({object: 'notes', id: vm.editnote_id}, noteData).$promise.then(function () {
                    updateNoteScope();
                    dialogService.close();
                    toastService.show('Note edited');
                });
            };
        }
        ;

        $scope.deleteNote = function (id) {
            CRUDservice.delete({object: 'notes', id: id}).$promise.then(function () {
                updateNoteScope();
                toastService.show('Note deleted');
            });
        };
        function updateNoteScope() {
            CRUDservice.query({object: 'tasks'})
                    .$promise.then(function (data) {
                        vm.notes = data.notes;
                    });
        }
        ;
        /*Function to mark a task that is done*/
        vm.isDone = function (task_id) {
            if (confirm('Mark task as done?')) {
                angular.forEach($rootScope.task_status, function (value, key) {

                    if (value.status == 'Done') {
                        vm.done_id = value.id;

                    }
                });
                var done = {
                    status: vm.done_id
                }

                CRUDservice.update({object: 'tasks', id: task_id}, done).$promise.then(function () {

                    updateScope();
                    toastService.show('Task marked as done');
                });
            }
        }

        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach(vm.tasks, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete(vm.tasks, 'tasks');
            updateScope();

            toastService.show('Successfully deleted');

        };


        $scope.pinDocument = function (ev, task_id) {
            vm.doctask = task_id;

            dialogService.show(ev, 'pinDocument', pinDocument, true);
        };

        function pinDocument($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.docname = [];
            $scope.uploadDoc = function (files) {
                var docdata={
                        users:$scope.users_chosen,
                        doctype:vm.doc_type
                    };
                uploadService.upload(files, vm.type.id, vm.doctask, $scope.docname, $scope.permission_id,docdata);
            };
        }

        $scope.deleteDoc = function (id) {
            uploadService.delete(id);
            updateDocScope();
        };
        function updateDocScope() {
            CRUDservice.get({object: 'tasks'}).$promise.then(function (data) {
                $scope.documents = data.documents;
            });
        }
    }

})();