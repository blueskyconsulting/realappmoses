/* 
 * opportunities Controller
 * BlueSky CRM
 * Controller File for Contacts App
 */
(function () {
    "use strict";
    angular.module('authApp')
            .controller('opportunitiesController', opportunitiesController);
    function opportunitiesController($scope, $mdDialog, $state, $rootScope, CRUDservice, filterFilter,
            toastService, dialogService, pinService, moment, $stateParams, taskService, bulkDelService, permService,
            uploadService) {

        /* ======================================================================
         * Initialize Controller's Global Variables
         * ======================================================================
         */
        var vm = this;
        $scope.pin = false;
        $scope.cust = false;
        $scope.pinned = true;
        $scope.org = false;
        $scope.search = $stateParams.search;
        $scope.arraySize;
        $scope.flexSize;
        $scope.opportunities;
        $scope.displaynumber = 5;
        // let's define the entry point in the add opportunity dialog i.e. either
        // adding it from here or from other points. From other points, we won't
        // be required to show the dropdown for customers 
        $rootScope.show_customers = true;
        $scope.products = [];
        // For the payment itself
        $scope.plan = [];
        // Quantity
        $scope.quantity = [];
        $scope.prod = [];
        // For the checkbox
        $scope.p_plan = [];
        $scope.stager = [];
        $scope.editOppNameFlag = [];
        $scope.editStageFlag = [];

        if ($scope.search.length) {
            $scope.displaynumber = "";
        }
        angular.forEach($rootScope.permissions, function (value, key) {
            if (value.permission == 'Private') {
                vm.privateid = value.id;
            } else if (value.permission == 'Assigned') {
                vm.assignedid = value.id;
            }
        });
        $rootScope.plans;
        // active plans, plan types etc        
        CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
            $rootScope.plans = data.payment_plans;

            if ($rootScope.plans.length == 0) {
                $scope.empty = true;
            }
        });
        // get the inventories here
        CRUDservice.query({object: 'product_inventories'}).$promise.then(function (data) {
            $rootScope.inventories = data.product_inventories;

            if ($rootScope.inventories.length == 0) {
                $scope.empty = true;
            }
        });
        // get opportunities products
        CRUDservice.query({object: 'opp_products'}).$promise.then(function (data) {
            $rootScope.opp_products = data.opp_prods;

        });
        CRUDservice.query({object: 'products'}).$promise.then(function (data) {
            $rootScope.product = data.products;
        });
        // Now Populate The Opportunities
        CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
            // Opportunities
            $scope.opportunities = data.opportunities;
            $scope.owners = data.owners;
            $scope.events = data.events;
            console.log($scope.events);
            var orgop = data.orgop;
            angular.forEach(orgop, function (value, key) {
                var op = {
                    close_date: value.close_date,
                    current_stage_id: value.current_stage_id,
                    description: value.description,
                    entity_id: value.entity_id,
                    firstname: value.firstname,
                    id: value.id,
                    name: value.name,
                    opp_id: value.opp_id,
                    stage_name: value.stage_name,
                    type_id: value.type_id

                }
                $scope.opportunities.push(op);
            })

            // Pipeline Stages

            // Bid Types
            $rootScope.bidTypes = data.bid_type;
            // Users
            $scope.users = data.users;
            // Type id
            $rootScope.type = data.type;
            console.log('opp id' + $rootScope.type.id);
            $scope.tasks = data.tasks;
            vm.orgtype = data.orgtype.id;
            $rootScope.organizations = data.organizations;
            // Pipelines
            $scope.pipelines = data.pipelines;
            $scope.stages = data.stages;
            $scope.arraySize = $scope.stages.length;
            $scope.flexSize = 100 / $scope.arraySize;
            for (var i = 0; i < $scope.opportunities.length; i++) {
                $scope.editOppNameFlag[i] = true;
                $scope.editStageFlag[i] = true;

                //$scope.phone_selected[i] = false;
            }
            vm.assigned_id = data.assigned_id;
            $rootScope.users_choose = data.usser;
            vm.doc_type = data.doc_type;
        });
        $scope.getOwners = function (op_id) {
            return filterFilter($scope.owners, {entity_id: op_id});
        };
        $scope.getEvents = function (op_id) {
            return filterFilter($scope.events, {entity_id: op_id});
        };
        $scope.getDocuments = function (lead_id) {
            return filterFilter($scope.documents, {entity_id: lead_id});
        };
        function updateScope() {
            // Now Populate The Opportunities
            CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
                // Opportunities
                $scope.opportunities = data.opportunities;
                $scope.owners = data.owners;
                var orgop = data.orgop;
                angular.forEach(orgop, function (value, key) {
                    var op = {
                        close_date: value.close_date,
                        current_stage_id: value.current_stage_id,
                        description: value.description,
                        entity_id: value.entity_id,
                        firstname: value.firstname,
                        id: value.id,
                        name: value.name,
                        opp_id: value.opp_id,
                        stage_name: value.stage_name,
                        type_id: value.type_id
                    }
                    $scope.opportunities.push(op);
                })
                console.log($scope.opportunities);
                // Pipeline Stages

                // Bid Types
                $rootScope.bidTypes = data.bid_type;
                // Users
                $scope.users = data.users;
                // Type id
                $rootScope.type = data.type;
                vm.orgtype = data.orgtype.id;
                $rootScope.organizations = data.organizations;
                // Pipelines
                $scope.pipelines = data.pipelines;
                $scope.stages = data.stages;
                $scope.arraySize = $scope.stages.length;
                $scope.flexSize = 100 / $scope.arraySize;
                for (var i = 0; i < $scope.opportunities.length; i++) {
                    $scope.editOppNameFlag[i] = true;
                    $scope.editStageFlag[i] = true;
                    console.log($scope.editOppNameFlag[i] + '' + i);
                    //$scope.phone_selected[i] = false;
                }
            });
        }
        /*
         * ===================================================================
         * Get Contacts data from the index function (laravel)
         * It also returns various parameter such as phone nature
         * email natures, salutations and address natures 
         * ===================================================================
         */
        CRUDservice.query({object: 'contacts'}).$promise.then(function (data) {
            $rootScope.customers = data.contacts;

            vm.contact_type = data.type_id.id;
        });
        // Turn Editing Name on and off
        $scope.toggleEditNameFlag = function (counter) {
            console.log('Counter' + counter);
            $scope.editOppNameFlag[counter] = !$scope.editOppNameFlag[counter];
        };
        // Turn on editing the current stage on and off
        $scope.toggleStageFlag = function (counter) {
            //console.log('Counter' + counter);
            $scope.editStageFlag[counter] = !$scope.editStageFlag[counter];
        };
        // Edit the current stage of an opportunity
        $scope.editStage = function (opp_id, current, counter) {
            var current_stage = {
                opportunity_id: opp_id,
                current_stage_id: $scope.stage[counter],
                previous_stage_id: current,
                notes: 'Edited The Opportunity'
            };
            console.log($scope.stage[counter]);
            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
            // Set the new name 
            $scope.stager[counter] = stageName($scope.stage[counter]);
            //remove the edit thing
            $scope.editStageFlag[counter] = !$scope.editStageFlag[counter];
            toastService.show('Opportunity Stage Edited Successfully');
            //$scope.stage[counter]=
        };
        //return the name of a stage given it's id
        var stageName = function (stage_id) {
            var stage = filterFilter($rootScope.stages, {id: stage_id});
            //console.log('Stage is '+stage.length);
            for (var i = 0; i < stage.length; i++) {
                return stage[i].stage_name;
            }
            ;
        };



        /* =====================================================================
         * Get the total value of opportunities in each stage
         * 
         * =====================================================================
         */

        /* =====================================================================
         * Delete an Opportunity
         * @param1 = id, the id of the opportunty
         * @param2 = ev, click event
         * =====================================================================
         */
        vm.deleteTunity = function (id, ev) {
            //console.log('Opportunity Id' + id);
            CRUDservice.get({object: 'opportunities', id: id}).$promise.then(function (data) {
                $rootScope.task = data.opportunity;
                //console.log($rootScope.task);
            });
            dialogService.show(ev, 'confirmDelete', confirmDelete, true);
        };
        /*Function called in confirm delete dialog. */
        function confirmDelete($scope, $mdDialog) {
            /*function that deletes an opportunities
             @params id of opportunity to delete
             @returns true to delete the item*/
            $scope.deleteConfirm = function (tid) {
                var success = CRUDservice.delete({object: 'opportunities', id: tid});
                if (success) {
                    dialogService.close();
                    toastService.show('Opportunity deleted');
                    updateScope();
                }

            }
            $scope.close = function () {
                dialogService.close();
            }
        }
        /* ====================================================================
         * Function to handle showing the addTunities Dialog
         * =====================================================================
         */
        vm.addTunity = function (ev) {
            $mdDialog.show({
                scope: $scope.$new(),
                controller: addTunity,
                templateUrl: '../views/dialogs/addTunity.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        /* =====================================================================
         * 'Contoller' function to add an opportunity
         * =====================================================================
         */
        function addTunity($scope, $state, filterFilter) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.showCustomer = function () {
                $scope.pinned = false;
                $scope.pin = true;
                $scope.cust = true;

            }
            $scope.showOrg = function () {
                $scope.pinned = false;
                $scope.pin = true;
                $scope.org = true;

            }
            //return stages in a given pipeline            
            $scope.addNew = function () {
                //console.log($scope.amount);
                // get opportunitites data from the dialog form

                var start = moment($scope.close_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.cust === true) {
                    var opportunities = {
                        name: $scope.oppname,
                        entity_id: $scope.customer,
                        type_id: vm.contact_type,
                        description: $scope.description,
                        user_responsible_id: $scope.assigned_to,
                        close_date: start,
                        permission_id: $scope.permission_id
                    };
                } else if ($scope.org === true) {
                    var opportunities = {
                        name: $scope.oppname,
                        entity_id: $scope.organization,
                        type_id: vm.orgtype,
                        description: $scope.description,
                        user_responsible_id: $scope.assigned_to,
                        close_date: start,
                        permission_id: $scope.permission_id
                    };
                }

                //console.log('Saving this' + opportunities);

                var currentStage = $scope.stage;
                //console.log('The stage id is ' + currentStage);
                // save opportunities
                CRUDservice.save({object: 'opportunities'}, opportunities)
                        .$promise.then(function (data) {
                            //get the opportunity id.
                            $scope.opportunity_id = data.opportunity_id;
                            //log the id 
                            //console.log($scope.opportunity_id);
                            //console.log($scope.p_plan[2]);
                            // save the products to the products table                           
                            angular.forEach($scope.p_plan, function (plan, i) {

                                var productsData = {
                                    opportunity_id: data.opportunity_id,
                                    inventory_id: $scope.p_plan[i],
                                    payment_plan_id: $scope.plan[i],
                                    //quantity: $scope.quantity[i],
                                    value: vm.payment_plan_value($scope.plan[i])
                                };

                                CRUDservice.save({object: 'opp_products'}, productsData);
                            });

                            /*
                             * =========================================================
                             * call the permission service to save the permission
                             * First check if the permission is private or public
                             * the call the appriate function of the service
                             * =========================================================
                             */


                            if ($scope.permission_id == vm.privateid) {
                                permService.savePrivate($rootScope.currentUser.id, $scope.opportunity_id, $rootScope.type.id);
                            } else if ($scope.permission_id == vm.assignedid) {
                                permService.saveAssigned($scope.users_chosen, $scope.opportunity_id, $rootScope.type.id);
                            }
                            // now let's save the current stage for the app
                            // since we are saving for the first time, previous
                            // stage id is equated to 
                            // notes < entry stage for opportunity >
                            var current_stage = {
                                opportunity_id: $scope.opportunity_id,
                                current_stage_id: currentStage,
                                notes: 'Initial entry stage for this opportunity'

                            };
                            /*
                             * Save the owners
                             */
                            if ($scope.user_selected.length > 0) {
                                angular.forEach($scope.user_selected, function (value, key) {
                                    var ownersData = {
                                        user_id: value.id,
                                        entity_id: data.opportunity_id,
                                        type_id: $rootScope.type.id
                                    };
                                    CRUDservice.save({object: 'owners'}, ownersData);
                                });
                            } else {
                                var ownersData = {
                                    user_id: $rootScope.currentUser.id,
                                    entity_id: data.opportunity_id,
                                    type_id: $rootScope.type.id
                                };
                                CRUDservice.save({object: 'owners'}, ownersData);
                            }

                            var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                            //console.log(success);
                            //show success dialog here
                            toastService.show('Opportunity Created Successfully');
                            $state.reload('users.opportunities');
                            $mdDialog.hide();

                        });
            };
            var redirect = function () {
                $state.go('users.opportunities');
            };
        }
        /* =====================================================================
         * 
         * @param {type} product_id
         * @returns {undefined}
         * 
         * =====================================================================
         */
        $scope.getPlan = function (id) {
            // console.log('Plans are:' + $rootScope.plans.length)
            return filterFilter($rootScope.plans, {product_id: id});
        };
        /*
         * 
         */
        $scope.toInt = function (id) {
            return Number(id);
        };
        /*                        End addTunity Controller                    */

        /* =====================================================================
         * Function that calls md dialog to edit a particular opportunity
         * @params the opportunity 
         * =====================================================================
         */
        vm.editTunity = function (id, ev) {
            // Refactor code to use filters instead of making a new GET call to
            // the backend server
            CRUDservice.get({object: 'opportunities', id: id}).$promise.then(function (data) {
                //console.log('shiet');
                $rootScope.opportunity = data.opportunity;
                $rootScope.id = id,
                        $rootScope.organization = data.organization;
                $rootScope.customer = data.customer;
                $rootScope.cust = data.cust;
                $rootScope.org = data.org;
                $rootScope.pipelines = data.pipelines;
                $rootScope.stages = data.pipeline_stages;
                $rootScope.currentStage = data.current_stage;
                //console.log(data.current_stage.id);
                angular.forEach(data.current_stage, function (value, key) {
                    $rootScope.currentStage = value.current_stage_id;
                });



            });
            // get the opportunity products here
            CRUDservice.get({object: 'opp_products'}).$promise.then(function (data) {
                $rootScope.prodz = filterFilter(data.opp_prods, {opportunity_id: id});
                //console.log('Oppotunity Products are' + $rootScope.prods)
            });
            // active plans, plan types etc        
            CRUDservice.query({object: 'payment_plans'}).$promise.then(function (data) {
                $rootScope.planz = data.plans;
            });
            dialogService.show(ev, 'editTunity', editDialog, true);
        };
        /*Edit dialog controller*/
        function editDialog($scope, $mdDialog, filterFilter) {
            /*Function to edit an opportunity. 
             @params ID of an opportunity,
             @returns success if edit is successful */
            var start = moment($scope.opportunity.close_date).format('YYYY-MM-DD HH:mm:ss');
            $scope.edit = function (tid) {
                if ($rootScope.cust === true) {
                    var tunity = {
                        name: $scope.opportunity.name,
                        customer_id: $scope.customer.id,
                        description: $scope.opportunity.description,
                        close_date: start
                    };
                    console.log(tunity);
                } else if ($rootScope.org === true) {
                    var tunity = {
                        name: $scope.opportunity.name,
                        customer_id: $scope.organization.id,
                        description: $scope.opportunity.description,
                        close_date: start
                    };
                }

                console.log('Customer Id is:' + $scope.opportunity.customer);
                //console.log(tunity);
                //console.log("The Id is" + tid);
                //console.log('Current Stage' + $rootScope.currentStage);
                CRUDservice.update({object: 'opportunities', id: tid}, tunity);
                var current_stage = {
                    opportunity_id: tid,
                    current_stage_id: $scope.currentStage,
                    previous_stage_id: 0,
                    notes: 'Edited The Opportunity'
                };

                var success = CRUDservice.save({object: 'stage_changes'}, current_stage);
                toastService.show('Edited Successfully ');
                $state.reload('users.opportunities');
                $mdDialog.hide();

            };
            // Return plans for the current product_id
            // @params  the product id
            // @returns plans under a product
            $scope.getPlanz = function (id) {
                return filterFilter($rootScope.planz, {product_id: id});
            };
        }
        ;

        /* =====================================================================
         * Filter The Opportunity Products Using the opportunity ID
         * @param opp_id
         * @return products under an opportunity 
         * =====================================================================
         */

        vm.oppProducts = function (opp_id) {
            //console.log('Opp Id For Listing' + opp_id);
            return filterFilter($rootScope.opp_products, {opportunity_id: opp_id});
        };
        /* =====================================================================
         * Returns the Products Name
         * @param the product id
         * @return the product name
         * =====================================================================
         */
        vm.prodName = function (product_id) {
            var product = filterFilter($rootScope.product, {id: product_id});
            return product;
        };
        /* =====================================================================
         * get the type of the payment plan
         * whether cash or deposits
         * @params the payment plan id
         * @returns whether cash of deposit
         * =====================================================================
         */
        vm.getType = function (id) {
            var plans = [];
            var installments, installments_no;
            plans = filterFilter($rootScope.plans, {id: id});
            for (var i = 0; i < plans.length; i++) {
                //console.log('YES....');
                installments = plans[i].installment;
                installments_no = plans[i].installment_no;
            }
            if (parseInt(installments) === 0 && parseInt(installments_no) === 0) {
                return true;
            } else {
                return false;
            }
        };

        vm.pinNote = function (opp_id, ev) {
            vm.oppnoteid = opp_id;
            dialogService.show(ev, 'addNote', PinNote, true);
        };

        function PinNote($scope) {
            $scope.attachNote = function () {
                var noteData = {
                    note: $scope.note
                };
                pinService.pinNote(noteData, vm.oppnoteid, vm.type);

            };
        }
        ;
        /* =====================================================================
         * Add Products to an existing opportunity
         * @param the opportunity id, the trigger event
         * @return true or false for success or failure
         * =====================================================================
         */
        vm.addProducts = function (opp_id, ev) {
            $scope.opportunity_id = opp_id;
            $mdDialog.show({
                scope: $scope.$new(),
                controller: addProds,
                templateUrl: '../views/dialogs/addProdToTunity.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };
        /* =====================================================================
         * Controller function for adding products to an opportunity from the 
         * dialog 
         * =====================================================================
         */
        function addProds($scope, $state, filterFilter) {
            $scope.add = function () {
                console.log('Glory! It works...' + $scope.opportunity_id);
                angular.forEach($scope.p_plan, function (plan, i) {
                    console.log('Got it' + plan + ' got it too' + $scope.plan[i]);
                    var productsData = {
                        opportunity_id: $scope.opportunity_id,
                        inventory_id: $scope.p_plan[i],
                        payment_plan_id: $scope.plan[i],
                        value: vm.payment_plan_value($scope.plan[i])
                                //uantity: $scope.quantity[i]
                    };
                    console.log('Plans are' + $scope.p_plan[i]);
                    CRUDservice.save({object: 'opp_products'}, productsData);
                });
                toastService.show('Products added successfully to Opportunity');
                $mdDialog.hide();
            };

        }
        ;
        /* =====================================================================
         * Fuction to calculate the total value for a given opportunity based on
         * the products it has
         * @param the opportunity id
         * @returns total value for the opportunity
         * =====================================================================
         */
        vm.oppValue = function (opp_id) {
            var opp_products = [];
            // hold the value of the plan
            var value = 0;
            opp_products = filterFilter($rootScope.opp_products, {opportunity_id: opp_id});
            //console.log('Products in opportunity: ' + opp_products);
            for (var i = 0; i < opp_products.length; i++) {
                //now get the sums of the payment_plans_
                value += parseInt(vm.payment_plan_value(opp_products[i].payment_plan_id));
            }
            ;
            //console.log('Total Tunity Value is : ' + value);
            return value;
        };
        vm.payment_plan_value = function (plan_id) {
            //get the plans earlier saved
            var plans = [];
            var value;
            plans = filterFilter($rootScope.plans, {id: plan_id});
            for (var i = 0; i < plans.length; i++) {
                // Handle Cash Payment
                if (parseInt(plans[i].installment_no, 10) === 0) {
                    value = plans[i].deposit;
                    //console.log('Value finally got' + value);
                } //Handle Deposit payment
                else {
                    value = parseInt(plans[i].deposit, 10) +
                            (parseInt(plans[i].installment, 10) * parseInt(plans[i].installment_no, 10));
                }
                return value;
            }

        };
        /* =====================================================================
         * Edit a single Product  Plan/ Cart
         * @params the opp_product_plan entry id
         * @returns edited plan
         * =====================================================================
         */
        vm.editPlan = function (id) {
            console.log('Entry id is ' + id);
        };
        /* =====================================================================
         * @params the opp_product_plan entry id
         * @returns true or false
         * =====================================================================
         */
        vm.deletePlan = function (tid, ev) {
            if (confirm("Are you sure you want to delete this plan?")) {
                var success = CRUDservice.delete({object: 'opp_products', id: tid}).$promise
                        .then(function () {
                            dialogService.close();
                            toastService.show('Plan Deleted');
                        });
            }
            // dialogService.show(ev, 'confirmDelete', conDelete, true);
        };
        /*Function called in confirm delete dialog. */
        function conDelete($scope, $mdDialog) {
            /*function that deletes an opportunities
             @params id of opportunity to delete
             @returns true to delete the item*/
            $scope.deleteConfirm = function (id) {
                var success = CRUDservice.delete({object: 'opp_products', id: id});
                if (success) {

                }
            }
            $scope.close = function () {
                dialogService.close();
            };
        }
        /*Reset display number when search touched*/
        $scope.resetItems = function () {
            $scope.displaynumber = "";
        }
        function updateTask() {
            CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
                $scope.tasks = data.tasks;
            });
        }
        $scope.addTask = function (ev, op_id) {

            $rootScope.pin_org = op_id;
            CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
                $rootScope.priority = data.priorities;
                $rootScope.task_status = data.status;
                $rootScope.task_type = data.task_types;
                $rootScope.users = data.users;

            });
            dialogService.show(ev, 'addTask', addTasks, true);
        };

        function addTasks($scope, $rootScope, dialogService) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.createTask = function () {
                var start = moment($scope.start_date).format('YYYY-MM-DD HH:mm:ss');
                if ($scope.reminder === undefined) {
                    var reminder = null;
                } else {
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                var task = {
                    name: $scope.name,
                    start_date: start,
                    status: $scope.status,
                    task_type: $scope.t_type,
                    reminder: reminder,
                    priority_type_id: $scope.priorities
                };

                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    var task_type = data.task_type;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: task_type
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    } else {
                        var ownersData = {
                            user_id: $rootScope.currentUser.id,
                            entity_id: task.id,
                            type_id: task_type
                        };
                        CRUDservice.save({object: 'owners'}, ownersData);

                    }
                    ;
                    var pin = {
                        task_id: task.id,
                        entity_id: $rootScope.pin_org,
                        type_id: $rootScope.type.id
                    };
                    var success = CRUDservice.save({object: 'pinnedtasks'}, pin);

                    updateTask();
                    toastService.show('Task Added');
                    dialogService.close();
                });


            };
        }

        $scope.isDone = function (task_id) {
            if (confirm('Mark task as done?')) {
                CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {

                    $rootScope.task_status = data.status;
                    angular.forEach($rootScope.task_status, function (value, key) {

                        if (value.status == 'Done') {
                            vm.done_id = value.id;

                        }
                    });
                    var done = {
                        status: vm.done_id
                    }
                    console.log(done);
                    CRUDservice.update({object: 'tasks', id: task_id}, done).$promise.then(function () {

                        CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
                            $scope.tasks = data.tasks;
                        });
                        toastService.show('Task marked as done');
                    });


                });

            }
        }
        $scope.getTasks = function (op_id) {
            return filterFilter($scope.tasks, {entity_id: op_id});
        };
        $scope.deleteTask = function (tid) {
            //$scope.task=CRUDservice.get({object:'tasks',id:tid});
            if (confirm('Do you want to delete this task?')) {
                CRUDservice.delete({object: 'tasks', id: tid}).$promise.then(function () {
                    CRUDservice.query({object: 'opportunities'}).$promise.then(function (data) {
                        $scope.tasks = data.tasks;
                    });
                    dialogService.close();
                    toastService.show('task deleted');
                });

            }


        };
        $scope.addEvent = function (ev, op_id) {
            $rootScope.pin_org = op_id;
            dialogService.show(ev, 'addEvent', add, true);
        }
        function add($scope) {
            $scope.close = function () {
                dialogService.close();
            }
            $scope.addEvent = function () {
                var start = moment($scope.data.start).format('YYYY-MM-DD HH:mm:ss');

                var end = moment($scope.data.end).format('YYYY-MM-DD HH:mm:ss');

                var event = {
                    title: $scope.title,
                    start: start,
                    end: end,
                    allDay: false
                }

                CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {

                    toastService.show('event added');
                    var event_id = data.event.id;
                    var pin = {
                        event_id: event_id,
                        entity_id: $rootScope.pin_org,
                        type_id: $rootScope.type.id
                    };
                    console.log(pin);
                    CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {

                        updateEvents();
                        dialogService.close();
                    });
                });
            }

        }
        function updateEvents() {
            CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
                $scope.events = data.events;

            });
        }

        $scope.checkAll = function () {

            if ($scope.selectAll) {
                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;
            }

            angular.forEach($scope.opportunities, function (value) {
                value.selected = $scope.selectAll;
            });

        };
        $scope.deleteSome = function () {
            bulkDelService.delete($scope.opportunities, 'opportunities');
            updateScope();

            toastService.show('Successfully deleted');
        }


        $scope.pinDocument = function (ev, opp_id) {
            vm.docopp = opp_id;
            console.log(vm.docopp);
            dialogService.show(ev, 'pinDocument', pinDocument, true);
        };

        function pinDocument($scope) {
            $scope.$watch('permission_id', function () {
                $rootScope.assigned = $scope.permission_id == vm.assigned_id ? true : false;
            });
            $scope.docname = [];
            $scope.uploadDoc = function (files) {
                var docdata={
                        users:$scope.users_chosen,
                        doctype:vm.doc_type
                    };
                uploadService.upload(files, $rootScope.type.id, vm.docopp, $scope.docname, $scope.permission_id,docdata);
            };

        }
    }
}
)();