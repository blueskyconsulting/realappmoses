/* 
 * Contacts Controller
 * BlueSky CRM
 * Controller File for a particular organization
 * @author James, Simon, Moses
 */
(function () {
    "use strict";
    angular.module('authApp')
 
            .controller('orgDetailController',orgDetailController);
    function orgDetailController($scope,CRUDservice,$mdDialog,
    toastService,dialogService,$rootScope,$stateParams) {
        var vm=this;
        CRUDservice.get({object:'organizations',id:$stateParams.id}).$promise
                .then(function(data){
                    vm.organization=data.organization;
                    vm.industry=data.industry;
                    vm.type=data.type;
                    vm.phones=data.phone_numbers;
                    vm.emails=data.emails;
                    vm.tasks=data.tasks;
                    vm.dates=data.dates;
                   
                });
        function updateScope(){
            CRUDservice.get({object:'organizations',id:$stateParams.id}).$promise
                .then(function(data){
                    
                    vm.industry=data.industry;
                    vm.type=data.type;
                    vm.phones=data.phone_numbers;
                    vm.emails=data.emails;
                    vm.tasks=data.tasks;
                    vm.dates=data.dates;
                   
                });
        }        
        vm.add=function(type){
            if(type==='phone'){
                $scope.addphone=true;
            }
            else if(type==='email'){
                $scope.addemail=true;
            }
             else if(type==='date'){
                $scope.addDay=true;
            }
        }
        vm.addDetail=function(type){
            if(type==='phone'){
                var orgphone={
                    phone_number:vm.phone,
                    type_id:vm.type.id,
                    entity_id:vm.organization.id,
                    phone_nature_id:2
                }
                var success=CRUDservice.save({object:'phone_numbers'},orgphone);
                if(success){
                    updateScope();
                    toastService.show('Phone number for organization added');
                    $scope.addphone=false;
                    
                    
                }
            }
            else if(type==='email'){
                 var orgemail={
                    email:vm.email,
                    type_id:vm.type.id,
                    entity_id:vm.organization.id
                    
                }
                 var success=CRUDservice.save({object:'emails'},orgemail);
                if(success){
                    updateScope();
                    toastService.show('Email for organization added');
                    $scope.addemail=false;
                    
                    
                } 
            }else if(type==='date'){
                
                 var date={
                    description:vm.description,
                    date:vm.date,
                    type_id:vm.type.id,
                    entity_id:vm.organization.id
                    
                }
                 var success=CRUDservice.save({object:'dates'},date);
                if(success){
                    updateScope();
                    toastService.show('Date for organization added');
                    $scope.addDay=false;
                    
                    
                } 
            }
        }
        vm.deletePhone=function(tid){
            CRUDservice.delete({object:'phone_numbers',id:tid}).$promise.then(function(data){
               updateScope();
               toastService.show('Phone number deleted.');
            });   
        }
        vm.deleteEmail=function(tid){
            CRUDservice.delete({object:'emails',id:tid}).$promise.then(function(data){
               updateScope();
               toastService.show('Email deleted.');
            });   
        }
        vm.deleteDate=function(tid){
            CRUDservice.delete({object:'dates',id:tid}).$promise.then(function(data){
               updateScope();
               toastService.show('Date deleted.');
            });   
        }
        
    }

})();
