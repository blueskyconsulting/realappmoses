(function(){
    "use strict";
    angular.module('authApp')
            .controller('emailMessagesController',emailMessagesController );
    function emailMessagesController($scope, CRUDservice, $mdDialog, toastService,
            dialogService, $rootScope, filterFilter, pinService, moment, taskService,$stateParams,$state,$http,Upload) {
        $scope.displaynumber=5;       
        /*first populate all the emails*/
                CRUDservice.get({object:'email_messages'}).$promise.then(function(data){
                   
                    $scope.messages=data.orgemails;
                    $scope.conmessages=data.contact_emails;
                    $scope.leadmessages=data.lead_emails;
                    angular.forEach($scope.conmessages,function(value,key){
                        var mesg={
                            id:value.id,
                            name:value.name,
                            subject:value.subject,
                            body_text:value.subject,
                            from:value.from,
                            created_at:value.created_at,
                            status:value.status,
                            to:value.to,
                            url:value.url
                        }
                        $scope.messages.push(mesg);
                    });
                    angular.forEach($scope.leadmessages,function(value,key){
                        var message={
                            id:value.id,
                            name:value.name,
                            subject:value.subject,
                            body_text:value.subject,
                            from:value.from,
                            created_at:value.created_at,
                            status:value.status,
                            to:value.to,
                            url:value.url
                        }
                        $scope.messages.push(message);
                    });
                    
                    
                });
            }
})();
