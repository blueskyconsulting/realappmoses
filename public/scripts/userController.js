(function() {

	'use strict';

	angular
		.module('authApp')
		.controller('UserController', UserController);

	function UserController($http, $auth, $rootScope,CRUDservice) {

		var vm = this;

		vm.users;
		vm.error;
                $http.get('api/notifications').success(function(data){
                    vm.events=data.events;
                    vm.orgdates=data.orgdates;
                    vm.ldates=data.lead_dates;
                    vm.cdates=data.contact_dates;
                    
                    vm.tasks=data.tasks;
                    
                });
                //vm.notifications=vm.orgdates.length+vm.ldates.length+vm.cdates.length;
		vm.getUsers = function() {

			//Grab the list of users from the API
			$http.get('api/authenticate').success(function(users) {
				vm.users = users;
			}).error(function(error) {
				vm.error = error;
			});
		}

		// We would normally put the logout method in the same
		// spot as the login method, ideally extracted out into
		// a service. For this simpler example we'll leave it here
		vm.logout = function() {

			$auth.logout().then(function() {

				// Remove the authenticated user from local storage
				localStorage.removeItem('user');

				// Flip authenticated to false so that we no longer
				// show UI elements dependant on the user being logged in
				$rootScope.authenticated = false;

				// Remove the current user info from rootscope
				$rootScope.currentUser = null;
			});
		}
	}
	
})();