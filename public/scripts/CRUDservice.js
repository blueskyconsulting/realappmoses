(function () {

    'use strict';

    angular.module('authApp')
            .factory('CRUDservice', function ($resource) {
                return $resource('api/:object/:id',
                        {
                            object: '@object',
                            id: '@id'
                        }, {
                    update: {method: 'PUT'},
                    create: {method: 'POST'},
                    delete: {method: 'DELETE'}
                });
            });

})();

