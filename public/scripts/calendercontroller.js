angular.module('authApp')
        .controller('calenderController', calenderController);
function calenderController($http,$scope, $rootScope, $compile, uiCalendarConfig, dialogService, toastService, moment, CRUDservice, filterFilter,$state) {
    var vm = this;
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.eventSources = [];
    CRUDservice.query({object: 'events'}).$promise.then(function (data) {
        $scope.events = data.events;
        $scope.tasks = data.tasks;
        $scope.dates = data.dates;
        // console.log('Event Are:'+$scope.events);
        angular.forEach($scope.events, function (data, value) {
            $scope.eventSources[value] = [{id: data.id, 'title': data.title,
                    'start': data.start, 'end': data.end, 'allDay': false}
            ];
            console.log('Value Data:' + value + '__' + $scope.eventSources[value]);
        });

        angular.forEach($scope.tasks, function (data, value) {
            $scope.eventSources[value + $scope.events.length] = [{id: data.id, 'title': data.name,
                    'start': data.start_date, color: 'yellow', // an option!
                    textColor: 'black'}
            ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
        });
        angular.forEach($scope.dates, function (data, value) {
            $scope.eventSources[value + $scope.events.length + $scope.tasks.length] = [{id: data.id, 'title': data.description,
                    'start': data.date, 'allDay': true, color: 'red', // an option!
                    textColor: 'white'}
            ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
        });




        //console.log('Event Sources Are:'+$scope.eventSources);
        //onsole.log('Event Sources Are:'+$scope.eventSources.length);
    });

    function updateScope() {
        CRUDservice.query({object: 'events'}).$promise.then(function (data) {
            $scope.events = data.events;
            $scope.tasks = data.tasks;
            $scope.dates = data.dates;
            // console.log('Event Are:'+$scope.events);
            angular.forEach($scope.events, function (data, value) {
                $scope.eventSources[value] = [{id: data.id, 'title': data.title,
                        'start': data.start, 'end': data.end, 'allDay': false}
                ];
                console.log('Value Data:' + value + '__' + $scope.eventSources[value]);
            });

            angular.forEach($scope.tasks, function (data, value) {
                $scope.eventSources[value + $scope.events.length] = [{id: data.id, 'title': data.name,
                        'start': data.start_date, color: 'yellow', // an option!
                        textColor: 'black'}
                ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
            });
            angular.forEach($scope.dates, function (data, value) {
                $scope.eventSources[value + $scope.events.length + $scope.tasks.length] = [{id: data.id, 'title': data.description,
                        'start': data.date, 'allDay': true, color: 'red', // an option!
                        textColor: 'white'}
                ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
            });

            uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('refetchEvents');

            //console.log('Event Sources Are:'+$scope.eventSources);
            //onsole.log('Event Sources Are:'+$scope.eventSources.length);
        });
    }
    $scope.searchEvents = function () {
        $scope.eventSources = filterFilter($scope.eventSources, {title: $scope.search});
        uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');
        uiCalendarConfig.calendars['myCalendar'].fullCalendar('refetchEvents');

    }
    $scope.filter = function () {
        console.log($scope.filtered);
        if ($scope.filtered === 'all') {
            updateScope();
        } else if ($scope.filtered === 'tasks') {
            angular.forEach($scope.tasks, function (data, value) {
                $scope.eventSources[value] = [{id: data.id, 'title': data.name,
                        'start': data.start_date, color: 'yellow', // an option!
                        textColor: 'black'}
                ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
            });
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('refetchEvents');
            //uiCalendarConfig.calendars['myCalendar'].fullCalendar('addEventSource', $scope.eventSources);
        } else if ($scope.filtered === 'events') {
            angular.forEach($scope.events, function (data, value) {
                $scope.eventSources[value] = [{id: data.id, 'title': data.title,
                        'start': data.start, 'end': data.end, 'allDay': false}
                ];

            });

            uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('refetchEvents');
        } else if ($scope.filtered === 'dates') {
            angular.forEach($scope.dates, function (data, value) {
                $scope.eventSources[value] = [{id: data.id, 'title': data.description,
                        'start': data.date, 'allDay': true, color: 'red', // an option!
                        textColor: 'white'}
                ];
//                console.log('Value Data:' + + '__' + $scope.eventSources[i]);    
            });
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('removeEvents');
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('refetchEvents');
        }
    }

    /* alert on eventClick */
    $scope.alertOnEventClick = function (date, jsEvent, view) {
       
        if ($(this).css('background-color') === 'rgb(255, 255, 0)') {
            $state.go('users.tasks',{search:date.title});
        }
       else if ($(this).css('background-color') === 'rgb(58, 135, 173)') {
        CRUDservice.get({object: 'events', id: date.id}).$promise.then(function (data) {

            $rootScope.orgevents = data.orgevents;

            $rootScope.opevents = data.opevents;
            $rootScope.contact_events = data.contact_events;
            $rootScope.lead_events = data.lead_events;
            $rootScope.event = data.event;
            console.log(data.event);
            dialogService.show(jsEvent, 'viewEvent', viewEvent, true);
            console.log(date.title + ' was clicked ');
        });
    }else if($(this).css('background-color') === 'rgb(255, 0, 0)'){
        console.log('date id = '+date.id);
        $http.get('api/dateredirect/'+date.id).success(function(data){
                    $scope.object=data.object;
                    var date=data.date;
                    console.log($scope.object);
                    console.log(date);
                    if($scope.object==='organization'){
                        $state.go('users.organizations',{search:date.name});
                    }else if($scope.object==='lead'){
                        $state.go('users.leads',{search:date.firstname});
                    }
                    else if($scope.object==='contact'){
                        $state.go('users.contacts',{search:date.firstname});
                    }
                   
                    
                });
         
         
    }

    };
    function viewEvent($scope) {
        $scope.close = function () {
            dialogService.close();
        }
        $scope.edit = function (event_id) {
            var event = {
                title: $scope.event.title

            }

            CRUDservice.update({object: 'events', id: event_id}, event).$promise.then(function (data) {
                toastService.show('event edited');
                updateScope();
                dialogService.close();
            });
        }
        $scope.delete = function (event_id) {
            if (confirm('Are you sure you want to delete this event?')) {
                CRUDservice.delete({object: 'events', id: event_id}).$promise.then(function (data) {
                    toastService.show('event deleted');
                    updateScope();
                    dialogService.close();
                });
            }
        }
        $scope.pin = function (object) {

            $scope.showpin = true;
            if (object === 'organization') {


                $scope.object = 'organization';
                $scope.pinobjects = CRUDservice.query({object: 'organizations'})
                        .$promise.then(function (data) {
                            $scope.organizations = data.organizations;
                            console.log($scope.organizations);
                            $scope.orgtype = data.type.id;
                        });
            } else if (object === 'customer') {

                $scope.object = 'customer';
                $scope.pinobjects = CRUDservice.query({object: 'contacts'})
                        .$promise.then(function (data) {
                            $scope.contacts = data.contacts;
                            $scope.contact_type = data.type_id.id;
                        });
            } else if (object === 'lead') {

                $scope.object = 'prospect';
                $scope.pinobjects = CRUDservice.query({object: 'leads'})
                        .$promise.then(function (data) {
                            $scope.leads = data.leads;
                            $scope.contact_type = data.type_id.id;
                        });
            } else if (object === 'opportunity') {
                $scope.object = 'opportunity';
                $scope.pinobjects = CRUDservice.query({object: 'opportunities'})
                        .$promise.then(function (data) {
                            $scope.opportunities = data.opportunities;
                            $scope.op_type = data.type.id;
                        });
            }
        };
        $scope.pinned = function (object, event_id) {
            if (object === 'organization') {
                console.log('org id ' + $scope.object_id);
                console.log('org_type' + $scope.orgtype);
                var pin = {
                    event_id: event_id,
                    entity_id: $scope.object_id,
                    type_id: $scope.orgtype
                };
                console.log(pin);
                CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {
                    toastService.show('Event pinned');
                    dialogService.close();
                    updateScope();
                });

            } else if (object === 'customer') {

                var pin = {
                    event_id: event_id,
                    entity_id: $scope.object_id,
                    type_id: $scope.contact_type
                };
                console.log(pin);
                CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {
                    toastService.show('Event pinned');
                    dialogService.close();
                    updateScope();
                });
            } else if (object === 'prospect') {
                var pin = {
                    event_id: event_id,
                    entity_id: $scope.object_id,
                    type_id: $scope.contact_type
                };
                console.log(pin);
                CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {
                    toastService.show('Event pinned');
                    dialogService.close();
                    updateScope();
                });
            } else if (object === 'opportunity') {
                var pin = {
                    event_id: event_id,
                    entity_id: $scope.object_id,
                    type_id: $scope.op_type
                };
                console.log(pin);
                CRUDservice.save({object: 'pinned_events'}, pin).$promise.then(function () {
                    toastService.show('Event pinned');
                    dialogService.close();
                    updateScope();
                });
            }
        }
    }
    /* alert on Drop */
    /*Function that run when event is dragged and dropped. 
     **/
    $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        console.log($(this).css('background-color'));
        /*We differentiate tasks, events and dates based on their rgb values*/
        if ($(this).css('background-color') === 'rgb(58, 135, 173)') {
            $rootScope.event_id = event.id;
            var start = moment(event.start.format()).format('YYYY-MM-DD HH:mm:ss');

            var end = moment(event.end.format()).format('YYYY-MM-DD HH:mm:ss');
            var event = {
                start: start,
                end: end
            };
            ;
            console.log(event.color);
            CRUDservice.update({object: 'events', id: $rootScope.event_id}, event).$promise.then(function () {
                updateScope();
                toastService.show('Event date updated');
            });

        } else if ($(this).css('background-color') === 'rgb(255, 0, 0)') {
            $rootScope.date_id = event.id;
            var start = moment(event.start.format()).format('YYYY-MM-DD HH:mm:ss');
            var date = {
                date: start
            };

            CRUDservice.update({object: 'dates', id: $rootScope.date_id}, date).$promise.then(function () {
                updateScope();
                toastService.show('Event date updated');
            });
        } else if ($(this).css('background-color') === 'rgb(255, 255, 0)') {
            $rootScope.task_id = event.id;
            var start = moment(event.start.format()).format('YYYY-MM-DD HH:mm:ss');
            var task = {
                start_date: start
            };

            CRUDservice.update({object: 'tasks', id: $rootScope.task_id}, task).$promise.then(function () {
                updateScope();
                toastService.show('Event date updated');
            });
        }


    };
    /* alert on Resize */
    $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
        console.log('Event Resized to make dayDelta ' + delta);
    };
   
    /* Change View */
    $scope.changeView = function (view) {
        uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', view);
    };
    /* Change View */
    $scope.renderCalender = function () {
        if (uiCalendarConfig.calendars['myCalendar']) {
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('render');
        }
    };
    /* Render Tooltip */
    $scope.eventRender = function (event, element, view) {
        element.attr({'tooltip': event.title,
            'tooltip-append-to-body': true});
        $compile(element)($scope);
    };
    $scope.dayClick=function(date,allDay, jsEvent, view,$event) {
        
        $rootScope.clicked_date=date.format();
        $scope.check=false;
        if(!allDay) {
              
            }
            uiCalendarConfig.calendars['myCalendar'].fullCalendar('clientEvents', function(event) {
               var start= moment(event.start).format('YYYY-MM-DD');
               var end=moment(event.end).format('YYYY-MM-DD');
               
                if(start === $rootScope.clicked_date || end === $rootScope.clicked_date) {
                    $scope.check=true;
                    
                }
              
            
            });
            if($scope.check){
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', date.format());
                
              
            }
            else{
        CRUDservice.query({object: 'tasks'}).$promise.then(function (data) {
            $rootScope.priority = data.priorities;
            
            
            $rootScope.users = data.users;
            vm.task_type = data.type;
            $rootScope.task_status=data.status;
            $rootScope.task_type=data.task_types;   
        });
        CRUDservice.query({object: 'contacts'}).$promise.then(function (data) {
            
            $rootScope.contacts = data.contacts;
           
            vm.contact_type = data.type_id.id;
           

        });
        CRUDservice.query({object: 'leads'}).$promise.then(function (data) {
            $rootScope.leads=data.leads;
        });
        CRUDservice.query({object: 'organizations'})
                .$promise.then(function (data) {
                    $rootScope.organizations = data.organizations;
                   
                    $rootScope.users = data.users;
                    vm.orgtype = data.type.id;
                   
                });
        dialogService.show($event,'addDay',addDay,true);
        }
    }
    function addDay($scope,$rootScope){
        $scope.assign_to=[];
        
        $scope.addClickEvent = function () {
            console.log($scope.title);
            var starttime=moment($scope.data.start).format('HH:mm:ss');
            var start=$rootScope.clicked_date+' '+starttime;
            var endtime=moment($scope.data.end).format('HH:mm:ss');
            var end=$rootScope.clicked_date+' '+endtime;
            
            var event = {
                title: $scope.title,
                start: start,
                end: end,
                allDay: false
            }

            CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {
                toastService.show('event added');
                updateScope();
                dialogService.close();
            });
        }
        $scope.createTask = function () {
                var start = moment($rootScope.clicked_date).format('YYYY-MM-DD HH:mm:ss');
                if($scope.reminder===undefined){
                    var reminder=null;
                }
                else{
                    var reminder = moment($scope.reminder).format('YYYY-MM-DD HH:mm:ss');
                }
                
                var task = {
                    name: $scope.name,
                    start_date: start,
                    status:$scope.status,
                    task_type:$scope.t_type,
                    reminder:reminder,
                    
                    priority_type_id: $scope.priorities
                };
               
                CRUDservice.save({object: 'tasks'}, task).$promise.then(function (data) {
                    var task = data.task;
                    if ($scope.assign_to.length > 0) {
                        angular.forEach($scope.assign_to, function (value, key) {
                            var ownersData = {
                                user_id: value.id,
                                entity_id: task.id,
                                type_id: vm.task_type.id
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                        });
                    }else{
                        var ownersData = {
                                user_id: $rootScope.currentUser.id,
                                entity_id: task.id,
                                type_id: vm.task_type.id
                            };
                            CRUDservice.save({object: 'owners'}, ownersData);
                    }
                    ;
                    updateScope();
                    //vm.tasks=data.tasks;
                    toastService.show('Task Added');
                    dialogService.close();
                });


            };
            $scope.createDay=function(){
                var type_id;
               
              if($scope.entity==='customer') {
                  type_id=vm.contact_type;
                 
                 
              }
              else if($scope.entity==='prospect'){
                   type_id=vm.contact_type;
                   
              }
              else if($scope.entity==='organization'){
                   type_id=vm.orgtype;
                   
                   
              }
              console.log($scope.object_id);
                var date = {
                    description: $scope.description,
                    date: $rootScope.clicked_date,
                    type_id: type_id,
                    entity_id: $scope.object_id

                }
                console.log(date);
                var success = CRUDservice.save({object: 'dates'}, date);
                if (success) {
                    dialogService.close();
                    updateScope();
                    toastService.show('Date added');

                }
              
            };
    }
    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 550,
            editable: true,
            header: {
                left: 'title',
                center: '',
                right: 'today prev,next'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            dayClick:$scope.dayClick
        }
    };

    /* event sources array*/


    vm.addEvent = function (ev) {

        dialogService.show(ev, 'addEvent', add, true);
    }
    function add($scope) {
        $scope.close = function () {
            dialogService.close();
        }
        $scope.addEvent = function () {
            var start = moment($scope.data.start).format('YYYY-MM-DD HH:mm:ss');

            var end = moment($scope.data.end).format('YYYY-MM-DD HH:mm:ss');

            var event = {
                title: $scope.title,
                start: start,
                end: end,
                allDay: false
            }

            CRUDservice.save({object: 'events'}, event).$promise.then(function (data) {
                toastService.show('event added');
                updateScope();
                dialogService.close();
            });
        }

    }
}
