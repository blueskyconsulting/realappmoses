<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>BlueSKy Real Estate Management System</title>
        <link rel="stylesheet" href="node_modules/css/isteven-multi-select.css">
        <link rel="stylesheet" href="node_modules/autocomplete/angucomplete-alt.css">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="node_modules/angular-material/angular-material.min.css">
        <link rel="stylesheet" href="node_modules/css/app.css"> 
        <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="node_modules/ux/build/loading-bar.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="node_modules/dist/css/app.min.css">
        <link rel="stylesheet" href="node_modules/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="node_modules/plugins/iCheck/flat/blue.css">
       
        
        <link rel="stylesheet" href="node_modules/plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" href="node_modules/plugins/morris/morris.css">
        <link rel="stylesheet" href="node_modules/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="node_modules/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="node_modules/plugins/daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="node_modules/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="node_modules/angularjs-datepicker/src/css/angular-datepicker.css">
        <link href="bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css" rel="stylesheet">
        <link rel="stylesheet" href="bower_components/fullcalendar/dist/fullcalendar.css"/>
        <link rel="stylesheet" href="node_modules/angular-ui-bootstrap/ui-bootstrap-csp.css"/>
        <link rel="stylesheet" href="node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css"/>
        <link rel="stylesheet" href="bower_components/ng-img-crop/compile/minified/ng-img-crop.css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
       
    <body ng-app="authApp" style="background-color: rgba(218, 218, 218, 0.3)" ng-cloak>

        <div ui-view class="wrapper"></div>


    </body>

     <!-- Application Dependencies -->
    <script src="node_modules/jQuery/jquery-2.1.4.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/js/jquery-ui.min.js"></script> 
    <script src="node_modules/angular/angular.js"></script>
    <script src="node_modules/ux/build/loading-bar.min.js"></script>
    <script src="node_modules/angular/angular-resource.min.js"></script>
    <script src="node_modules/angular-ui-router/build/angular-ui-router.js"></script>
    <script src="node_modules/satellizer/satellizer.js"></script>
    <script src="node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="node_modules/angular-recursion/angular-recursion.min.js"></script>
    <script src="node_modules/angular-aria/angular-aria.min.js"></script>
    <script src="node_modules/angular-material/angular-material.min.js"></script>
    <script src="node_modules/icons/angular-material-icons.min.js"></script>
     
    <script src="node_modules/angular-utils-pagination/dirPagination.js"></script>
    <script src="node_modules/autocomplete/angucomplete-alt.js"></script>
    <script src="node_modules/js/truncate.js" ></script>
    <script src="node_modules/group_by/ngGroup.js"></script>
    <script src="node_modules/js/angular-dragdrop.min.js" ></script>
    <script src="node_modules/angular-recursion/angular-recursion.min.js" ></script>
    <script src="node_modules/js/angularjs-gravatardirective.min.js" ></script>
    <script src="node_modules/angular-ui-bootstrap/ui-bootstrap.min.js" ></script>
    <script src="scripts/isteven-multi-select.js"></script>
    <script src="node_modules/angularjs-datepicker/src/js/angular-datepicker.js"></script>
    <script src="bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>
    <script src="bower_components/angular-jquery-timepicker/src/timepickerdirective.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/angular-toArrayFilter/toArrayFilter.js"></script>
    <script src="node_modules/ng-file-upload/dist/ng-file-upload.min.js" ></script>
    <script src="bower_components/ng-img-crop/compile/minified/ng-img-crop.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="node_modules/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>-->
    <script src="node_modules/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="node_modules/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="node_modules/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="node_modules/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="node_modules/plugins/fastclick/fastclick.min.js"></script>
    <script src="node_modules/ng-file-upload/dist/ng-file-upload.min.js"></script>
    <script src="node_modules/dist/js/app.min.js"></script>
   
<script type="text/javascript" src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/angular-moment/angular-moment.js"></script>
    <script type="text/javascript" src="node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js"></script>

<script type="text/javascript" src="bower_components/angular-ui-calendar/src/calendar.js"></script>
<script type="text/javascript" src="bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script type="text/javascript" src="bower_components/fullcalendar/dist/gcal.js"></script>
    
    <!-- App Scripts -->
    <script src="scripts/app.js"></script>
    <script src="scripts/appServices.js"></script>
    <script src="scripts/appDirectives.js"></script>    
    <script src="scripts/authController.js"></script>
    <script src="scripts/contactsController.js"></script>
    <script src="scripts/userController.js"></script>
    <script src="scripts/tasksController.js"></script>
    <script src="scripts/organizationsController.js"></script>
    <script src="scripts/organizationDetailController.js"></script>
    <script src="scripts/leadsController.js"></script>
    <script src="scripts/projectController.js"></script>
    <script src="scripts/appControllers.js"></script>
    <script src="scripts/leadDetail.js"></script>
    <script src="scripts/productsController.js"></script>
    <script src="scripts/productInventoryController.js"></script>
    <script src="scripts/opportunitiesController.js"></script>
    <script src="scripts/notesController.js"></script>
    <script src="scripts/calendercontroller.js"></script>
    <script src="scripts/transactionController.js"></script>

     <script src="scripts/emailController.js"></script>
     <script src="scripts/emailMessagesController.js"></script>

    <script src="scripts/documentsController.js"></script>

</html>
