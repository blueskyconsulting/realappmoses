<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="css/pdf.css">
        <style>
            
            body {
                background-image: url('img/back.png');
            }
            .footer{
                height: 150px; width:790px; overflow: hidden;
                margin-bottom: -44px;
                margin-left:-44px;
                padding: 0px 0px 0px 0px;
                position:fixed;
                bottom:0px;
                left:0%;
            }
            
            .top{
                height: 100px; width:790px; overflow: hidden;
                margin-top: -44px;
                position: fixed;
                top: 0px;
            }
            .content{
                position: relative;
            }
            
            .voffset{
                margin-top: 100px;
            }
            .voffset2{
                margin-top: 50px;
            }
            .bringright{
                margin-left: 350px;
            }
            .green-heading{
                font-size:40px;
                color:#00a65a;
            }
            .leftoffset{
                margin-left: 350px;
            }
            
            .field{
                margin-right: 0px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <img class="top" src="img/top.png" alt="a picture" class="img-responsive">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 voffset">
                        <span class="green-heading leftoffset ">Sales Invoice</span>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Date</span>:<strong>{{$invoice->due_date}}</strong>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Invoice#</span>:<strong>{{$invoice->invoice_no}}</strong>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Customer</span>:<strong>{{$invoice->firstname}}  {{$invoice->lastname}}</strong>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Company</span>:<strong>{{$invoice->org_name}}</strong>
                    </div>
                    <div class="col-md-12">
                        <strong class="leftoffset">Address</strong>:@if($address !='')
                                                                <address class='leftoffset'>
                                                                    P.O. Box{!!$address->address!!}-{!!$address->address!!}<br>
                                                                    {!!$address->city!!}, {!!$address->country!!}
                                                                </address>
                                                                @endif
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Phone</span>:<strong>{{$invoice->phone_number}}</strong>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-bordered voffset2">
                            <thead>
                                <tr>
                                    <td>Project</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$invoice->project_name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Property_no</td>
                                    <td>Product</td>
                                    <td>Deposit/cash</td>
                                    <td>Installment</td>
                                    <td>Number Of installments</td>
                                    <td>Total</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoices as $key)
                                <tr>
                                    <td>{{$key->property_no}}</td>
                                    <td>{{$key->name}}</td>
                                    <td>{{$key->deposit}}</td>
                                    <td>{{$key->installment}}</td>
                                    <td>{{$key->installment_no}}</td>
                                    <td>{{$key->price}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="5"><span class="bringright">Total amount</span></td>
                                    <td>{{$invoice->total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <img class="footer" src="img/bottom.png" alt="a picture">
        </div>
    </body>
</html>


