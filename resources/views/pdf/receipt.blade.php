<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="css/pdf.css">
        <style>
            
            body {
                background-image: url('img/back.png');
            }
            .footer{
                height: 150px; width:790px; overflow: hidden;
                margin-bottom: -44px;
                margin-left:-44px;
                padding: 0px 0px 0px 0px;
                position:fixed;
                bottom:0px;
                left:0%;
            }
            
            .top{
                height: 100px; width:790px; overflow: hidden;
                margin-top: -44px;
                position: fixed;
                top: 0px;
            }
            .content{
                position: relative;
            }
            
            .voffset{
                margin-top: 100px;
            }
            .voffset2{
                margin-top: 50px;
            }
            .bringright{
                margin-left: 330px;
            }
            .green-heading{
                font-size:40px;
                color:#00a65a;
            }
            .leftoffset{
                margin-left: 350px;
            }
            
            .field{
                margin-right: 0px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <img class="top" src="img/top.png" alt="a picture" class="img-responsive">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 voffset">
                        <span class="green-heading leftoffset ">Sales Receipt</span>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Date</span>:   <strong>{{$receipt->receipt_date}}</strong>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Receipt#</span>:<strong>{{$receipt->receipt_no}}</strong>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Sold to</span>:<span class="field"><strong>{{$receipt->firstname}} {{$receipt->lastname}}</strong></span>
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Company</span>:<span class="field"><strong>{{$receipt->org_name}}</strong></span>
                    </div>
                    <div class="col-md-12">
                        <strong class="leftoffset">Address</strong>:@if($address !='')
                                                                <address class='leftoffset'>
                                                                    P.O. Box{!!$address->address!!}-{!!$address->address!!}<br>
                                                                    {!!$address->city!!}, {!!$address->country!!}
                                                                </address>
                                                                @endif
                    </div>
                    <div class="col-md-12">
                        <span class="leftoffset">Phone</span>:  <strong>{{$receipt->phone_number}}</strong>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-bordered voffset2">
                            <thead>
                                <tr>
                                    <th>Payment Method</th>
                                    <th>Cheque No</th>
                                    <th>Project</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{!!$receipt->payment_mode!!}</td>
                                    <td>{!!$receipt->cheque_no!!}</td>
                                    <td>{!!$receipt->proj_name!!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Plot Number</th>
                                    <th>Amount Paid</th>
                                    <th>Total Invoice Amount</th>
                                    <th>Total Received</th>
                                    <th>Outstanding Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <ol style="margin-top:-1px">
                                            @foreach($receipts as $key)
                                            <li>
                                                {!!$key->property_no!!}
                                            </li>
                                            @endforeach
                                        </ol>
                                    </td>
                                    <td>{!!$receipt->amount!!}</td>
                                    <td>{!!$receipt->total!!}</td>
                                    <td>
                                        <ul style="list-style: none;margin-top:-1px">
                                            @foreach($received as $key)
                                                <li>{!!$key->amount!!}</li>
                                            @endforeach
                                            <hr style="width: 50px;margin-left:-5px">
                                            <li><strong>{!!$received_total->received_sum!!}</strong></li>
                                        </ul>
                                    </td>
                                    <td>{!!$receipt->total-$received_total->received_sum!!}</td>
                                </tr>
                                <tr>
                                    <th colspan="4"><span class="bringright">Next Installment Date</span></th>
                                    <td>{!!$receipt->next_payment_date!!}</td>
                                </tr>
                                <tr>
                                    <th colspan="4"><span class="bringright">Next Installment Amount</span></th>
                                    <td>{!!$receipt->installment!!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <img class="footer" src="img/bottom.png" alt="a picture">
        </div>
    </body>
</html>

